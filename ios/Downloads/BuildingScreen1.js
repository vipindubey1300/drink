import React, { Component } from 'react';
import { View, Alert, KeyboardAvoidingView, PermissionsAndroid,Dimensions, Platform, StyleSheet, Modal, ImageBackground, ScrollView, Image, Text, TextInput,TouchableWithoutFeedback ,TouchableOpacity,Keyboard, ActivityIndicator} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import { Customtextinput, Customtext } from '../components/CustomComponent';
import CustomDropdown from '../components/MyDropDown'
import PickerExample from '../components/PickerExample'
import { background, logo } from '../utility/Icons';
import { Container, Header, Content, Picker, Form, Left ,Input} from "native-base";
import FontAwesome from "react-native-vector-icons/dist/FontAwesome";
import { validateFields, validateDate } from '../utility/Validation';
var widthhh = Dimensions.get('window').width
var heighttt = Dimensions.get('window').height
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';

var RNFS = require('react-native-fs');
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

export default class BuildingScreen1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      items: [],
      pdf_data: [],
      selected: '',
      value: '',

      building: '',
      week_of: '',
      manager: '',

      /*------------------------ Supt -------------------------------------------------- */
      supt: '',
      supt_rating: '',

      /* ------------------------- Exterior ------------------------------------------ */
      exterior_cond_sidewalk_steps: '',
      exterior_cond_cans_cover: '',
      exterior_cond_doors: '',
      exterior_cond_courtyard: '',
      exterior_cond_yard_drain: '',
      exterior_cond_fountain_walls: '',
      exterior_cond_lights_at_entry: '',
      exterior_cond_lights_at_yard: '',

      exterior_cond_sidewalk_steps_rating: '',
      exterior_cond_cans_cover_rating: '',
      exterior_cond_doors_rating: '',
      exterior_cond_courtyard_rating: '',
      exterior_cond_yard_drain_rating: '',
      exterior_cond_fountain_walls_rating: '',
      exterior_cond_lights_at_entry_rating: '',
      exterior_cond_lights_at_yard_rating: '',
      /*---------------------Entrance --------------------------------------------- */
      entrance_entrance_door: '', entrance_entrance_door_rating: '',
      entrance_sidelight: '', entrance_sidelight_rating: '',
      entrance_intercom: '', entrance_intercom_rating: '',
      entrance_mailboxes: '', entrance_mailboxes_rating: '',
      entrance_floor: '', entrance_floor_rating: '',
      entrance_walls: '', entrance_walls_rating: '',
      entrance_lightning: '', entrance_lightning_rating: '',

      /*-----------------Stairwells------------------------ */
      stairwells_steps_treads: '', stairwells_steps_treads_rating: '',
      stairwells_walls: '', stairwells_walls_rating: '',
      stairwells_lightning: '', stairwells_lightning_rating: '',
      stairwells_emergency_lightning: '', stairwells_emergency_lightning_rating: '',
      stairwells_doors: '', stairwells_doors_rating: '',
      stairwells_windows: '', stairwells_windows_rating: '',
      stairwells_signs: '', stairwells_signs_rating: '',

      /*----------------------Emergency------------------- */
      emergency_exit_lightning: '',
      emergency_exit_floor: '',
      emergency_exit_doors: '',
      emergency_exit_signs: '',

      emergency_exit_lightning_rating: '',
      emergency_exit_floor_rating: '',
      emergency_exit_doors_rating: '',
      emergency_exit_signs_rating: '',

      /*--------------------Roof ---------------------------- */
      roof_bulkhead: '',
      roof_roof_doors: '',
      roof_roof_surface: '',
      roof_gutter: '',
      roof__leaders: '',
      roof_skylight: '',
      roof_parapet: '',


      roof_bulkhead_rating: '',
      roof_roof_doors_rating: '',
      roof_roof_surface_rating: '',
      roof_gutter_rating: '',
      roof__leaders_rating: '',
      roof_skylight_rating: '',
      roof_parapet_rating: '',

      /*------------------------------ fire escapes ----------------------------------- */
      fire_escapes_ladders: '',
      fire_escapes_basket: '',
      fire_escapes_railling: '',
      fire_escapes_ladder_shoes: '',

      fire_escapes_ladders_rating: '',
      fire_escapes_basket_rating: '',
      fire_escapes_railling_rating: '',
      fire_escapes_ladder_shoes_rating: '',

      /*----------------------------- Lobbies -------------------------------------------- */

      lobbies_floor: '',
      lobbies_walls: '',
      lobbies_lightning_emergency: '',
      lobbies_windows_sills: '',


      lobbies_floor_rating: '',
      lobbies_walls_rating: '',
      lobbies_lightning_emergency_rating: '',
      lobbies_windows_sills_rating: '',

      /* --------------------------------- basement ----------------------------------------------- */
      basement_doors: '',
      basement_stairs: '',
      basement_floor: '',
      basement_walls: '',
      basement_lightning: '',
      basement_windows: '',
      basement_garbage_room: '',


      basement_doors_rating: '',
      basement_stairs_rating: '',
      basement_floor_rating: '',
      basement_walls_rating: '',
      basement_lightning_rating: '',
      basement_windows_rating: '',
      basement_garbage_room_rating: '',

      /*--------------------------------------- Laundry Room --------------------------------------- */

      laundry_room_equipment: '',
      laundry_room_floor: '',
      laundry_room_walls: '',
      laundry_room_lightning: '',

      laundry_room_equipment_rating: '',
      laundry_room_floor_rating: '',
      laundry_room_walls_rating: '',
      laundry_room_lightning_rating: '',

      /*------------------------------------ Boiler------------------------------------------------- */

      boiler_fuel_supply: '',
      boiler_gauge_color_coding_markings: '',
      boiler_nurners: '',
      boiler_coils: '',
      boiler_oil_tank: '',
      boiler_plumbing: '',
      boiler_leaks: '',
      boiler_overheads_returns: '',


      boiler_fuel_supply_rating: '',
      boiler_gauge_color_coding_markings_rating: '',
      boiler_nurners_rating: '',
      boiler_coils_rating: '',
      boiler_oil_tank_rating: '',
      boiler_plumbing_rating: '',
      boiler_leaks_rating: '',
      boiler_overheads_returns_rating: '',

      /*------------------------------------ Meters ------------------------------------------------------- */

      meters_lightning: '',
      meters_accessibility: '',
      meters_up_keeping: '',


      meters_lightning_rating: '',
      meters_accessibility_rating: '',
      meters_up_keeping_rating: '',

      /*--------------------------- Elevator ------------------------------------------------------ */

      elevator_lightning: '',
      elevator_signals: '',
      elevator_doors: '',
      elevator_cab_floor: '',
      elevator_cab_walls: '',
      elevator_cab_ceilling: '',
      elevator_floor_nmber_on_doors: '',


      elevator_lightning_rating: '',
      elevator_signals_rating: '',
      elevator_doors_rating: '',
      elevator_cab_floor_rating: '',
      elevator_cab_walls_rating: '',
      elevator_cab_ceilling_rating: '',
      elevator_floor_nmber_on_doors_rating: '',

      /* -------------------------------------------------Fire Equipments --------------------------------------- */
      fire_equipment_sprinklers: '',
      fire_equipment_extinguishers: '',
      fire_equipment_alarm_system: '',

      fire_equipment_sprinklers_rating: '',
      fire_equipment_extinguishers_rating: '',
      fire_equipment_alarm_system_rating: '',

      /*-------------------------------------- Vacant---------------------------------------- */
      vacant_units_apt1: '',
      vacant_units_apt1_rating: '',
      vacant_units_apt2: '',
      vacant_units_apt2_rating: '',

      inventory: '',
      inventory_rating: '',

      problem: '',
      notes: '',


      selectedChooseRating: '',

      bottomHeight:0


    }
  }



  onSwipeUp(gestureState) {
    this.setState({myText: 'You swiped up!'});
    Keyboard.dismiss()
  }
 
  onSwipeDown(gestureState) {
    this.setState({myText: 'You swiped down!'});
    Keyboard.dismiss()
  }
 
  onSwipeLeft(gestureState) {
    this.setState({myText: 'You swiped left!'});
  }
 
  onSwipeRight(gestureState) {
    this.setState({myText: 'You swiped right!'});
  }
  onSwipe(gestureName, gestureState) {
    const {SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
    Keyboard.dismiss()
    console.log("------->>>>",swipeDirections)
   
  }


  testIOS =()=>{
      RNFS.readDir(RNFS.DocumentDirectoryPath).then(files => {
      //console.log('--->>>>>>',files)
      Alert.alert(JSON.stringify(files))
  })
  .catch(err => {
    
     // console.log(err.message, err.code);
     
  });
  }

  componentDidMount(){ 

    this.DropDownData_Api();

    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow =() =>{
    this.setState({bottomHeight:200})
  }

  _keyboardDidHide =() => {
    this.setState({bottomHeight:0})
  }

  DropDownData_Api = () => {
    this.setState({ loading: true })
    try {
      const siteUrl = "http://webmobril.org/dev/inspectionapp/api/ratings";
      fetch(siteUrl)
        .then(response => response.json())
        .then((resp) => {
          if (resp.code == 200) {
            //  this.setState({loading:false})   
            var temp = [] 
            if(Platform.OS == 'andorid')  resp.data // not remove becayse it act as placeholder in android
            else if  (Platform.OS == 'ios')  resp.data.shift() // will remove first element 
            console.log(resp.data)
            this.setState({ items:resp.data }, () => this.setState({ loading: false }))
       //, () => this.setState({ loading: false })
          }
          else {
            this.setState({ loading: false })
            alert("Oops! Please check network connection.")
          }
        }).catch((e) => {
          this.setState({ loading: false })
         // console.log("error=->", e)
        })
    }
    catch (err) {
      this.setState({ loading: false })
     // console.log("Exeption error=-=>>", err)
    }
  }


  onValueChange = (value, name) => {
    /* --------------------- Exterior Rating ----------------------- */

    if (name == "Rating0") {
      this.setState({ supt_rating: value })
     // console.log('----------', this.state.supt_rating)
    }
    else if (name == "Rating1") {
      this.setState({ exterior_cond_sidewalk_steps_rating: value })
      console.log('----------', this.state.exterior_cond_sidewalk_steps_rating)
    }
    else if (name == "Rating2") {
      this.setState({ exterior_cond_cans_cover_rating: value })
      console.log('----------', this.state.exterior_cond_cans_cover_rating)
    }
    else if (name == "Rating3") {
      this.setState({ exterior_cond_doors_rating: value })
      console.log('----------', this.state.exterior_cond_doors_rating)
    }
    else if (name == "Rating4") {
      this.setState({ exterior_cond_courtyard_rating: value })
      console.log('----------', this.state.exterior_cond_courtyard_rating)
    }
    else if (name == "Rating5") {
     
      this.setState({ exterior_cond_yard_drain_rating: value })
      console.log('----------', this.state.exterior_cond_yard_drain_rating)
    }
    else if (name == "Rating6") {
     
      this.setState({ exterior_cond_fountain_walls_rating: value })
      console.log(this.state.exterior_cond_fountain_walls_rating)
    }
    else if (name == "Rating7") {
     
      this.setState({ exterior_cond_lights_at_entry_rating: value })
      console.log('----------', this.state.exterior_cond_lights_at_entry_rating)
    }
    else if (name == "Rating8") {
     
      this.setState({ exterior_cond_lights_at_yard_rating: value })
      console.log('----------', this.state.exterior_cond_lights_at_yard_rating)
    }
    /*-------------------------- Entrance Condition----------------------- */

    else if (name == "Rating9") {
     
      this.setState({ entrance_entrance_door_rating: value })
    }

    else if (name == "Rating10") {
     
      this.setState({ entrance_sidelight_rating: value })
    }
    else if (name == "Rating11") {
     
      this.setState({ entrance_intercom_rating: value })
    }
    else if (name == "Rating12") {
     
      this.setState({ entrance_mailboxes_rating: value })
    }
    else if (name == "Rating13") {
     
      this.setState({ entrance_floor_rating: value })
    }
    else if (name == "Rating14") {
     
      this.setState({ entrance_walls_rating: value })
    }

    /*------------------------------- stairwells_Rating -------------------------- */
    else if (name == "Rating15") {
      this.setState({ stairwells_steps_treads_rating: value })

    }
    else if (name == "Rating16") {
      this.setState({ stairwells_walls_rating: value })
    }
    else if (name == "Rating17") {
      this.setState({ stairwells_lightning_rating: value })
    }
    else if (name == "Rating18") {
      this.setState({ stairwells_emergency_lightning_rating: value })
    }
    else if (name == "Rating19") {
      this.setState({ stairwells_doors_rating: value })
    }
    else if (name == "Rating20") {
      this.setState({ stairwells_windows_rating: value })
    }
    else if (name == "Rating21") {
      this.setState({ stairwells_signs_rating: value })
    }
    /*------------------------------- Emergency Rating------------------------ */

    else if (name == "Rating22") {
      this.setState({ emergency_exit_lightning_rating: value })
    }
    else if (name == "Rating23") {
      this.setState({ emergency_exit_floor_rating: value })
    }
    else if (name == "Rating24") {
      this.setState({ emergency_exit_doors_rating: value })
    }
    else if (name == "Rating25") {
      this.setState({ emergency_exit_signs_rating: value })
    }

    /*------------------------------- Roof Rating------------------------ */
    else if (name == "Rating26") {
      this.setState({ roof_bulkhead_rating: value })

    } else if (name == "Rating27") {
      this.setState({ roof_roof_doors_rating: value })
    }
    else if (name == "Rating28") {
      this.setState({ roof_roof_surface_rating: value })
    }
    else if (name == "Rating29") {
      this.setState({ roof_gutter_rating: value })
    }
    else if (name == "Rating30") {
      this.setState({ roof__leaders_rating: value })
    } else if (name == "Rating31") {
      this.setState({ roof_skylight_rating: value })
    }
    else if (name == "Rating32") {
      this.setState({ roof_parapet_rating: value })
    }
    else if (name == "Rating33") {
      this.setState({ fire_escapes_ladders_rating: value })
    }
    else if (name == "Rating34") {
      this.setState({ fire_escapes_basket_rating: value })
    }
    else if (name == "Rating35") {
      this.setState({ fire_escapes_railling_rating: value })
    }
    else if (name == "Rating36") {
      this.setState({ fire_escapes_ladder_shoes_rating: value })
    }
    else if (name == "Rating37") {
      this.setState({ lobbies_floor_rating: value })
    }
    else if (name == "Rating38") {
      this.setState({ lobbies_walls_rating: value })
    }
    else if (name == "Rating39") {
      this.setState({ lobbies_lightning_emergency_rating: value })
    }
    else if (name == "Rating40") {
      this.setState({ lobbies_windows_sills_rating: value })
    }
    else if (name == "Rating40") {
      this.setState({ basement_doors_rating: value })
    }
    else if (name == "Rating41") {
      this.setState({ basement_stairs_rating: value })
    }
    else if (name == "Rating42") {
      this.setState({ basement_floor_rating: value })
    }
    else if (name == "Rating43") {
      this.setState({ basement_walls_rating: value })
    }
    else if (name == "Rating44") {
      this.setState({ basement_lightning_rating: value })
    }
    else if (name == "Rating45") {
      this.setState({ basement_windows_rating: value })
    }
    else if (name == "Rating46") {
      this.setState({ basement_garbage_room_rating: value })
    }
    else if (name == "Rating47") {
      this.setState({ laundry_room_equipment_rating: value })
    }
    else if (name == "Rating48") {
      this.setState({ laundry_room_floor_rating: value })
    }
    else if (name == "Rating49") {
      this.setState({ laundry_room_walls_rating: value })
    }
    else if (name == "Rating50") {
      this.setState({ laundry_room_lightning_rating: value })
    }
    else if (name == "Rating51") {
      this.setState({ boiler_fuel_supply_rating: value })
    }
    else if (name == "Rating52") {
      this.setState({ boiler_gauge_color_coding_markings_rating: value })
    }
    else if (name == "Rating53") {
      this.setState({ boiler_nurners_rating: value })
    }
    else if (name == "Rating54") {
      this.setState({ boiler_oil_tank_rating: value })
    }
    else if (name == "Rating55") {
      this.setState({ boiler_plumbing_rating: value })
    }
    else if (name == "Rating56") {
      this.setState({ boiler_leaks_rating: value })
    }
    else if (name == "Rating57") {
      this.setState({ boiler_overheads_returns_rating: value })
    }

    else if (name == "Rating58") {
      this.setState({ meters_lightning_rating: value })
    }
    else if (name == "Rating59") {
      this.setState({ meters_accessibility_rating: value })
    }
    else if (name == "Rating60") {
      this.setState({ meters_up_keeping_rating: value })
    }

    else if (name == "Rating61") {
      this.setState({ elevator_lightning_rating: value })
    }
    else if (name == "Rating62") {
      this.setState({ elevator_signals_rating: value })
    }
    else if (name == "Rating63") {
      this.setState({ elevator_doors_rating: value })
    }
    else if (name == "Rating64") {
      this.setState({ elevator_cab_floor_rating: value })
    }
    else if (name == "Rating65") {
      this.setState({ elevator_cab_walls_rating: value })
    }
    else if (name == "Rating66") {
      this.setState({ elevator_cab_ceilling_rating: value })
    }
    else if (name == "Rating67") {
      this.setState({ elevator_floor_nmber_on_doors_rating: value })
    }

    else if (name == "Rating68") {
      this.setState({ fire_equipment_sprinklers_rating: value })
    }
    else if (name == "Rating69") {
      this.setState({ fire_equipment_extinguishers_rating: value })
    }
    else if (name == "Rating70") {
      this.setState({ fire_equipment_alarm_system_rating: value })
    }

    /*-----------------------------Vacant---------------------------------------- */

    else if (name == "Rating71") {

      this.setState({ vacant_units_apt1_rating: value })
    }
    else if (name == "Rating72") {
      this.setState({ vacant_units_apt2_rating: value })
    }

    /*--------------------- Inventory -------------------------------------------- */
    else if (name == "Rating73") {
      this.setState({ inventory_rating: value })
    }

    else {
      alert("Please choose rating.")
    }
  }


  AskPermissionGalary = () => {
    console.log("AskPermissionGalary fn called")
    var that = this;
    //Checking for the permission just after component loaded
    async function requestGalaryPermission() {
      //Calling the permission function
      //   android.permission.WRITE_EXTERNAL_STORAGE
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'App Galary Permission.',
          message: 'App needs access to your Galary.',
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        that.createPDF();
      } else {
        alert('Galary Permission Denied.');
      }
    }
    if (Platform.OS === 'android') {
      requestGalaryPermission();
    } else {
      this.createPDF();
    }
  }


  saveDataIOS = async (from,name) => {
    console.log('yess IOSSSS----------')
    Alert.alert("PDF file saved succcessfully " + name );
  
  }

   saveData = async (from) => {
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      ]);
    } catch (err) {
      Alert.alert("Write permissions have not been granted,go to settings to enable it." );
    }
    const readGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE); 
    const writeGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
    if(!readGranted || !writeGranted) {
      console.log('Read and write permissions have not been granted');
      Alert.alert("Read and write permissions have not been granted" );
      return;
    }
    let r = Math.random().toString(36).substring(7);
    let name = 'building_' + r.toString()
   
    var path = `${RNFS.ExternalStorageDirectoryPath}/BuildingIspection`;
    RNFS.mkdir(path);
    path += '/'+ name+ '.pdf';
    RNFS.moveFile(from, path)
      .then((success) => {
        console.log('Success',path);
        Alert.alert("PDF file saved succcessfully " + name );
      })
      .catch((err) => {
        console.log(err.message);
        Alert.alert(err.message)
      });
  }

  createPDF = async () => {
    let body = {
      'building': this.state.building,
      "week_of": this.state.week_of,
      "manager": this.state.manager,
      "supt": this.state.supt,
      "rating_choose_from": this.state.supt_rating,

      "exterior_cond_sidewalk_steps": this.state.exterior_cond_sidewalk_steps,
      "exterior_cond_sidewalk_steps_rating": this.state.exterior_cond_sidewalk_steps_rating,

      "exterior_cond_cans_cover": this.state.exterior_cond_cans_cover,
      "exterior_cond_cans_cover_rating": this.state.exterior_cond_cans_cover_rating,

      "exterior_cond_doors": this.state.exterior_cond_doors,
      "exterior_cond_doors_rating": this.state.exterior_cond_doors_rating,

      "exterior_cond_courtyard": this.state.exterior_cond_courtyard,
      "exterior_cond_courtyard_rating": this.state.exterior_cond_courtyard_rating,

      "exterior_cond_yard_drain": this.state.exterior_cond_yard_drain,
      "exterior_cond_yard_drain_rating": this.state.exterior_cond_yard_drain_rating,

      "exterior_cond_fountain_walls": this.state.exterior_cond_fountain_walls,
      "exterior_cond_fountain_walls_rating": this.state.exterior_cond_fountain_walls_rating,

      "exterior_cond_lights_at_entry": this.state.exterior_cond_lights_at_entry,
      "exterior_cond_lights_at_entry_rating": this.state.exterior_cond_lights_at_entry_rating,

      "exterior_cond_lights_at_yard": this.state.exterior_cond_lights_at_yard,
      "exterior_cond_lights_at_yard_rating": this.state.exterior_cond_lights_at_yard_rating,

      "entrance_entrance_door": this.state.entrance_entrance_door,
      "entrance_entrance_door_rating": this.state.entrance_entrance_door_rating,

      "entrance_sidelight": this.state.entrance_sidelight,
      "entrance_sidelight_rating": this.state.entrance_sidelight_rating,

      "entrance_intercom": this.state.entrance_intercom,
      "entrance_intercom_rating": this.state.entrance_intercom_rating,

      "entrance_mailboxes": this.state.entrance_mailboxes,
      "entrance_mailboxes_rating": this.state.entrance_mailboxes_rating,

      "entrance_floor": this.state.entrance_floor,
      "entrance_floor_rating": this.state.entrance_floor_rating,

      "entrance_walls": this.state.entrance_walls,
      "entrance_walls_rating": this.state.entrance_walls_rating,

      "entrance_lightning": this.state.entrance_lightning,
      "entrance_lightning_rating": this.state.entrance_lightning_rating,

      "stairwells_steps_treads": this.state.stairwells_steps_treads,
      "stairwells_steps_treads_rating": this.state.stairwells_steps_treads_rating,

      "stairwells_walls": this.state.stairwells_walls,
      "stairwells_walls_rating": this.state.stairwells_walls_rating,

      "stairwells_lightning": this.state.stairwells_lightning,
      "stairwells_lightning_rating": this.state.stairwells_lightning_rating,

      "stairwells_emergency_lightning": this.state.stairwells_emergency_lightning,
      "stairwells_emergency_lightning_rating": this.state.stairwells_emergency_lightning_rating,

      "stairwells_doors": this.state.stairwells_doors,
      "stairwells_doors_rating": this.state.stairwells_doors_rating,

      "stairwells_windows": this.state.stairwells_windows,
      "stairwells_windows_rating": this.state.stairwells_windows_rating,

      "stairwells_signs": this.state.stairwells_signs,
      "stairwells_signs_rating": this.state.stairwells_signs_rating,

      "emergency_exit_lightning": this.state.emergency_exit_lightning,
      "emergency_exit_lightning_rating": this.state.emergency_exit_lightning_rating,

      "emergency_exit_floor": this.state.emergency_exit_floor,
      "emergency_exit_floor_rating": this.state.emergency_exit_floor_rating,

      "emergency_exit_doors": this.state.emergency_exit_doors,
      "emergency_exit_doors_rating": this.state.emergency_exit_doors_rating,

      "emergency_exit_signs": this.state.emergency_exit_signs,
      "emergency_exit_signs_rating": this.state.emergency_exit_signs_rating,

      "roof_bulkhead": this.state.roof_bulkhead,
      "roof_bulkhead_rating": this.state.roof_bulkhead_rating,

      "roof_roof_doors": this.state.roof_roof_doors,
      "roof_roof_doors_rating": this.state.roof_roof_doors_rating,

      "roof_roof_surface": this.state.roof_roof_surface,
      "roof_roof_surface_rating": this.state.roof_roof_surface_rating,

      "roof_gutter": this.state.roof_gutter,
      "roof_gutter_rating": this.state.roof_gutter_rating,

      "roof__leaders": this.state.roof__leaders,
      "roof__leaders_rating": this.state.roof__leaders_rating,

      "roof_skylight": this.state.roof_skylight,
      "roof_skylight_rating": this.state.roof_skylight_rating,

      "roof_parapet": this.state.roof_parapet,
      "roof_parapet_rating": this.state.roof_parapet_rating,
      "fire_escapes_ladders": this.state.fire_escapes_ladders,
      "fire_escapes_ladders_rating": this.state.fire_escapes_ladders_rating,

      "fire_escapes_basket": this.state.fire_escapes_basket,
      "fire_escapes_basket_rating": this.state.fire_escapes_basket_rating,

      "fire_escapes_railling": this.state.fire_escapes_railling,
      "fire_escapes_railling_rating": this.state.fire_escapes_railling_rating,

      "fire_escapes_ladder_shoes": this.state.fire_escapes_ladder_shoes,
      "fire_escapes_ladder_shoes_rating": this.state.fire_escapes_ladder_shoes_rating,

      "lobbies_floor": this.state.lobbies_floor,
      "lobbies_floor_rating": this.state.lobbies_floor_rating,

      "lobbies_walls": this.state.lobbies_walls,
      "lobbies_walls_rating": this.state.lobbies_walls_rating,

      "lobbies_lightning_emergency": this.state.lobbies_lightning_emergency,
      "lobbies_lightning_emergency_rating": this.state.lobbies_lightning_emergency_rating,

      "lobbies_windows_sills": this.state.lobbies_windows_sills,
      "lobbies_windows_sills_rating": this.state.lobbies_windows_sills_rating,

      "basement_doors": this.state.basement_doors,
      "basement_doors_rating": this.state.basement_doors_rating,

      "basement_stairs": this.state.basement_stairs,
      "basement_stairs_rating": this.state.basement_stairs_rating,

      "basement_floor": this.state.basement_floor,
      "basement_floor_rating": this.state.basement_floor_rating,

      "basement_walls": this.state.basement_walls,
      "basement_walls_rating": this.state.basement_walls_rating,

      "basement_lightning": this.state.basement_lightning,
      "basement_lightning_rating": this.state.basement_lightning_rating,

      "basement_windows": this.state.basement_windows,
      "basement_windows_rating": this.state.basement_windows_rating,

      "basement_garbage_room": this.state.basement_garbage_room,
      "basement_garbage_room_rating": this.state.basement_garbage_room_rating,

      "laundry_room_equipment": this.state.laundry_room_equipment,
      "laundry_room_equipment_rating": this.state.laundry_room_equipment_rating,

      "laundry_room_floor": this.state.laundry_room_floor,
      "laundry_room_floor_rating": this.state.laundry_room_floor_rating,

      "laundry_room_walls": this.state.laundry_room_walls,
      "laundry_room_walls_rating": this.state.laundry_room_walls_rating,

      "laundry_room_lightning": this.state.laundry_room_lightning,
      "laundry_room_lightning_rating": this.state.laundry_room_lightning_rating,

      "boiler_fuel_supply": this.state.boiler_fuel_supply,
      "boiler_fuel_supply_rating": this.state.boiler_fuel_supply_rating,

      "boiler_gauge_color_coding_markings": this.state.boiler_gauge_color_coding_markings,
      "boiler_gauge_color_coding_markings_rating": this.state.boiler_gauge_color_coding_markings_rating,


      "boiler_nurners": this.state.boiler_nurners,
      "boiler_nurners_rating": this.state.boiler_nurners_rating,

      "boiler_coils": this.state.boiler_coils,
      "boiler_coils_rating": this.state.boiler_coils_rating,

      "boiler_oil_tank": this.state.boiler_oil_tank,
      "boiler_oil_tank_rating": this.state.boiler_oil_tank_rating,

      "boiler_plumbing": this.state.boiler_plumbing,
      "boiler_plumbing_rating": this.state.boiler_plumbing_rating,

      "boiler_leaks": this.state.boiler_leaks,
      "boiler_leaks_rating": this.state.boiler_leaks_rating,

      "boiler_overheads_returns": this.state.boiler_overheads_returns,
      "boiler_overheads_returns_rating": this.state.boiler_overheads_returns_rating,

      "meters_lightning": this.state.meters_lightning,
      "meters_lightning_rating": this.state.meters_lightning_rating,

      "meters_accessibility": this.state.meters_accessibility,
      "meters_accessibility_rating": this.state.meters_accessibility_rating,

      "meters_up_keeping": this.state.meters_up_keeping,
      "meters_up_keeping_rating": this.state.meters_up_keeping_rating,

      "elevator_lightning": this.state.elevator_lightning,
      "elevator_lightning_rating": this.state.elevator_lightning_rating,

      "elevator_signals": this.state.elevator_signals,
      "elevator_signals_rating": this.state.elevator_signals_rating,

      "elevator_doors": this.state.elevator_doors,
      "elevator_doors_rating": this.state.elevator_doors_rating,

      "elevator_cab_floor": this.state.elevator_cab_floor,
      "elevator_cab_floor_rating": this.state.elevator_cab_floor_rating,

      "elevator_cab_walls": this.state.elevator_cab_walls,
      "elevator_cab_walls_rating": this.state.elevator_cab_walls_rating,

      "elevator_cab_ceilling": this.state.elevator_cab_ceilling,
      "elevator_cab_ceilling_rating": this.state.elevator_cab_ceilling_rating,

      "elevator_floor_nmber_on_doors": this.state.elevator_floor_nmber_on_doors,
      "elevator_floor_nmber_on_doors_rating": this.state.elevator_floor_nmber_on_doors_rating,

      "fire_equipment_sprinklers": this.state.fire_equipment_sprinklers,
      "fire_equipment_sprinklers_rating": this.state.fire_equipment_sprinklers_rating,

      "fire_equipment_extinguishers": this.state.fire_equipment_extinguishers,
      "fire_equipment_extinguishers_rating": this.state.fire_equipment_extinguishers_rating,

      "fire_equipment_alarm_system": this.state.fire_equipment_alarm_system,
      "fire_equipment_alarm_system_rating": this.state.fire_equipment_alarm_system_rating,


      "vacant_units_apt1": this.state.vacant_units_apt1,
      "vacant_units_apt1_rating": this.state.vacant_units_apt1_rating,

      "vacant_units_apt2": this.state.vacant_units_apt2,
      "vacant_units_apt2_rating": this.state.vacant_units_apt2_rating,

      "inventory": this.state.inventory,
      "inventory_rating": this.state.inventory_rating,

      "problem": this.state.problem,
      "notes": this.state.notes,


    }


    //this.Call();
    console.log("createPDF fn called")
    var dta = this.state.pdf_data
    // var String_data = JSON.stringify(dta)
    var String_data = JSON.stringify(body)


    console.log("PDF DATA_+=->>", String_data)

    let a = []
    Object.entries(body).forEach(function ([key, value]) {
      a.push('<tr><td>' + `${key}` + '</td><td>' + `${value}` + '</td></tr>')
    })

    let r = Math.random().toString(36).substring(7);
    let name = 'building_' + r.toString()
   
    let options = {
      html: `<p>
          <h1 style="color:red;"><u>Building Inspection Details</u></h1></p>
          <table border=1> ${a} </table>`,
      fileName: name,
      directory: 'Documents',
    };

    

   if(Platform.OS == 'android'){
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
      ]);
    } catch (err) {
      Alert.alert("Write permissions have not been granted,go to settings to enable it." );
    }
    const readGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE); 
    const writeGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
    if(!readGranted || !writeGranted) {
      console.log('Read and write permissions have not been granted');
      Alert.alert("Read and write permissions have not been granted" );
      return;
    }
   }

    this.Call()
    let file = await RNHTMLtoPDF.convert(options)
    console.log("Pdf stored location=-=>>", file.filePath);
    Platform.OS == 'android' ? this.saveData(file.filePath)
    : this.saveDataIOS(file.filePath,name)

    
  }

  requestRunTimePermission = () => {
    var that = this;
    async function externalStoragePermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External Storage Write Permission',
            message: 'App needs access to Storage data.',
          }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          that.createPDF_File();
        } else {
          alert('WRITE_EXTERNAL_STORAGE permission denied');
        }
      } catch (err) {
        Alert.alert('Write permission err', err);
        console.warn(err);
      }
    }

    if (Platform.OS === 'android') {
        externalStoragePermission();
    } else {
        this.createPDF_File();
    }
  }

  async createPDF_File() {

    let body = {
      'building': this.state.building,
      "week_of": this.state.week_of,
      "manager": this.state.manager,
      "supt": this.state.supt,
      "rating_choose_from": this.state.supt_rating,

      "exterior_cond_sidewalk_steps": this.state.exterior_cond_sidewalk_steps,
      "exterior_cond_sidewalk_steps_rating": this.state.exterior_cond_sidewalk_steps_rating,

      "exterior_cond_cans_cover": this.state.exterior_cond_cans_cover,
      "exterior_cond_cans_cover_rating": this.state.exterior_cond_cans_cover_rating,

      "exterior_cond_doors": this.state.exterior_cond_doors,
      "exterior_cond_doors_rating": this.state.exterior_cond_doors_rating,

      "exterior_cond_courtyard": this.state.exterior_cond_courtyard,
      "exterior_cond_courtyard_rating": this.state.exterior_cond_courtyard_rating,

      "exterior_cond_yard_drain": this.state.exterior_cond_yard_drain,
      "exterior_cond_yard_drain_rating": this.state.exterior_cond_yard_drain_rating,

      "exterior_cond_fountain_walls": this.state.exterior_cond_fountain_walls,
      "exterior_cond_fountain_walls_rating": this.state.exterior_cond_fountain_walls_rating,

      "exterior_cond_lights_at_entry": this.state.exterior_cond_lights_at_entry,
      "exterior_cond_lights_at_entry_rating": this.state.exterior_cond_lights_at_entry_rating,

      "exterior_cond_lights_at_yard": this.state.exterior_cond_lights_at_yard,
      "exterior_cond_lights_at_yard_rating": this.state.exterior_cond_lights_at_yard_rating,

      "entrance_entrance_door": this.state.entrance_entrance_door,
      "entrance_entrance_door_rating": this.state.entrance_entrance_door_rating,

      "entrance_sidelight": this.state.entrance_sidelight,
      "entrance_sidelight_rating": this.state.entrance_sidelight_rating,

      "entrance_intercom": this.state.entrance_intercom,
      "entrance_intercom_rating": this.state.entrance_intercom_rating,

      "entrance_mailboxes": this.state.entrance_mailboxes,
      "entrance_mailboxes_rating": this.state.entrance_mailboxes_rating,

      "entrance_floor": this.state.entrance_floor,
      "entrance_floor_rating": this.state.entrance_floor_rating,

      "entrance_walls": this.state.entrance_walls,
      "entrance_walls_rating": this.state.entrance_walls_rating,

      "entrance_lightning": this.state.entrance_lightning,
      "entrance_lightning_rating": this.state.entrance_lightning_rating,

      "stairwells_steps_treads": this.state.stairwells_steps_treads,
      "stairwells_steps_treads_rating": this.state.stairwells_steps_treads_rating,

      "stairwells_walls": this.state.stairwells_walls,
      "stairwells_walls_rating": this.state.stairwells_walls_rating,

      "stairwells_lightning": this.state.stairwells_lightning,
      "stairwells_lightning_rating": this.state.stairwells_lightning_rating,

      "stairwells_emergency_lightning": this.state.stairwells_emergency_lightning,
      "stairwells_emergency_lightning_rating": this.state.stairwells_emergency_lightning_rating,

      "stairwells_doors": this.state.stairwells_doors,
      "stairwells_doors_rating": this.state.stairwells_doors_rating,

      "stairwells_windows": this.state.stairwells_windows,
      "stairwells_windows_rating": this.state.stairwells_windows_rating,

      "stairwells_signs": this.state.stairwells_signs,
      "stairwells_signs_rating": this.state.stairwells_signs_rating,

      "emergency_exit_lightning": this.state.emergency_exit_lightning,
      "emergency_exit_lightning_rating": this.state.emergency_exit_lightning_rating,

      "emergency_exit_floor": this.state.emergency_exit_floor,
      "emergency_exit_floor_rating": this.state.emergency_exit_floor_rating,

      "emergency_exit_doors": this.state.emergency_exit_doors,
      "emergency_exit_doors_rating": this.state.emergency_exit_doors_rating,

      "emergency_exit_signs": this.state.emergency_exit_signs,
      "emergency_exit_signs_rating": this.state.emergency_exit_signs_rating,

      "roof_bulkhead": this.state.roof_bulkhead,
      "roof_bulkhead_rating": this.state.roof_bulkhead_rating,

      "roof_roof_doors": this.state.roof_roof_doors,
      "roof_roof_doors_rating": this.state.roof_roof_doors_rating,

      "roof_roof_surface": this.state.roof_roof_surface,
      "roof_roof_surface_rating": this.state.roof_roof_surface_rating,

      "roof_gutter": this.state.roof_gutter,
      "roof_gutter_rating": this.state.roof_gutter_rating,

      "roof__leaders": this.state.roof__leaders,
      "roof__leaders_rating": this.state.roof__leaders_rating,

      "roof_skylight": this.state.roof_skylight,
      "roof_skylight_rating": this.state.roof_skylight_rating,

      "roof_parapet": this.state.roof_parapet,
      "roof_parapet_rating": this.state.roof_parapet_rating,
      "fire_escapes_ladders": this.state.fire_escapes_ladders,
      "fire_escapes_ladders_rating": this.state.fire_escapes_ladders_rating,

      "fire_escapes_basket": this.state.fire_escapes_basket,
      "fire_escapes_basket_rating": this.state.fire_escapes_basket_rating,

      "fire_escapes_railling": this.state.fire_escapes_railling,
      "fire_escapes_railling_rating": this.state.fire_escapes_railling_rating,

      "fire_escapes_ladder_shoes": this.state.fire_escapes_ladder_shoes,
      "fire_escapes_ladder_shoes_rating": this.state.fire_escapes_ladder_shoes_rating,

      "lobbies_floor": this.state.lobbies_floor,
      "lobbies_floor_rating": this.state.lobbies_floor_rating,

      "lobbies_walls": this.state.lobbies_walls,
      "lobbies_walls_rating": this.state.lobbies_walls_rating,

      "lobbies_lightning_emergency": this.state.lobbies_lightning_emergency,
      "lobbies_lightning_emergency_rating": this.state.lobbies_lightning_emergency_rating,

      "lobbies_windows_sills": this.state.lobbies_windows_sills,
      "lobbies_windows_sills_rating": this.state.lobbies_windows_sills_rating,

      "basement_doors": this.state.basement_doors,
      "basement_doors_rating": this.state.basement_doors_rating,

      "basement_stairs": this.state.basement_stairs,
      "basement_stairs_rating": this.state.basement_stairs_rating,

      "basement_floor": this.state.basement_floor,
      "basement_floor_rating": this.state.basement_floor_rating,

      "basement_walls": this.state.basement_walls,
      "basement_walls_rating": this.state.basement_walls_rating,

      "basement_lightning": this.state.basement_lightning,
      "basement_lightning_rating": this.state.basement_lightning_rating,

      "basement_windows": this.state.basement_windows,
      "basement_windows_rating": this.state.basement_windows_rating,

      "basement_garbage_room": this.state.basement_garbage_room,
      "basement_garbage_room_rating": this.state.basement_garbage_room_rating,

      "laundry_room_equipment": this.state.laundry_room_equipment,
      "laundry_room_equipment_rating": this.state.laundry_room_equipment_rating,

      "laundry_room_floor": this.state.laundry_room_floor,
      "laundry_room_floor_rating": this.state.laundry_room_floor_rating,

      "laundry_room_walls": this.state.laundry_room_walls,
      "laundry_room_walls_rating": this.state.laundry_room_walls_rating,

      "laundry_room_lightning": this.state.laundry_room_lightning,
      "laundry_room_lightning_rating": this.state.laundry_room_lightning_rating,

      "boiler_fuel_supply": this.state.boiler_fuel_supply,
      "boiler_fuel_supply_rating": this.state.boiler_fuel_supply_rating,

      "boiler_gauge_color_coding_markings": this.state.boiler_gauge_color_coding_markings,
      "boiler_gauge_color_coding_markings_rating": this.state.boiler_gauge_color_coding_markings_rating,


      "boiler_nurners": this.state.boiler_nurners,
      "boiler_nurners_rating": this.state.boiler_nurners_rating,

      "boiler_coils": this.state.boiler_coils,
      "boiler_coils_rating": this.state.boiler_coils_rating,

      "boiler_oil_tank": this.state.boiler_oil_tank,
      "boiler_oil_tank_rating": this.state.boiler_oil_tank_rating,

      "boiler_plumbing": this.state.boiler_plumbing,
      "boiler_plumbing_rating": this.state.boiler_plumbing_rating,

      "boiler_leaks": this.state.boiler_leaks,
      "boiler_leaks_rating": this.state.boiler_leaks_rating,

      "boiler_overheads_returns": this.state.boiler_overheads_returns,
      "boiler_overheads_returns_rating": this.state.boiler_overheads_returns_rating,

      "meters_lightning": this.state.meters_lightning,
      "meters_lightning_rating": this.state.meters_lightning_rating,

      "meters_accessibility": this.state.meters_accessibility,
      "meters_accessibility_rating": this.state.meters_accessibility_rating,

      "meters_up_keeping": this.state.meters_up_keeping,
      "meters_up_keeping_rating": this.state.meters_up_keeping_rating,

      "elevator_lightning": this.state.elevator_lightning,
      "elevator_lightning_rating": this.state.elevator_lightning_rating,

      "elevator_signals": this.state.elevator_signals,
      "elevator_signals_rating": this.state.elevator_signals_rating,

      "elevator_doors": this.state.elevator_doors,
      "elevator_doors_rating": this.state.elevator_doors_rating,

      "elevator_cab_floor": this.state.elevator_cab_floor,
      "elevator_cab_floor_rating": this.state.elevator_cab_floor_rating,

      "elevator_cab_walls": this.state.elevator_cab_walls,
      "elevator_cab_walls_rating": this.state.elevator_cab_walls_rating,

      "elevator_cab_ceilling": this.state.elevator_cab_ceilling,
      "elevator_cab_ceilling_rating": this.state.elevator_cab_ceilling_rating,

      "elevator_floor_nmber_on_doors": this.state.elevator_floor_nmber_on_doors,
      "elevator_floor_nmber_on_doors_rating": this.state.elevator_floor_nmber_on_doors_rating,

      "fire_equipment_sprinklers": this.state.fire_equipment_sprinklers,
      "fire_equipment_sprinklers_rating": this.state.fire_equipment_sprinklers_rating,

      "fire_equipment_extinguishers": this.state.fire_equipment_extinguishers,
      "fire_equipment_extinguishers_rating": this.state.fire_equipment_extinguishers_rating,

      "fire_equipment_alarm_system": this.state.fire_equipment_alarm_system,
      "fire_equipment_alarm_system_rating": this.state.fire_equipment_alarm_system_rating,


      "vacant_units_apt1": this.state.vacant_units_apt1,
      "vacant_units_apt1_rating": this.state.vacant_units_apt1_rating,

      "vacant_units_apt2": this.state.vacant_units_apt2,
      "vacant_units_apt2_rating": this.state.vacant_units_apt2_rating,

      "inventory": this.state.inventory,
      "inventory_rating": this.state.inventory_rating,

      "problem": this.state.problem,
      "notes": this.state.notes,


    }

    let a = []
    Object.entries(body).forEach(function ([key, value]) {
      a.push('<tr><td>' + `${key}` + '</td><td>' + `${value}` + '</td></tr>')
    })


    let options = {
      // HTML Content for PDF.
      // I am putting all the HTML code in Single line but if you want to use large HTML code then you can use + Symbol to add them.
      //html: '<h1 style="text-align: center;"><strong>Welcome Guys</strong></h1><p style="text-align: center;">In This Tutorial we would learn about creating PDF File using HTML Text.</p><p style="text-align: center;"><strong>ReactNativeCode.com</strong></p>',
      // Setting UP File Name for PDF File.

      html: `<p>
          <h1 style="color:red;"><u>Build Inspection Details</u></h1></p>
          <table border=1> ${a} </table>`,

      fileName: 'test',

      //File directory in which the PDF File Will Store.
      directory: 'Albums',
    };

    let file = await RNHTMLtoPDF.convert(options);

    console.log(file.filePath);

    Alert.alert(file.filePath);

    this.setState({ filePath: file.filePath });
  }



  Call = () => {
    // if (this.isValidate) {

    let body = {
      'building': this.state.building,
      "week_of": this.state.week_of,
      "manager": this.state.manager,
      "supt": this.state.supt,
      "rating_choose_from": this.state.supt_rating,

      "exterior_cond_sidewalk_steps": this.state.exterior_cond_sidewalk_steps,
      "exterior_cond_sidewalk_steps_rating": this.state.exterior_cond_sidewalk_steps_rating,

      "exterior_cond_cans_cover": this.state.exterior_cond_cans_cover,
      "exterior_cond_cans_cover_rating": this.state.exterior_cond_cans_cover_rating,

      "exterior_cond_doors": this.state.exterior_cond_doors,
      "exterior_cond_doors_rating": this.state.exterior_cond_doors_rating,

      "exterior_cond_courtyard": this.state.exterior_cond_courtyard,
      "exterior_cond_courtyard_rating": this.state.exterior_cond_courtyard_rating,

      "exterior_cond_yard_drain": this.state.exterior_cond_yard_drain,
      "exterior_cond_yard_drain_rating": this.state.exterior_cond_yard_drain_rating,

      "exterior_cond_fountain_walls": this.state.exterior_cond_fountain_walls,
      "exterior_cond_fountain_walls_rating": this.state.exterior_cond_fountain_walls_rating,

      "exterior_cond_lights_at_entry": this.state.exterior_cond_lights_at_entry,
      "exterior_cond_lights_at_entry_rating": this.state.exterior_cond_lights_at_entry_rating,

      "exterior_cond_lights_at_yard": this.state.exterior_cond_lights_at_yard,
      "exterior_cond_lights_at_yard_rating": this.state.exterior_cond_lights_at_yard_rating,

      "entrance_entrance_door": this.state.entrance_entrance_door,
      "entrance_entrance_door_rating": this.state.entrance_entrance_door_rating,

      "entrance_sidelight": this.state.entrance_sidelight,
      "entrance_sidelight_rating": this.state.entrance_sidelight_rating,

      "entrance_intercom": this.state.entrance_intercom,
      "entrance_intercom_rating": this.state.entrance_intercom_rating,

      "entrance_mailboxes": this.state.entrance_mailboxes,
      "entrance_mailboxes_rating": this.state.entrance_mailboxes_rating,

      "entrance_floor": this.state.entrance_floor,
      "entrance_floor_rating": this.state.entrance_floor_rating,

      "entrance_walls": this.state.entrance_walls,
      "entrance_walls_rating": this.state.entrance_walls_rating,

      "entrance_lightning": this.state.entrance_lightning,
      "entrance_lightning_rating": this.state.entrance_lightning_rating,

      "stairwells_steps_treads": this.state.stairwells_steps_treads,
      "stairwells_steps_treads_rating": this.state.stairwells_steps_treads_rating,

      "stairwells_walls": this.state.stairwells_walls,
      "stairwells_walls_rating": this.state.stairwells_walls_rating,

      "stairwells_lightning": this.state.stairwells_lightning,
      "stairwells_lightning_rating": this.state.stairwells_lightning_rating,

      "stairwells_emergency_lightning": this.state.stairwells_emergency_lightning,
      "stairwells_emergency_lightning_rating": this.state.stairwells_emergency_lightning_rating,

      "stairwells_doors": this.state.stairwells_doors,
      "stairwells_doors_rating": this.state.stairwells_doors_rating,

      "stairwells_windows": this.state.stairwells_windows,
      "stairwells_windows_rating": this.state.stairwells_windows_rating,

      "stairwells_signs": this.state.stairwells_signs,
      "stairwells_signs_rating": this.state.stairwells_signs_rating,

      "emergency_exit_lightning": this.state.emergency_exit_lightning,
      "emergency_exit_lightning_rating": this.state.emergency_exit_lightning_rating,

      "emergency_exit_floor": this.state.emergency_exit_floor,
      "emergency_exit_floor_rating": this.state.emergency_exit_floor_rating,

      "emergency_exit_doors": this.state.emergency_exit_doors,
      "emergency_exit_doors_rating": this.state.emergency_exit_doors_rating,

      "emergency_exit_signs": this.state.emergency_exit_signs,
      "emergency_exit_signs_rating": this.state.emergency_exit_signs_rating,

      "roof_bulkhead": this.state.roof_bulkhead,
      "roof_bulkhead_rating": this.state.roof_bulkhead_rating,

      "roof_roof_doors": this.state.roof_roof_doors,
      "roof_roof_doors_rating": this.state.roof_roof_doors_rating,

      "roof_roof_surface": this.state.roof_roof_surface,
      "roof_roof_surface_rating": this.state.roof_roof_surface_rating,

      "roof_gutter": this.state.roof_gutter,
      "roof_gutter_rating": this.state.roof_gutter_rating,

      "roof__leaders": this.state.roof__leaders,
      "roof__leaders_rating": this.state.roof__leaders_rating,

      "roof_skylight": this.state.roof_skylight,
      "roof_skylight_rating": this.state.roof_skylight_rating,

      "roof_parapet": this.state.roof_parapet,
      "roof_parapet_rating": this.state.roof_parapet_rating,
      "fire_escapes_ladders": this.state.fire_escapes_ladders,
      "fire_escapes_ladders_rating": this.state.fire_escapes_ladders_rating,

      "fire_escapes_basket": this.state.fire_escapes_basket,
      "fire_escapes_basket_rating": this.state.fire_escapes_basket_rating,

      "fire_escapes_railling": this.state.fire_escapes_railling,
      "fire_escapes_railling_rating": this.state.fire_escapes_railling_rating,

      "fire_escapes_ladder_shoes": this.state.fire_escapes_ladder_shoes,
      "fire_escapes_ladder_shoes_rating": this.state.fire_escapes_ladder_shoes_rating,

      "lobbies_floor": this.state.lobbies_floor,
      "lobbies_floor_rating": this.state.lobbies_floor_rating,

      "lobbies_walls": this.state.lobbies_walls,
      "lobbies_walls_rating": this.state.lobbies_walls_rating,

      "lobbies_lightning_emergency": this.state.lobbies_lightning_emergency,
      "lobbies_lightning_emergency_rating": this.state.lobbies_lightning_emergency_rating,

      "lobbies_windows_sills": this.state.lobbies_windows_sills,
      "lobbies_windows_sills_rating": this.state.lobbies_windows_sills_rating,

      "basement_doors": this.state.basement_doors,
      "basement_doors_rating": this.state.basement_doors_rating,

      "basement_stairs": this.state.basement_stairs,
      "basement_stairs_rating": this.state.basement_stairs_rating,

      "basement_floor": this.state.basement_floor,
      "basement_floor_rating": this.state.basement_floor_rating,

      "basement_walls": this.state.basement_walls,
      "basement_walls_rating": this.state.basement_walls_rating,

      "basement_lightning": this.state.basement_lightning,
      "basement_lightning_rating": this.state.basement_lightning_rating,

      "basement_windows": this.state.basement_windows,
      "basement_windows_rating": this.state.basement_windows_rating,

      "basement_garbage_room": this.state.basement_garbage_room,
      "basement_garbage_room_rating": this.state.basement_garbage_room_rating,

      "laundry_room_equipment": this.state.laundry_room_equipment,
      "laundry_room_equipment_rating": this.state.laundry_room_equipment_rating,

      "laundry_room_floor": this.state.laundry_room_floor,
      "laundry_room_floor_rating": this.state.laundry_room_floor_rating,

      "laundry_room_walls": this.state.laundry_room_walls,
      "laundry_room_walls_rating": this.state.laundry_room_walls_rating,

      "laundry_room_lightning": this.state.laundry_room_lightning,
      "laundry_room_lightning_rating": this.state.laundry_room_lightning_rating,

      "boiler_fuel_supply": this.state.boiler_fuel_supply,
      "boiler_fuel_supply_rating": this.state.boiler_fuel_supply_rating,

      "boiler_gauge_color_coding_markings": this.state.boiler_gauge_color_coding_markings,
      "boiler_gauge_color_coding_markings_rating": this.state.boiler_gauge_color_coding_markings_rating,


      "boiler_nurners": this.state.boiler_nurners,
      "boiler_nurners_rating": this.state.boiler_nurners_rating,

      "boiler_coils": this.state.boiler_coils,
      "boiler_coils_rating": this.state.boiler_coils_rating,

      "boiler_oil_tank": this.state.boiler_oil_tank,
      "boiler_oil_tank_rating": this.state.boiler_oil_tank_rating,

      "boiler_plumbing": this.state.boiler_plumbing,
      "boiler_plumbing_rating": this.state.boiler_plumbing_rating,

      "boiler_leaks": this.state.boiler_leaks,
      "boiler_leaks_rating": this.state.boiler_leaks_rating,

      "boiler_overheads_returns": this.state.boiler_overheads_returns,
      "boiler_overheads_returns_rating": this.state.boiler_overheads_returns_rating,

      "meters_lightning": this.state.meters_lightning,
      "meters_lightning_rating": this.state.meters_lightning_rating,

      "meters_accessibility": this.state.meters_accessibility,
      "meters_accessibility_rating": this.state.meters_accessibility_rating,

      "meters_up_keeping": this.state.meters_up_keeping,
      "meters_up_keeping_rating": this.state.meters_up_keeping_rating,

      "elevator_lightning": this.state.elevator_lightning,
      "elevator_lightning_rating": this.state.elevator_lightning_rating,

      "elevator_signals": this.state.elevator_signals,
      "elevator_signals_rating": this.state.elevator_signals_rating,

      "elevator_doors": this.state.elevator_doors,
      "elevator_doors_rating": this.state.elevator_doors_rating,

      "elevator_cab_floor": this.state.elevator_cab_floor,
      "elevator_cab_floor_rating": this.state.elevator_cab_floor_rating,

      "elevator_cab_walls": this.state.elevator_cab_walls,
      "elevator_cab_walls_rating": this.state.elevator_cab_walls_rating,

      "elevator_cab_ceilling": this.state.elevator_cab_ceilling,
      "elevator_cab_ceilling_rating": this.state.elevator_cab_ceilling_rating,

      "elevator_floor_nmber_on_doors": this.state.elevator_floor_nmber_on_doors,
      "elevator_floor_nmber_on_doors_rating": this.state.elevator_floor_nmber_on_doors_rating,

      "fire_equipment_sprinklers": this.state.fire_equipment_sprinklers,
      "fire_equipment_sprinklers_rating": this.state.fire_equipment_sprinklers_rating,

      "fire_equipment_extinguishers": this.state.fire_equipment_extinguishers,
      "fire_equipment_extinguishers_rating": this.state.fire_equipment_extinguishers_rating,

      "fire_equipment_alarm_system": this.state.fire_equipment_alarm_system,
      "fire_equipment_alarm_system_rating": this.state.fire_equipment_alarm_system_rating,


      "vacant_units_apt1": this.state.vacant_units_apt1,
      "vacant_units_apt1_rating": this.state.vacant_units_apt1_rating,

      "vacant_units_apt2": this.state.vacant_units_apt2,
      "vacant_units_apt2_rating": this.state.vacant_units_apt2_rating,

      "inventory": this.state.inventory,
      "inventory_rating": this.state.inventory_rating,

      "problem": this.state.problem,
      "notes": this.state.notes,


    }

    /*----------------------------------------------------------- */
    var formdata = new FormData();
    formdata.append("building", this.state.building);
    formdata.append("week_of", this.state.week_of);
    formdata.append("manager", this.state.manager);
    formdata.append("supt", this.state.supt);
    formdata.append("rating_choose_from", this.state.supt_rating);

    formdata.append("exterior_cond_sidewalk_steps", this.state.exterior_cond_sidewalk_steps);
    formdata.append("exterior_cond_sidewalk_steps_rating", this.state.exterior_cond_sidewalk_steps_rating);

    formdata.append("exterior_cond_cans_cover", this.state.exterior_cond_cans_cover);
    formdata.append("exterior_cond_cans_cover_rating", this.state.exterior_cond_cans_cover_rating);

    formdata.append("exterior_cond_doors", this.state.exterior_cond_doors);
    formdata.append("exterior_cond_doors_rating", this.state.exterior_cond_doors_rating);

    formdata.append("exterior_cond_courtyard", this.state.exterior_cond_courtyard);
    formdata.append("exterior_cond_courtyard_rating", this.state.exterior_cond_courtyard_rating);

    formdata.append("exterior_cond_yard_drain", this.state.exterior_cond_yard_drain);
    formdata.append("exterior_cond_yard_drain_rating", this.state.exterior_cond_yard_drain_rating);

    formdata.append("exterior_cond_fountain_walls", this.state.exterior_cond_fountain_walls);
    formdata.append("exterior_cond_fountain_walls_rating", this.state.exterior_cond_fountain_walls_rating);

    formdata.append("exterior_cond_lights_at_entry", this.state.exterior_cond_lights_at_entry);
    formdata.append("exterior_cond_lights_at_entry_rating", this.state.exterior_cond_lights_at_entry_rating);

    formdata.append("exterior_cond_lights_at_yard", this.state.exterior_cond_lights_at_yard);
    formdata.append("exterior_cond_lights_at_yard_rating", this.state.exterior_cond_lights_at_yard_rating);

    formdata.append("entrance_entrance_door", this.state.entrance_entrance_door);
    formdata.append("entrance_entrance_door_rating", this.state.entrance_entrance_door_rating);

    formdata.append("entrance_sidelight", this.state.entrance_sidelight);
    formdata.append("entrance_sidelight_rating", this.state.entrance_sidelight_rating);

    formdata.append("entrance_intercom", this.state.entrance_intercom);
    formdata.append("entrance_intercom_rating", this.state.entrance_intercom_rating);

    formdata.append("entrance_mailboxes", this.state.entrance_mailboxes);
    formdata.append("entrance_mailboxes_rating", this.state.entrance_mailboxes_rating);

    formdata.append("entrance_floor", this.state.entrance_floor);
    formdata.append("entrance_floor_rating", this.state.entrance_floor_rating);

    formdata.append("entrance_walls", this.state.entrance_walls);
    formdata.append("entrance_walls_rating", this.state.entrance_walls_rating);

    formdata.append("entrance_lightning", this.state.entrance_lightning);
    formdata.append("entrance_lightning_rating", this.state.entrance_lightning_rating);

    formdata.append("stairwells_steps_treads", this.state.stairwells_steps_treads);
    formdata.append("stairwells_steps_treads_rating", this.state.stairwells_steps_treads_rating);

    formdata.append("stairwells_walls", this.state.stairwells_walls);
    formdata.append("stairwells_walls_rating", this.state.stairwells_walls_rating);

    formdata.append("stairwells_lightning", this.state.stairwells_lightning);
    formdata.append("stairwells_lightning_rating", this.state.stairwells_lightning_rating);

    formdata.append("stairwells_emergency_lightning", this.state.stairwells_emergency_lightning);
    formdata.append("stairwells_emergency_lightning_rating", this.state.stairwells_emergency_lightning_rating);

    formdata.append("stairwells_doors", this.state.stairwells_doors);
    formdata.append("stairwells_doors_rating", this.state.stairwells_doors_rating);

    formdata.append("stairwells_windows", this.state.stairwells_windows);
    formdata.append("stairwells_windows_rating", this.state.stairwells_windows_rating);

    formdata.append("stairwells_signs", this.state.stairwells_signs);
    formdata.append("stairwells_signs_rating", this.state.stairwells_signs_rating);

    formdata.append("emergency_exit_lightning", this.state.emergency_exit_lightning);
    formdata.append("emergency_exit_lightning_rating", this.state.emergency_exit_lightning_rating);

    formdata.append("emergency_exit_floor", this.state.emergency_exit_floor);
    formdata.append("emergency_exit_floor_rating", this.state.emergency_exit_floor_rating);

    formdata.append("emergency_exit_doors", this.state.emergency_exit_doors);
    formdata.append("emergency_exit_doors_rating", this.state.emergency_exit_doors_rating);

    formdata.append("emergency_exit_signs", this.state.emergency_exit_signs);
    formdata.append("emergency_exit_signs_rating", this.state.emergency_exit_signs_rating);

    formdata.append("roof_bulkhead", this.state.roof_bulkhead);
    formdata.append("roof_bulkhead_rating", this.state.roof_bulkhead_rating);

    formdata.append("roof_roof_doors", this.state.roof_roof_doors);
    formdata.append("roof_roof_doors_rating", this.state.roof_roof_doors_rating);

    formdata.append("roof_roof_surface", this.state.roof_roof_surface);
    formdata.append("roof_roof_surface_rating", this.state.roof_roof_surface_rating);

    formdata.append("roof_gutter", this.state.roof_gutter);
    formdata.append("roof_gutter_rating", this.state.roof_gutter_rating);

    formdata.append("roof__leaders", this.state.roof__leaders);
    formdata.append("roof__leaders_rating", this.state.roof__leaders_rating);

    formdata.append("roof_skylight", this.state.roof_skylight);
    formdata.append("roof_skylight_rating", this.state.roof_skylight_rating);

    formdata.append("roof_parapet", this.state.roof_parapet);
    formdata.append("roof_parapet_rating", this.state.roof_parapet_rating);

    formdata.append("fire_escapes_ladders", this.state.fire_escapes_ladders);
    formdata.append("fire_escapes_ladders_rating", this.state.fire_escapes_ladders_rating);

    formdata.append("fire_escapes_basket", this.state.fire_escapes_basket);
    formdata.append("fire_escapes_basket_rating", this.state.fire_escapes_basket_rating);

    formdata.append("fire_escapes_railling", this.state.fire_escapes_railling);
    formdata.append("fire_escapes_railling_rating", this.state.fire_escapes_railling_rating);

    formdata.append("fire_escapes_ladder_shoes", this.state.fire_escapes_ladder_shoes);
    formdata.append("fire_escapes_ladder_shoes_rating", this.state.fire_escapes_ladder_shoes_rating);

    formdata.append("lobbies_floor", this.state.lobbies_floor);
    formdata.append("lobbies_floor_rating", this.state.lobbies_floor_rating);

    formdata.append("lobbies_walls", this.state.lobbies_walls);
    formdata.append("lobbies_walls_rating", this.state.lobbies_walls_rating);

    formdata.append("lobbies_lightning_emergency", this.state.lobbies_lightning_emergency);
    formdata.append("lobbies_lightning_emergency_rating", this.state.lobbies_lightning_emergency_rating);

    formdata.append("lobbies_windows_sills", this.state.lobbies_windows_sills);
    formdata.append("lobbies_windows_sills_rating", this.state.lobbies_windows_sills_rating);

    formdata.append("basement_doors", this.state.basement_doors);
    formdata.append("basement_doors_rating", this.state.basement_doors_rating);

    formdata.append("basement_stairs", this.state.basement_stairs);
    formdata.append("basement_stairs_rating", this.state.basement_stairs_rating);

    formdata.append("basement_floor", this.state.basement_floor);
    formdata.append("basement_floor_rating", this.state.basement_floor_rating);

    formdata.append("basement_walls", this.state.basement_walls);
    formdata.append("basement_walls_rating", this.state.basement_walls_rating);

    formdata.append("basement_lightning", this.state.basement_lightning);
    formdata.append("basement_lightning_rating", this.state.basement_lightning_rating);

    formdata.append("basement_windows", this.state.basement_windows);
    formdata.append("basement_windows_rating", this.state.basement_windows_rating);

    formdata.append("basement_garbage_room", this.state.basement_garbage_room);
    formdata.append("basement_garbage_room_rating", this.state.basement_garbage_room_rating);

    formdata.append("laundry_room_equipment", this.state.laundry_room_equipment);
    formdata.append("laundry_room_equipment_rating", this.state.laundry_room_equipment_rating);

    formdata.append("laundry_room_floor", this.state.laundry_room_floor);
    formdata.append("laundry_room_floor_rating", this.state.laundry_room_floor_rating);

    formdata.append("laundry_room_walls", this.state.laundry_room_walls);
    formdata.append("laundry_room_walls_rating", this.state.laundry_room_walls_rating);

    formdata.append("laundry_room_lightning", this.state.laundry_room_lightning);
    formdata.append("laundry_room_lightning_rating", this.state.laundry_room_lightning_rating);

    formdata.append("boiler_fuel_supply", this.state.boiler_fuel_supply);
    formdata.append("boiler_fuel_supply_rating", this.state.boiler_fuel_supply_rating);

    formdata.append("boiler_gauge_color_coding_markings", this.state.boiler_gauge_color_coding_markings);
    formdata.append("boiler_gauge_color_coding_markings_rating", this.state.boiler_gauge_color_coding_markings_rating);


    formdata.append("boiler_nurners", this.state.boiler_nurners);
    formdata.append("boiler_nurners_rating", this.state.boiler_nurners_rating);

    formdata.append("boiler_coils", this.state.boiler_coils);
    formdata.append("boiler_coils_rating", this.state.boiler_coils_rating);

    formdata.append("boiler_oil_tank", this.state.boiler_oil_tank);
    formdata.append("boiler_oil_tank_rating", this.state.boiler_oil_tank_rating);

    formdata.append("boiler_plumbing", this.state.boiler_plumbing);
    formdata.append("boiler_plumbing_rating", this.state.boiler_plumbing_rating);

    formdata.append("boiler_leaks", this.state.boiler_leaks);
    formdata.append("boiler_leaks_rating", this.state.boiler_leaks_rating);

    formdata.append("boiler_overheads_returns", this.state.boiler_overheads_returns);
    formdata.append("boiler_overheads_returns_rating", this.state.boiler_overheads_returns_rating);

    formdata.append("meters_lightning", this.state.meters_lightning);
    formdata.append("meters_lightning_rating", this.state.meters_lightning_rating);

    formdata.append("meters_accessibility", this.state.meters_accessibility);
    formdata.append("meters_accessibility_rating", this.state.meters_accessibility_rating);

    formdata.append("meters_up_keeping", this.state.meters_up_keeping);
    formdata.append("meters_up_keeping_rating", this.state.meters_up_keeping_rating);

    formdata.append("elevator_lightning", this.state.elevator_lightning);
    formdata.append("elevator_lightning_rating", this.state.elevator_lightning_rating);

    formdata.append("elevator_signals", this.state.elevator_signals);
    formdata.append("elevator_signals_rating", this.state.elevator_signals_rating);

    formdata.append("elevator_doors", this.state.elevator_doors);
    formdata.append("elevator_doors_rating", this.state.elevator_doors_rating);

    formdata.append("elevator_cab_floor", this.state.elevator_cab_floor);
    formdata.append("elevator_cab_floor_rating", this.state.elevator_cab_floor_rating);

    formdata.append("elevator_cab_walls", this.state.elevator_cab_walls);
    formdata.append("elevator_cab_walls_rating", this.state.elevator_cab_walls_rating);

    formdata.append("elevator_cab_ceilling", this.state.elevator_cab_ceilling);
    formdata.append("elevator_cab_ceilling_rating", this.state.elevator_cab_ceilling_rating);

    formdata.append("elevator_floor_nmber_on_doors", this.state.elevator_floor_nmber_on_doors);
    formdata.append("elevator_floor_nmber_on_doors_rating", this.state.elevator_floor_nmber_on_doors_rating);

    formdata.append("fire_equipment_sprinklers", this.state.fire_equipment_sprinklers);
    formdata.append("fire_equipment_sprinklers_rating", this.state.fire_equipment_sprinklers_rating);

    formdata.append("fire_equipment_extinguishers", this.state.fire_equipment_extinguishers);
    formdata.append("fire_equipment_extinguishers_rating", this.state.fire_equipment_extinguishers_rating);

    formdata.append("fire_equipment_alarm_system", this.state.fire_equipment_alarm_system);
    formdata.append("fire_equipment_alarm_system_rating", this.state.fire_equipment_alarm_system_rating);


    formdata.append("vacant_units_apt1", this.state.vacant_units_apt1);
    formdata.append("vacant_units_apt1_rating", this.state.vacant_units_apt1_rating);

    formdata.append("vacant_units_apt2", this.state.vacant_units_apt2);
    formdata.append("vacant_units_apt2_rating", this.state.vacant_units_apt2_rating);

    formdata.append("inventory", this.state.inventory);
    formdata.append("inventory_rating", this.state.inventory_rating);

    formdata.append("problem", this.state.problem);
    formdata.append("notes", this.state.notes);


    var requestOptions = {
      method: 'POST',
      body: formdata,
      redirect: 'follow'
    };

    this.setState({ pdf_data: formdata })
    this.setState({ loading: true })


    fetch("http://webmobril.org/dev/inspectionapp/api/inspection", requestOptions)
      .then(response => response.json())
      .then((response) => {
        this.setState({ loading: false })
        console.log("inspection response=-=+++++=->>", response)
        // this.textInput.clear()
        this.setState({
         
          pdf_data: [],
          selected: '',
          value: '',
    
          building: '',
          week_of: '', 
          manager: '',
    
          /*------------------------ Supt -------------------------------------------------- */
          supt: '',
          supt_rating: '',
    
          /* ------------------------- Exterior ------------------------------------------ */
          exterior_cond_sidewalk_steps: '',
          exterior_cond_cans_cover: '',
          exterior_cond_doors: '',
          exterior_cond_courtyard: '',
          exterior_cond_yard_drain: '',
          exterior_cond_fountain_walls: '',
          exterior_cond_lights_at_entry: '',
          exterior_cond_lights_at_yard: '',
    
          exterior_cond_sidewalk_steps_rating: '',
          exterior_cond_cans_cover_rating: '',
          exterior_cond_doors_rating: '',
          exterior_cond_courtyard_rating: '',
          exterior_cond_yard_drain_rating: '',
          exterior_cond_fountain_walls_rating: '',
          exterior_cond_lights_at_entry_rating: '',
          exterior_cond_lights_at_yard_rating: '',
          /*---------------------Entrance --------------------------------------------- */
          entrance_entrance_door: '', entrance_entrance_door_rating: '',
          entrance_sidelight: '', entrance_sidelight_rating: '',
          entrance_intercom: '', entrance_intercom_rating: '',
          entrance_mailboxes: '', entrance_mailboxes_rating: '',
          entrance_floor: '', entrance_floor_rating: '',
          entrance_walls: '', entrance_walls_rating: '',
          entrance_lightning: '', entrance_lightning_rating: '',
    
          /*-----------------Stairwells------------------------ */
          stairwells_steps_treads: '', stairwells_steps_treads_rating: '',
          stairwells_walls: '', stairwells_walls_rating: '',
          stairwells_lightning: '', stairwells_lightning_rating: '',
          stairwells_emergency_lightning: '', stairwells_emergency_lightning_rating: '',
          stairwells_doors: '', stairwells_doors_rating: '',
          stairwells_windows: '', stairwells_windows_rating: '',
          stairwells_signs: '', stairwells_signs_rating: '',
    
          /*----------------------Emergency------------------- */
          emergency_exit_lightning: '',
          emergency_exit_floor: '',
          emergency_exit_doors: '',
          emergency_exit_signs: '',
    
          emergency_exit_lightning_rating: '',
          emergency_exit_floor_rating: '',
          emergency_exit_doors_rating: '',
          emergency_exit_signs_rating: '',
    
          /*--------------------Roof ---------------------------- */
          roof_bulkhead: '',
          roof_roof_doors: '',
          roof_roof_surface: '',
          roof_gutter: '',
          roof__leaders: '',
          roof_skylight: '',
          roof_parapet: '',
    
    
          roof_bulkhead_rating: '',
          roof_roof_doors_rating: '',
          roof_roof_surface_rating: '',
          roof_gutter_rating: '',
          roof__leaders_rating: '',
          roof_skylight_rating: '',
          roof_parapet_rating: '',
    
          /*------------------------------ fire escapes ----------------------------------- */
          fire_escapes_ladders: '',
          fire_escapes_basket: '',
          fire_escapes_railling: '',
          fire_escapes_ladder_shoes: '',
    
          fire_escapes_ladders_rating: '',
          fire_escapes_basket_rating: '',
          fire_escapes_railling_rating: '',
          fire_escapes_ladder_shoes_rating: '',
    
          /*----------------------------- Lobbies -------------------------------------------- */
    
          lobbies_floor: '',
          lobbies_walls: '',
          lobbies_lightning_emergency: '',
          lobbies_windows_sills: '',
    
    
          lobbies_floor_rating: '',
          lobbies_walls_rating: '',
          lobbies_lightning_emergency_rating: '',
          lobbies_windows_sills_rating: '',
    
          /* --------------------------------- basement ----------------------------------------------- */
          basement_doors: '',
          basement_stairs: '',
          basement_floor: '',
          basement_walls: '',
          basement_lightning: '',
          basement_windows: '',
          basement_garbage_room: '',
    
    
          basement_doors_rating: '',
          basement_stairs_rating: '',
          basement_floor_rating: '',
          basement_walls_rating: '',
          basement_lightning_rating: '',
          basement_windows_rating: '',
          basement_garbage_room_rating: '',
    
          /*--------------------------------------- Laundry Room --------------------------------------- */
    
          laundry_room_equipment: '',
          laundry_room_floor: '',
          laundry_room_walls: '',
          laundry_room_lightning: '',
    
          laundry_room_equipment_rating: '',
          laundry_room_floor_rating: '',
          laundry_room_walls_rating: '',
          laundry_room_lightning_rating: '',
    
          /*------------------------------------ Boiler------------------------------------------------- */
    
          boiler_fuel_supply: '',
          boiler_gauge_color_coding_markings: '',
          boiler_nurners: '',
          boiler_coils: '',
          boiler_oil_tank: '',
          boiler_plumbing: '',
          boiler_leaks: '',
          boiler_overheads_returns: '',
    
    
          boiler_fuel_supply_rating: '',
          boiler_gauge_color_coding_markings_rating: '',
          boiler_nurners_rating: '',
          boiler_coils_rating: '',
          boiler_oil_tank_rating: '',
          boiler_plumbing_rating: '',
          boiler_leaks_rating: '',
          boiler_overheads_returns_rating: '',
    
          /*------------------------------------ Meters ------------------------------------------------------- */
    
          meters_lightning: '',
          meters_accessibility: '',
          meters_up_keeping: '',
    
    
          meters_lightning_rating: '',
          meters_accessibility_rating: '',
          meters_up_keeping_rating: '',
    
          /*--------------------------- Elevator ------------------------------------------------------ */
    
          elevator_lightning: '',
          elevator_signals: '',
          elevator_doors: '',
          elevator_cab_floor: '',
          elevator_cab_walls: '',
          elevator_cab_ceilling: '',
          elevator_floor_nmber_on_doors: '',
    
    
          elevator_lightning_rating: '',
          elevator_signals_rating: '',
          elevator_doors_rating: '',
          elevator_cab_floor_rating: '',
          elevator_cab_walls_rating: '',
          elevator_cab_ceilling_rating: '',
          elevator_floor_nmber_on_doors_rating: '',
    
          /* -------------------------------------------------Fire Equipments --------------------------------------- */
          fire_equipment_sprinklers: '',
          fire_equipment_extinguishers: '',
          fire_equipment_alarm_system: '',
    
          fire_equipment_sprinklers_rating: '',
          fire_equipment_extinguishers_rating: '',
          fire_equipment_alarm_system_rating: '',
    
          /*-------------------------------------- Vacant---------------------------------------- */
          vacant_units_apt1: '',
          vacant_units_apt1_rating: '',
          vacant_units_apt2: '',
          vacant_units_apt2_rating: '',
    
          inventory: '',
          inventory_rating: '',
    
          problem: '',
          notes: '',
    
    
          selectedChooseRating: '',
    
        })
      })
      .catch(error => {
        this.setState({ loading: false })
      });
    // }
    //else { Alert('Fields are missing') }
  }


  isValidate = () => {
    console.log("validating")
    if (!validateFields(this.state.building, 'Building field')) {
      return false
    }
    else if (!validateDate(this.state.week_of, 'Date field')) {
      return false
    }
    else if (!validateFields(this.state.manager, 'Manager field')) {
      return false
    }

    else if (!validateFields(this.state.supt, 'Supt Field')) {
      return false
    }
    else if (!validateFields(this.state.supt_rating, 'Supt rating')) {
      return false
    }
    else if (!validateFields(this.state.exterior_cond_sidewalk_steps, 'exterior_cond_sidewalk_steps field')) {
      return false
    }
    else if (!validateFields(this.state.exterior_cond_sidewalk_steps_rating, 'exterior_cond_sidewalk_steps_rating')) {
      return false
    }

    else if (!validateFields(this.state.exterior_cond_cans_cover, 'exterior_cond_cans_cover field')) {
      return false
    }
    else if (!validateFields(this.state.exterior_cond_cans_cover_rating, 'exterior_cond_cans_cover_rating')) {
      return false
    }

    else if (!validateFields(this.state.exterior_cond_doors, 'exterior_cond_doors field')) {
      return false
    }
    else if (!validateFields(this.state.exterior_cond_doors_rating, 'exterior_cond_doors_rating')) {
      return false
    }

    else if (!validateFields(this.state.exterior_cond_courtyard, 'exterior_cond_courtyard field')) {
      return false
    }
    else if (!validateFields(this.state.exterior_cond_courtyard_rating, 'exterior_cond_courtyard_rating')) {
      return false
    }

    else if (!validateFields(this.state.exterior_cond_yard_drain, 'exterior_cond_yard_drain field')) {
      return false
    }
    else if (!validateFields(this.state.exterior_cond_yard_drain_rating, 'exterior_cond_yard_drain_rating')) {
      return false
    }

    else if (!validateFields(this.state.exterior_cond_fountain_walls, 'exterior_cond_fountain_walls field')) {
      return false
    }
    else if (!validateFields(this.state.exterior_cond_fountain_walls_rating, 'exterior_cond_fountain_walls_rating')) {
      return false
    }

    else if (!validateFields(this.state.exterior_cond_lights_at_entry, 'exterior_cond_lights_at_entry field')) {
      return false
    }
    else if (!validateFields(this.state.exterior_cond_lights_at_entry_rating, 'exterior_cond_lights_at_entry_rating')) {
      return false
    }

    else if (!validateFields(this.state.exterior_cond_lights_at_yard, 'exterior_cond_lights_at_yard field')) {
      return false
    }
    else if (!validateFields(this.state.exterior_cond_lights_at_yard_rating, 'exterior_cond_lights_at_yard_rating')) {
      return false
    }

    else if (!validateFields(this.state.entrance_entrance_door, 'entrance_entrance_door field')) {
      return false
    }
    else if (!validateFields(this.state.entrance_entrance_door_rating, 'entrance_entrance_door_rating')) {
      return false
    }

    else if (!validateFields(this.state.entrance_sidelight, 'entrance_sidelight field')) {
      return false
    }
    else if (!validateFields(this.state.entrance_sidelight_rating, 'entrance_sidelight_rating')) {
      return false
    }



    else if (!validateFields(this.state.entrance_intercom, 'entrance_intercom field')) {
      return false
    }
    else if (!validateFields(this.state.entrance_intercom_rating, 'entrance_intercom_rating')) {
      return false
    }
    else if (!validateFields(this.state.entrance_mailboxes, 'entrance_mailboxes field')) {
      return false
    }
    else if (!validateFields(this.state.entrance_mailboxes_rating, 'entrance_mailboxes_rating')) {
      return false
    }

    else if (!validateFields(this.state.entrance_floor, 'entrance_floor field')) {
      return false
    }
    else if (!validateFields(this.state.entrance_floor_rating, 'entrance_floor_rating')) {
      return false
    }
    else if (!validateFields(this.state.entrance_walls, 'entrance_walls field')) {
      return false
    }
    else if (!validateFields(this.state.entrance_walls_rating, 'entrance_walls_rating')) {
      return false
    }
    else if (!validateFields(this.state.entrance_lightning, 'entrance_lightning field')) {
      return false
    }
    else if (!validateFields(this.state.entrance_lightning_rating, 'entrance_lightning_rating')) {
      return false
    }
    else if (!validateFields(this.state.stairwells_steps_treads, 'stairwells_steps_treads field')) {
      return false
    }
    else if (!validateFields(this.state.stairwells_steps_treads_rating, 'stairwells_steps_treads_rating')) {
      return false
    }
    else if (!validateFields(this.state.stairwells_walls, 'stairwells_walls field')) {
      return false
    }
    else if (!validateFields(this.state.stairwells_walls_rating, 'stairwells_walls_rating_rating')) {
      return false
    }
    else if (!validateFields(this.state.stairwells_lightning, 'stairwells_lightning field')) {
      return false
    }
    else if (!validateFields(this.state.stairwells_lightning_rating, 'stairwells_lightning_rating')) {
      return false
    }
    else if (!validateFields(this.state.stairwells_emergency_lightning, 'stairwells_emergency_lightning field')) {
      return false
    }
    else if (!validateFields(this.state.stairwells_emergency_lightning_rating, 'stairwells_emergency_lightning_rating')) {
      return false
    }

    else if (!validateFields(this.state.stairwells_doors, 'stairwells_doors field')) {
      return false
    }
    else if (!validateFields(this.state.stairwells_doors_rating, 'stairwells_doors_rating')) {
      return false
    }


    else if (!validateFields(this.state.stairwells_windows, 'stairwells_windows field')) {
      return false
    }
    else if (!validateFields(this.state.stairwells_windows_rating, 'stairwells_windows_rating')) {
      return false
    }

    else if (!validateFields(this.state.stairwells_signs, 'stairwells_signs field')) {
      return false
    }
    else if (!validateFields(this.state.stairwells_signs_rating, 'stairwells_signs_rating')) {
      return false
    }


    else if (!validateFields(this.state.emergency_exit_lightning, 'emergency_exit_lightning field')) {
      return false
    } else if (!validateFields(this.state.emergency_exit_lightning_rating, 'emergency_exit_lightning_rating')) {
      return false
    }

    else if (!validateFields(this.state.emergency_exit_floor, 'emergency_exit_floor field')) {
      return false
    } else if (!validateFields(this.state.emergency_exit_floor_rating, 'emergency_exit_floor_rating')) {
      return false
    }


    else if (!validateFields(this.state.emergency_exit_doors, 'emergency_exit_doors field')) {
      return false
    } else if (!validateFields(this.state.emergency_exit_doors_rating, 'emergency_exit_doors_rating')) {
      return false
    }

    else if (!validateFields(this.state.emergency_exit_signs, 'emergency_exit_signs field')) {
      return false
    } else if (!validateFields(this.state.emergency_exit_signs_rating, 'emergency_exit_signs_rating')) {
      return false
    }

    else if (!validateFields(this.state.roof_bulkhead, 'roof_bulkhead field')) {
      return false
    } else if (!validateFields(this.state.roof_bulkhead_rating, 'roof_bulkhead_rating')) {
      return false
    }


    else if (!validateFields(this.state.roof_roof_doors, 'roof_roof_doors field')) {
      return false
    } else if (!validateFields(this.state.roof_roof_doors_rating, 'roof_roof_doors_rating')) {
      return false
    }


    else if (!validateFields(this.state.roof_roof_surface, 'roof_roof_surface field')) {
      return false
    } else if (!validateFields(this.state.roof_roof_surface_rating, 'roof_roof_surface_rating')) {
      return false
    }

    else if (!validateFields(this.state.roof_gutter, 'roof_gutter field')) {
      return false
    } else if (!validateFields(this.state.roof_gutter_rating, 'roof_gutter_rating')) {
      return false
    }


    else if (!validateFields(this.state.roof__leaders, 'roof__leaders field')) {
      return false
    } else if (!validateFields(this.state.roof__leaders_rating, 'roof__leaders_rating')) {
      return false
    }


    else if (!validateFields(this.state.roof_skylight, 'roof_skylight field')) {
      return false
    } else if (!validateFields(this.state.roof_skylight_rating, 'roof_skylight_rating')) {
      return false
    }

    else if (!validateFields(this.state.roof_parapet, 'roof_parapet field')) {
      return false
    } else if (!validateFields(this.state.roof_parapet_rating, 'roof_parapet_rating')) {
      return false
    }


    else if (!validateFields(this.state.fire_escapes_ladders, 'fire_escapes_ladders field')) {
      return false
    } else if (!validateFields(this.state.fire_escapes_ladders_rating, 'fire_escapes_ladders_rating')) {
      return false
    }


    else if (!validateFields(this.state.fire_escapes_basket, 'fire_escapes_basket field')) {
      return false
    } else if (!validateFields(this.state.fire_escapes_basket_rating, 'fire_escapes_basket_rating')) {
      return false
    }

    else if (!validateFields(this.state.fire_escapes_railling, 'fire_escapes_railling field')) {
      return false
    } else if (!validateFields(this.state.fire_escapes_railling_rating, 'fire_escapes_railling_rating')) {
      return false
    }


    else if (!validateFields(this.state.fire_escapes_ladder_shoes, 'fire_escapes_ladder_shoes field')) {
      return false
    } else if (!validateFields(this.state.fire_escapes_ladder_shoes_rating, 'fire_escapes_ladder_shoes_rating')) {
      return false
    }

    else if (!validateFields(this.state.lobbies_floor, 'lobbies_floor field')) {
      return false
    } else if (!validateFields(this.state.lobbies_floor_rating, 'lobbies_floor_rating')) {
      return false
    }


    else if (!validateFields(this.state.lobbies_walls, 'lobbies_walls field')) {
      return false
    } else if (!validateFields(this.state.lobbies_walls_rating, 'lobbies_walls_rating')) {
      return false
    }

    else if (!validateFields(this.state.lobbies_lightning_emergency, 'lobbies_lightning_emergency field')) {
      return false
    } else if (!validateFields(this.state.lobbies_lightning_emergency_rating, 'lobbies_lightning_emergency_rating')) {
      return false
    }


    else if (!validateFields(this.state.lobbies_windows_sills, 'lobbies_windows_sills field')) {
      return false
    }
    else if (!validateFields(this.state.lobbies_windows_sills_rating, 'lobbies_windows_sills_rating')) {
      return false
    }


    else if (!validateFields(this.state.basement_doors, 'basement_doors field')) {
      return false
    } else if (!validateFields(this.state.basement_doors_rating, 'basement_doors_rating')) {
      return false
    }


    else if (!validateFields(this.state.basement_stairs, 'basement_stairs field')) {
      return false
    } else if (!validateFields(this.state.basement_stairs_rating, 'basement_stairs')) {
      return false
    }


    else if (!validateFields(this.state.basement_floor, 'basement_floor field')) {
      return false
    } else if (!validateFields(this.state.basement_floor_rating, 'basement_floor_rating')) {
      return false
    }


    else if (!validateFields(this.state.basement_walls, 'basement_walls field')) {
      return false
    } else if (!validateFields(this.state.basement_walls_rating, 'basement_walls_rating')) {
      return false
    }


    else if (!validateFields(this.state.basement_lightning, 'basement_lightning field')) {
      return false
    } else if (!validateFields(this.state.basement_lightning_rating, 'basement_lightning_rating')) {
      return false
    }

    else if (!validateFields(this.state.basement_windows, 'basement_windows field')) {
      return false
    } else if (!validateFields(this.state.basement_windows_rating, 'basement_windows_rating')) {
      return false
    }


    else if (!validateFields(this.state.basement_garbage_room, 'basement_garbage_room field')) {
      return false
    } else if (!validateFields(this.state.basement_garbage_room_rating, 'basement_garbage_room_rating')) {
      return false
    }

    else if (!validateFields(this.state.laundry_room_equipment, 'laundry_room_equipment field')) {
      return false
    } else if (!validateFields(this.state.laundry_room_equipment_rating, 'laundry_room_equipment_rating')) {
      return false
    }


    else if (!validateFields(this.state.laundry_room_floor, 'laundry_room_floor field')) {
      return false
    } else if (!validateFields(this.state.laundry_room_floor_rating, 'laundry_room_floor_rating')) {
      return false
    }


    else if (!validateFields(this.state.laundry_room_walls, 'laundry_room_walls field')) {
      return false
    } else if (!validateFields(this.state.laundry_room_walls_rating, 'laundry_room_walls_rating')) {
      return false
    }


    else if (!validateFields(this.state.laundry_room_lightning, 'laundry_room_lightning field')) {
      return false
    } else if (!validateFields(this.state.laundry_room_lightning_rating, 'laundry_room_lightning_rating')) {
      return false
    }


    else if (!validateFields(this.state.boiler_fuel_supply, 'boiler_fuel_supply field')) {
      return false
    } else if (!validateFields(this.state.boiler_fuel_supply_rating, 'boiler_fuel_supply_rating')) {
      return false
    }


    else if (!validateFields(this.state.boiler_gauge_color_coding_markings, 'boiler_gauge_color_coding_markings field')) {
      return false
    } else if (!validateFields(this.state.boiler_gauge_color_coding_markings_rating, 'boiler_gauge_color_coding_markings_rating')) {
      return false
    }


    else if (!validateFields(this.state.boiler_nurners, 'boiler_nurners field')) {
      return false
    } else if (!validateFields(this.state.boiler_nurners_rating, 'boiler_nurners_rating')) {
      return false
    }

    else if (!validateFields(this.state.boiler_coils, 'boiler_coils field')) {
      return false
    } else if (!validateFields(this.state.boiler_coils_rating, 'boiler_nurners_rating')) {
      return false
    }


    else if (!validateFields(this.state.boiler_oil_tank, 'boiler_oil_tank field')) {
      return false
    } else if (!validateFields(this.state.boiler_oil_tank, 'boiler_oil_tank_rating')) {
      return false
    }


    else if (!validateFields(this.state.boiler_plumbing, 'boiler_plumbing field')) {
      return false

    } else if (!validateFields(this.state.boiler_plumbing, 'boiler_plumbing_rating')) {
      return false
    }


    else if (!validateFields(this.state.boiler_leaks, 'boiler_leaks field')) {
      return false
    } else if (!validateFields(this.state.boiler_leaks_rating, 'boiler_leaks_rating')) {
      return false
    }


    else if (!validateFields(this.state.boiler_overheads_returns, 'boiler_overheads_returns field')) {
      return false
    } else if (!validateFields(this.state.boiler_overheads_returns_rating, 'boiler_overheads_returns_rating')) {
      return false
    }


    else if (!validateFields(this.state.meters_lightning, 'meters_lightning field')) {
      return false
    } else if (!validateFields(this.state.meters_lightning_rating, 'meters_lightning_rating')) {
      return false
    }


    else if (!validateFields(this.state.meters_accessibility, 'meters_accessibility field')) {
      return false
    } else if (!validateFields(this.state.meters_accessibility_rating, 'meters_accessibility_rating')) {
      return false
    }


    else if (!validateFields(this.state.meters_up_keeping, 'meters_up_keeping field')) {
      return false
    } else if (!validateFields(this.state.meters_up_keeping_rating, 'meters_up_keeping_rating')) {
      return false
    }


    else if (!validateFields(this.state.elevator_lightning, 'elevator_lightning field')) {
      return false
    } else if (!validateFields(this.state.elevator_lightning_rating, 'elevator_lightning_rating')) {
      return false
    }


    else if (!validateFields(this.state.elevator_signals, 'elevator_signals field')) {
      return false
    } else if (!validateFields(this.state.elevator_signals_rating, 'elevator_signals_rating')) {
      return false
    }
    else if (!validateFields(this.state.elevator_doors, 'elevator_doors field')) {
      return false
    } else if (!validateFields(this.state.elevator_doors_rating, 'elevator_doors_rating')) {
      return false
    }

   else if (!validateFields(this.state.elevator_cab_floor, 'elevator_cab_floor field')) {
      return false
    } else if (!validateFields(this.state.elevator_cab_floor_rating, 'elevator_cab_floor_rating')) {
      return false
    }
   else if (!validateFields(this.state.elevator_cab_walls, 'elevator_cab_walls field')) {
      return false
    } else if (!validateFields(this.state.elevator_cab_walls_rating, 'elevator_cab_walls_rating')) {
      return false
    }

    else if (!validateFields(this.state.elevator_cab_ceilling, 'elevator_cab_ceilling field')) {
      return false
    } else if (!validateFields(this.state.elevator_cab_ceilling_rating, 'elevator_cab_ceilling_rating')) {
      return false
    }

    else if (!validateFields(this.state.elevator_floor_nmber_on_doors, 'elevator_floor_nmber_on_doors field')) {
      return false
    } else if (!validateFields(this.state.elevator_floor_nmber_on_doors_rating, 'elevator_floor_nmber_on_doors_rating')) {
      return false
    }


    else if (!validateFields(this.state.fire_equipment_sprinklers, 'fire_equipment_sprinklers field')) {
      return false
    } else if (!validateFields(this.state.fire_equipment_sprinklers_rating, 'fire_equipment_sprinklers_rating')) {
      return false
    }


    else if (!validateFields(this.state.fire_equipment_extinguishers, 'fire_equipment_extinguishers field')) {
      return false
    } else if (!validateFields(this.state.fire_equipment_extinguishers_rating, 'fire_equipment_extinguishers_ratings')) {
      return false
    }


    else if (!validateFields(this.state.fire_equipment_alarm_system, 'fire_equipment_alarm_system field')) {
      return false
    } else if (!validateFields(this.state.fire_equipment_alarm_system_rating, 'fire_equipment_alarm_system_rating')) {
      return false
    }


    else if (!validateFields(this.state.vacant_units_apt1, 'vacant_units_apt1 field')) {
      return false
    } else if (!validateFields(this.state.vacant_units_apt1_rating, 'vacant_units_apt1_rating')) {
      return false
    }


    else if (!validateFields(this.state.vacant_units_apt2, 'vacant_units_apt2 field')) {
      return false
    } else if (!validateFields(this.state.vacant_units_apt2_rating, 'vacant_units_apt2_rating')) {
      return false
    }

    else if (!validateFields(this.state.inventory, 'inventory field')) {
      return false
    } else if (!validateFields(this.state.inventory_rating, 'inventory_rating')) {
      return false
    }
    else if (!validateFields(this.state.problem, 'problem field')) {
      return false
    } 
    else if (!validateFields(this.state.notes, 'notes field')) {
      return false
    }

    else {
      console.log('Validation Complete')
      this.createPDF();
      return true
    }
  }


  test=()=> {
    var that = this;
    async function externalStoragePermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'External Storage Write Permission',
          message:'App needs access to Storage data.',
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        that.testPDF();
      } else {
        alert('WRITE_EXTERNAL_STORAGE permission denied');
      }
    } catch (err) {
      Alert.alert('Write permission err', err);
      console.warn(err);
    }
   }

    if (Platform.OS === 'android') {
      externalStoragePermission();
    } else {
      this.testPDF();
    }
  }

 
  async testPDF() {

    console.log('yess----------')
    let r = Math.random().toString(36).substring(7);
    let name = 'building_' + r.toString()
    let options = {
        html: '<table><tr><th>Firstname</th><th>Lastname</th></tr></table>',
        fileName: name,
        directory: 'Documents',
    };
    let file = await RNHTMLtoPDF.convert(options)
    Platform.OS == 'android' ? this.saveData(file.filePath)
    : this.saveDataIOS(file.filePath)
    //alert(file.filePath);
}


 MyText=(textLable)=>{
return(
<View>
<Text style={styles.MyText}>
    {textLable}
  </Text>
</View>
)
}
MyInputText=(OnTextChange,textValue)=>{
  return(
  
    <TextInput
      style={styles.MyInputText} 
      ref={input => { this.textInput = input }}
      value={textValue}
     // onEndEditing={() => this.endEditing()}
      maxLength={40}
       keyboardType={Platform.OS === 'ios' ? 'ascii-capable' : 'visible-password'}
      onChangeText={OnTextChange}
     
      />
  )
}
MyDropDown=(selectedValue,onValueChange)=>{
  return( 
  <View style={styles.pickerstyle}>
    <Picker
      style={{width: widthhh*70/100}}
      iosHeader="Ratings"
      placeholder="Select your rating"
      placeholderStyle={{ marginRight:0 }}
      mode="dropdown"
      iosIcon={<FontAwesome name="caret-down" style={{ paddingLeft: hp('12%') }} />}
      selectedValue={selectedValue}
      itemStyle={{ paddingLeft: 10, paddingRight: hp('0%')}}
      onValueChange={onValueChange}
    >
      {
        this.state.items.map(items => {
           return (<Picker.Item style={{flex:1}} label={items.label} value={items.value} key={items.label} />)
        })
      }
    </Picker>
  </View>
  )
}

handleScroll(e){
 // console.log('----------')
  // Keyboard.dismiss()
}

render() {
  const config = {
    velocityThreshold: 0.3,
    directionalOffsetThreshold: 80
  };
    return (
      <View style={styles.container}>
        <ImageBackground style={styles.imagebackgroundstyle} source={background}>
          <TouchableWithoutFeedback onPressOut={()=> Keyboard.dismiss()}
          onPressIn={()=> Keyboard.dismiss()}
            onPress={()=> Keyboard.dismiss()}>
       
            <KeyboardAwareScrollView 
            innerRef={ref => {
              this.scroll = ref;
            }}
           
            enableOnAndroid={true}
            
            contentContainerStyle={{flexGrow:1}}> 
         


       
              <Image source={logo} style={styles.logostyle} />
              <Text style={{ alignSelf: 'center', fontSize: hp('3%'), fontFamily: 'Sina-Bold' }}>Building Inspection Form</Text>

              {this.MyText("Building")}
           
              {this.MyInputText((text) => this.setState({ building: text }),this.state.building)}
              {this.MyText("Week of (yyyy-mm-dd)")}
              {this.MyInputText((text) => this.setState({ week_of: text }),this.state.week_of)}
              {this.MyText("Manager")}
              {this.MyInputText((text) => this.setState({ manager: text }),this.state.manager)}
              {this.MyText("Supt")}
              {this.MyInputText((text) => this.setState({ supt: text }),this.state.supt)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.supt_rating,(value) => this.setState({ supt_rating: value }))}
            
              <Text style={{ alignSelf: 'center', fontSize: hp('2.5%'), fontWeight: 'bold', marginTop: hp('3%') }}>AREAS</Text>
              <Text style={{ alignSelf: 'center', fontSize: hp('2.2%'), fontWeight: 'bold', marginTop: hp('1%') }}>Exterior Conditions</Text>

              {this.MyText("Sidewalks Steps")}
              {this.MyInputText((text) => this.setState({ exterior_cond_sidewalk_steps: text }),this.state.exterior_cond_sidewalk_steps)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.exterior_cond_sidewalk_steps_rating,(value) => this.setState({ exterior_cond_sidewalk_steps_rating: value }))}
              {this.MyText("Cans / Cover")}
              {this.MyInputText((text) => this.setState({ exterior_cond_cans_cover: text }),this.state.exterior_cond_cans_cover)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.exterior_cond_cans_cover_rating,(value) => this.setState({ exterior_cond_cans_cover_rating: value }))}
              {this.MyText("Door(s)")}
              {this.MyInputText((text) => this.setState({ exterior_cond_doors: text }),this.state.exterior_cond_doors)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.exterior_cond_doors_rating,(value) => this.setState({ exterior_cond_doors_rating: value }))}
              {this.MyText("Courtyard (Paved Area)")}
              {this.MyInputText((text) => this.setState({ exterior_cond_courtyard: text }),this.state.exterior_cond_courtyard)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.exterior_cond_courtyard_rating,(value) => this.setState({ exterior_cond_courtyard_rating: value }))}
              {this.MyText("Yard Drain(s)")}
              {this.MyInputText((text) => this.setState({ exterior_cond_yard_drain: text }),this.state.exterior_cond_yard_drain)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.exterior_cond_yard_drain_rating,(value) => this.setState({ exterior_cond_yard_drain_rating: value }))}
              {this.MyText("Fountain Walls")}
              {this.MyInputText((text) => this.setState({ exterior_cond_fountain_walls: text }),this.state.exterior_cond_fountain_walls)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.exterior_cond_fountain_walls_rating,(value) => this.setState({ exterior_cond_fountain_walls_rating: value }))}
              {this.MyText("Light(s) at Entry")}
              {this.MyInputText((text) => this.setState({ exterior_cond_lights_at_entry: text }),this.state.exterior_cond_lights_at_entry)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.exterior_cond_lights_at_entry_rating,(value) => this.setState({ exterior_cond_lights_at_entry_rating: value }))}
              {this.MyText("Light(s) at Yard")}
              {this.MyInputText((text) => this.setState({ exterior_cond_lights_at_yard: text }),this.state.exterior_cond_lights_at_yard)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.exterior_cond_lights_at_yard_rating,(value) => this.setState({ exterior_cond_lights_at_yard_rating: value }))}
              <Text style={styles.labelstyle}>Entrance</Text>
              {this.MyText("Entrance Door")}
              {this.MyInputText((text) => this.setState({ entrance_entrance_door: text }),this.state
              .entrance_entrance_door)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.entrance_entrance_door_rating,(value) => this.setState({ entrance_entrance_door_rating: value }))}
              {this.MyText("Sidelight(s)")}
              {this.MyInputText((text) => this.setState({ entrance_sidelight: text }),this.state.entrance_sidelight)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.entrance_sidelight_rating,(value) => this.setState({ entrance_sidelight_rating: value }))}
              {this.MyText("Intercom")}
              {this.MyInputText((text) => this.setState({ entrance_intercom: text }),this.state.entrance_intercom)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.entrance_intercom_rating,(value) => this.setState({ entrance_intercom_rating: value }))}
              {this.MyText("Mailboxes")}
              {this.MyInputText((text) => this.setState({ entrance_mailboxes: text }),this.state.entrance_mailboxes)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.entrance_mailboxes_rating,(value) => this.setState({ entrance_mailboxes_rating: value }))}
              {this.MyText("Floor")}
              {this.MyInputText((text) => this.setState({ entrance_floor: text }),this.state.entrance_floor)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.entrance_floor_rating,(value) => this.setState({ entrance_floor_rating: value }))}
              {this.MyText("Walls")}
              {this.MyInputText((text) => this.setState({ entrance_walls: text }),this.state.entrance_walls)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.entrance_walls_rating,(value) => this.setState({ entrance_walls_rating: value }))}
              {this.MyText("Lightning")}
              {this.MyInputText((text) => this.setState({ entrance_lightning: text }),this.state.entrance_lightning)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.entrance_lightning_rating,(value) => this.setState({ entrance_lightning_rating: value }))}
              <Text style={styles.labelstyle}>Stairwells</Text>
            
              {this.MyText("Steps / Treads")}
              {this.MyInputText((text) => this.setState({ stairwells_steps_treads: text }),this.state.stairwells_steps_treads)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.stairwells_steps_treads_rating,(value) => this.setState({ stairwells_steps_treads_rating: value }))}
              {this.MyText("Walls")}
              {this.MyInputText((text) => this.setState({ stairwells_walls: text }),this.state.stairwells_walls)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.stairwells_walls_rating,(value) => this.setState({ stairwells_walls_rating: value }))}
              {this.MyText("Lightning")}
              {this.MyInputText((text) => this.setState({ stairwells_lightning: text }),this.state.stairwells_lightning)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.stairwells_lightning_rating,(value) => this.setState({ stairwells_lightning_rating: value }))}
              {this.MyText("Emergency Lightning")}
              {this.MyInputText((text) => this.setState({ stairwells_emergency_lightning: text }),this.state.stairwells_emergency_lightning)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.stairwells_emergency_lightning_rating,(value) => this.setState({ stairwells_emergency_lightning_rating: value }))}
              {this.MyText("Door(s)")}
              {this.MyInputText((text) => this.setState({ stairwells_doors: text }),this.state.stairwells_doors)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.stairwells_doors_rating,(value) => this.setState({ stairwells_doors_rating: value }))}
              {this.MyText("Window(s)")}
              {this.MyInputText((text) => this.setState({ stairwells_windows: text }),this.state.stairwells_windows)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.stairwells_windows_rating,(value) => this.setState({ stairwells_windows_rating: value }))}
              {this.MyText("Signs")}
              {this.MyInputText((text) => this.setState({ stairwells_signs: text }),this.state.stairwells_signs)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.stairwells_signs_rating,(value) => this.setState({ stairwells_signs_rating: value }))}
               <Text style={styles.labelstyle}>Emergency Exit</Text>
               {this.MyText("Lightning")}
              {this.MyInputText((text) => this.setState({ emergency_exit_lightning: text }),this.state.emergency_exit_lightning)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.emergency_exit_lightning_rating,(value) => this.setState({ emergency_exit_lightning_rating: value }))}
              {this.MyText("Floor")}
              {this.MyInputText((text) => this.setState({ emergency_exit_floor: text }),this.state.emergency_exit_floor)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.emergency_exit_floor_rating,(value) => this.setState({ emergency_exit_floor_rating: value }))}
              {this.MyText("Door(s)")}
              {this.MyInputText((text) => this.setState({ emergency_exit_doors: text }),this.state.emergency_exit_doors)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.emergency_exit_doors_rating,(value) => this.setState({ emergency_exit_doors_rating: value }))}
              {this.MyText("Signs")}
              {this.MyInputText((text) => this.setState({ emergency_exit_signs: text }),this.state.emergency_exit_signs)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.emergency_exit_signs_rating,(value) => this.setState({ emergency_exit_signs_rating: value }))}
              <Text style={styles.labelstyle}>Roof</Text>
              {this.MyText("Bulkhead")}
              {this.MyInputText((text) => this.setState({ roof_bulkhead: text }),this.state.roof_bulkhead)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.roof_bulkhead_rating,(value) => this.setState({ roof_bulkhead_rating: value }))}
              {this.MyText("Roof Doors")}
              {this.MyInputText((text) => this.setState({ roof_roof_doors: text }),this.state.roof_roof_doors)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.roof_roof_doors_rating,(value) => this.setState({ roof_roof_doors_rating: value }))}
              {this.MyText("Roof Surface")}
              {this.MyInputText((text) => this.setState({ roof_roof_surface: text }),this.state.roof_roof_surface)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.roof_roof_surface_rating,(value) => this.setState({ roof_roof_surface_rating: value }))}
              {this.MyText("Gutter")}
              {this.MyInputText((text) => this.setState({ roof_gutter : text }),this.state.roof_gutter)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.roof_gutter_rating,(value) => this.setState({ roof_gutter_rating: value }))}
              {this.MyText("Leader(s)")}
              {this.MyInputText((text) => this.setState({ roof__leaders : text }),this.state.roof__leaders)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.roof__leaders_rating,(value) => this.setState({ roof__leaders_rating: value }))}
              {this.MyText("Skylight")}
              {this.MyInputText((text) => this.setState({ roof_skylight : text }),this.state.roof_skylight)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.roof_skylight_rating,(value) => this.setState({ roof_skylight_rating: value }))}
              {this.MyText("Parapet(s)")}
              {this.MyInputText((text) => this.setState({ roof_parapet : text }),this.state.roof_parapet)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.roof_parapet_rating,(value) => this.setState({ roof_parapet_rating: value }))}
              <Text style={styles.labelstyle}>Fire Escapes</Text>
              {this.MyText("Ladders")}
              {this.MyInputText((text) => this.setState({ fire_escapes_ladders : text }),this.state.fire_escapes_ladders)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.fire_escapes_ladders_rating,(value) => this.setState({ fire_escapes_ladders_rating: value }))}
              {this.MyText("Basket(s)")}
              {this.MyInputText((text) => this.setState({ fire_escapes_basket : text }),this.state.fire_escapes_basket)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.fire_escapes_basket_rating,(value) => this.setState({ fire_escapes_basket_rating: value }))}
              {this.MyText("Railing(s)")}
              {this.MyInputText((text) => this.setState({ fire_escapes_railling : text }),this.state.fire_escapes_railling)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.fire_escapes_railling_rating,(value) => this.setState({ fire_escapes_railling_rating: value }))}
              {this.MyText("Ladder Shoes(s)")}
              {this.MyInputText((text) => this.setState({ fire_escapes_ladder_shoes : text }),this.state.fire_escapes_ladder_shoes)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.fire_escapes_ladder_shoes_rating,(value) => this.setState({ fire_escapes_ladder_shoes_rating: value }))}
              <Text style={styles.labelstyle}>Lobbies(Floor Wise)</Text>
              {this.MyText("Floor")}
              {this.MyInputText((text) => this.setState({ lobbies_floor : text }),this.state.lobbies_floor)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.lobbies_floor_rating,(value) => this.setState({ lobbies_floor_rating: value }))}
              {this.MyText("Walls")}
              {this.MyInputText((text) => this.setState({ lobbies_walls : text }),this.state.lobbies_walls)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.lobbies_walls_rating,(value) => this.setState({ lobbies_walls_rating: value }))}
              {this.MyText("Lightning / Emergency ")}
              {this.MyInputText((text) => this.setState({ lobbies_lightning_emergency : text }),this.state.lobbies_lightning_emergency)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.lobbies_lightning_emergency_rating,(value) => this.setState({ lobbies_lightning_emergency_rating: value }))}
              {this.MyText("Window(s) / Sills")}
              {this.MyInputText((text) => this.setState({ lobbies_windows_sills : text }),this.state.lobbies_windows_sills)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.lobbies_windows_sills_rating,(value) => this.setState({ lobbies_windows_sills_rating: value }))}
              <Text style={styles.labelstyle}>Basement</Text>
              {this.MyText("Basement Doors")}
              {this.MyInputText((text) => this.setState({ basement_doors : text }),this.state.basement_doors)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.basement_doors_rating,(value) => this.setState({ basement_doors_rating: value }))}
              {this.MyText("Stairs")}
              {this.MyInputText((text) => this.setState({ basement_stairs : text }),this.state.basement_stairs)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.basement_stairs_rating,(value) => this.setState({ basement_stairs_rating: value }))}
              {this.MyText("Floor")}
              {this.MyInputText((text) => this.setState({ basement_floor : text }),this.state.basement_floor)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.basement_floor_rating,(value) => this.setState({ basement_floor_rating: value }))}
              {this.MyText("Walls")}
              {this.MyInputText((text) => this.setState({ basement_walls : text }),this.state.basement_walls)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.basement_walls_rating,(value) => this.setState({ basement_walls_rating: value }))}
              {this.MyText("Lightning")}
              {this.MyInputText((text) => this.setState({ basement_lightning : text }),this.state.basement_lightning)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.basement_lightning_rating,(value) => this.setState({ basement_lightning_rating: value }))}
              {this.MyText("Window(s)")}
              {this.MyInputText((text) => this.setState({ basement_windows : text }),this.state.basement_windows)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.basement_windows_rating,(value) => this.setState({ basement_windows_rating: value }))}
              {this.MyText("Garbage Room")}
              {this.MyInputText((text) => this.setState({ basement_garbage_room : text }),this.state.basement_garbage_room)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.basement_garbage_room_rating,(value) => this.setState({ basement_garbage_room_rating: value }))}
              <Text style={styles.labelstyle}>Laundry Room</Text>
              {this.MyText("Equipment")}
              {this.MyInputText((text) => this.setState({ laundry_room_equipment : text }),this.state.laundry_room_equipment)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.laundry_room_equipment_rating,(value) => this.setState({ laundry_room_equipment_rating: value }))}
              {this.MyText("Floor")}
              {this.MyInputText((text) => this.setState({ laundry_room_floor : text }),this.state.laundry_room_floor)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.laundry_room_floor_rating,(value) => this.setState({ laundry_room_floor_rating: value }))}
              {this.MyText("Walls")}
              {this.MyInputText((text) => this.setState({ laundry_room_walls : text }),this.state.laundry_room_walls)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.laundry_room_walls_rating,(value) => this.setState({ laundry_room_walls_rating: value }))}
              {this.MyText("Lightning")}
              {this.MyInputText((text) => this.setState({ laundry_room_lightning : text }),this.state.laundry_room_lightning)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.laundry_room_lightning_rating,(value) => this.setState({ laundry_room_lightning_rating: value }))}
              <Text style={styles.labelstyle}>Boiler</Text>
              {this.MyText("Fuel Supply")}
              {this.MyInputText((text) => this.setState({ boiler_fuel_supply : text }),this.state.boiler_fuel_supply)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.boiler_fuel_supply_rating,(value) => this.setState({ boiler_fuel_supply_rating: value }))}
              {this.MyText("Gauge(s) Color Coding, Markings")}
              {this.MyInputText((text) => this.setState({ boiler_gauge_color_coding_markings : text }),this.state.boiler_gauge_color_coding_markings)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.boiler_gauge_color_coding_markings_rating,(value) => this.setState({ boiler_gauge_color_coding_markings_rating: value }))}
              {this.MyText("Burners")}
              {this.MyInputText((text) => this.setState({ boiler_nurners : text }),this.state.boiler_nurners)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.boiler_nurners_rating,(value) => this.setState({ boiler_nurners_rating: value }))}
              {this.MyText("Coils")}
              {this.MyInputText((text) => this.setState({ boiler_coils : text }),this.state.boiler_coils)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.boiler_coils_rating,(value) => this.setState({ boiler_coils_rating: value }))}
              {this.MyText("Oil Tank")}
              {this.MyInputText((text) => this.setState({ boiler_oil_tank : text }),this.state.boiler_oil_tank)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.boiler_oil_tank_rating,(value) => this.setState({ boiler_oil_tank_rating: value }))}
              {this.MyText("Plumbing")}
              {this.MyInputText((text) => this.setState({ boiler_plumbing : text }),this.state.boiler_plumbing)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.boiler_plumbing_rating,(value) => this.setState({ boiler_plumbing_rating: value }))}
              {this.MyText("Leaks")}
              {this.MyInputText((text) => this.setState({ boiler_leaks : text }),this.state.boiler_leaks)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.boiler_leaks_rating,(value) => this.setState({ boiler_leaks_rating: value }))}
              {this.MyText("Overheads / Returns")}
              {this.MyInputText((text) => this.setState({ boiler_overheads_returns : text }),this.state.boiler_overheads_returns)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.boiler_overheads_returns_rating,(value) => this.setState({ boiler_overheads_returns_rating: value }))}
              <Text style={styles.labelstyle}>Meter(Gas and Electric)</Text>
              {this.MyText("Lightning")}
              {this.MyInputText((text) => this.setState({ meters_lightning : text }),this.state.meters_lightning)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.meters_lightning_rating,(value) => this.setState({ meters_lightning_rating: value }))}
              {this.MyText("Accessibility")}
              {this.MyInputText((text) => this.setState({ meters_accessibility : text }),this.state.meters_accessibility)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.meters_accessibility_rating,(value) => this.setState({ meters_accessibility_rating: value }))}
              {this.MyText("Meters Up keeping")}
              {this.MyInputText((text) => this.setState({ meters_up_keeping : text }),this.state.meters_up_keeping)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.meters_up_keeping_rating,(value) => this.setState({ meters_up_keeping_rating: value }))}
              <Text style={styles.labelstyle}>Elevator / Elevator Room</Text>
              {this.MyText("Lightning")}
              {this.MyInputText((text) => this.setState({ elevator_lightning : text }),this.state.elevator_lightning)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.elevator_lightning_rating,(value) => this.setState({ elevator_lightning_rating: value }))}
              {this.MyText("Signals")}
              {this.MyInputText((text) => this.setState({ elevator_signals : text }),this.state.elevator_signals)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.elevator_signals_rating,(value) => this.setState({ elevator_signals_rating: value }))}
              {this.MyText("Doors")}
              {this.MyInputText((text) => this.setState({ elevator_doors : text }),this.state.elevator_doors)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.elevator_doors_rating,(value) => this.setState({ elevator_doors_rating: value }))}
              {this.MyText("Cab Floor")}
              {this.MyInputText((text) => this.setState({ elevator_cab_floor : text }),this.state.elevator_cab_floor)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.elevator_cab_floor_rating,(value) => this.setState({ elevator_cab_floor_rating: value }))}
              {this.MyText("Cab Walls")}
              {this.MyInputText((text) => this.setState({ elevator_cab_walls : text }),this.state.elevator_cab_walls)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.elevator_cab_walls_rating,(value) => this.setState({ elevator_cab_walls_rating: value }))}
              {this.MyText("Cab Ceiling")}
              {this.MyInputText((text) => this.setState({ elevator_cab_ceilling : text }),this.state.elevator_cab_ceilling)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.elevator_cab_ceilling_rating,(value) => this.setState({ elevator_cab_ceilling_rating: value }))}
              {this.MyText("Floor Number on Doors")}
              {this.MyInputText((text) => this.setState({ elevator_floor_nmber_on_doors : text }),this.state.elevator_floor_nmber_on_doors)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.elevator_floor_nmber_on_doors_rating,(value) => this.setState({ elevator_floor_nmber_on_doors_rating: value }))}
              <Text style={styles.labelstyle}>Fire Equipment</Text>
              {this.MyText("Sprinklers")}
              {this.MyInputText((text) => this.setState({ fire_equipment_sprinklers : text }),this.state.fire_equipment_sprinklers)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.fire_equipment_sprinklers_rating,(value) => this.setState({ fire_equipment_sprinklers_rating: value }))}
              {this.MyText("Fire Extinguishers")}
              {this.MyInputText((text) => this.setState({ fire_equipment_extinguishers : text }),this.state.fire_equipment_extinguishers)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.fire_equipment_extinguishers_rating,(value) => this.setState({ fire_equipment_extinguishers_rating: value }))}
              {this.MyText("Alarm System")}
              {this.MyInputText((text) => this.setState({ fire_equipment_alarm_system : text }),this.state.fire_equipment_alarm_system)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.fire_equipment_alarm_system_rating,(value) => this.setState({ fire_equipment_alarm_system_rating: value }))}
              <Text style={styles.labelstyle}>Vacant Units</Text>
              {this.MyText("APT #")}
              {this.MyInputText((text) => this.setState({ vacant_units_apt1 : text }),this.state.vacant_units_apt1)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.vacant_units_apt1_rating,(value) => this.setState({ vacant_units_apt1_rating: value }))}
             
              {this.MyText("APT #")}
              {this.MyInputText((text) => this.setState({ vacant_units_apt2 : text }),this.state.vacant_units_apt2)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.vacant_units_apt2_rating,(value) => this.setState({ vacant_units_apt2_rating: value }))}
              {this.MyText("Inventory")}
              {this.MyInputText((text) => this.setState({ inventory : text }),this.state.inventory)}
              {this.MyText("Choose Rating")}
              {this.MyDropDown(this.state.inventory_rating,(value) => this.setState({ inventory_rating: value }))}
             
              {this.MyText("Problem (Specify)")}
              
               <TextInput style={[styles.customtextinputstyle, {
                width: wp('80%'),
                height: hp('3%'),
                borderRadius: 10, paddingLeft: hp('1%'), paddingRight: hp('1%'),
                borderColor: '#000000', borderWidth: 2, height: hp('13%'),textAlignVertical: 'top',
              }]} onChangeText={(text) => this.setState({ problem: text })}
                multiline
                value={this.state.problem}
              />

              {this.MyText("Notes")}
              <TextInput style={[styles.customtextinputstyle, {
                width: wp('80%'),
                height: hp('3%'), borderRadius: 10, paddingLeft: hp('1%'), paddingRight: hp('1%'),
                borderColor: '#000000', borderWidth: 2, height: hp('13%'),
                textAlignVertical: 'top',
               }]} 
                multiline
                value={this.state.notes}
                onChangeText={(text) => this.setState({ notes: text })}
              />
          
              <TouchableOpacity  onPress={() => this.isValidate()}
              style={{ 
                 justifyContent: 'center', 
                 alignSelf: 'center', 
                 borderRadius: 15,
                 marginTop: 25,
                  marginBottom: 80, width:'80%', 
                  height: 50, backgroundColor: '#0059BF' ,elevation:10}}
                >
                  <Text
                   style={{ alignSelf: 'center', fontSize: hp('2%'), color: '#ffffff', fontWeight: 'bold' }}>Save</Text>
              
                </TouchableOpacity>
                <View style={{height:Platform.OS == 'android' ? this.state.bottomHeight : 0}}/>
                
            </KeyboardAwareScrollView>
                </TouchableWithoutFeedback>
        </ImageBackground>
        {/* {
          this.state.loading ?
            <Modal transparent={true}>
             <Image style={{ height: hp('10%'), width: wp('10%'), marginTop: hp('50%'), backgroundColor: "rgba(0, 0, 0, 0)", alignSelf: "center" }} source={require('../assets/Loader.gif')} /></Modal> : null
        } */}

        {this.state.loading &&
          <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.5)', 
            justifyContent: 'center',
            alignItems:'center' ,
            height:heighttt}
          ]}>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
         }
      </View>

    )
  }
}


const styles = StyleSheet.create({
  MyText:{
    marginLeft:hp('5.7%'),
    marginTop:hp('3.8%'),
    fontWeight:'bold',
    fontSize:hp('2%'),
  },
  MyInputText:{ 
    alignSelf: 'center',
    marginTop:15,
    width:widthhh * 0.8,
    height:58,
    borderColor:'#000000',
    borderWidth:hp('0.3%'),
    borderRadius:10,
    backgroundColor:'#ffffff',
    paddingLeft: hp('1%'), paddingRight: hp('1%')
   },
  container: {
    flex: 1,
    width: '100%',
    height:'100%',
  },

  customtextinputstyle: {
    alignSelf: 'center',
    marginTop:15,

  },

  imagebackgroundstyle: { width: wp('100%'), height: hp('100%') },
  logostyle: {
    width: wp('40%'), height: hp('16%'), marginTop: hp('2.1%'), alignSelf: 'center'
  },
  labelstyle: { alignSelf: 'center', fontSize: hp('2.2%'), fontWeight: 'bold', marginTop: hp('4%') },
  pickerstyle: { borderRadius: 10, width: wp('80%'), height: hp('7%'),justifyContent:'center',alignItems:'center',
   marginTop: hp('1%'), alignSelf: 'center', borderColor: '#000000', borderWidth: hp('0.3%'), backgroundColor: '#ffffff'}
});