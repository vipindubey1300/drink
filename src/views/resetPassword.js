import React, {Component, Fragment} from 'react';
import {
  ImageBackground,
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  Image,
  ScrollView,
  ActivityIndicator,
  Platform,
  BackHandler,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {connect} from 'react-redux';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import Toast, {DURATION} from 'react-native-easy-toast';
import CONFIG from '../config/config';
import * as LOGINAPI from '../services/api-services/login-api';
import {updateUserDetails} from '../services/redux/actions/userActions';
import NavigationBar from 'react-native-navbar-color';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class resetPassword extends Component {
  constructor (props) {
    super (props);
    this.state = {
      newPassword: '',
      confirmPassword: '',
      newPassMsg: false,
      newPassErrMsg: STRINGS.resetPassword.newPassMsg,
      confirmPassMsg1: false,
      confirmPassMsg2: false,
      token: '',
      userId: '',
      loader: false,
      count: 0,
    };
  }

  navigateToSignIn = () => {
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.handleBackButton
    );
    this.props.navigation.navigate ('SignIn');
  };

  componentDidMount () {
    BackHandler.addEventListener ('hardwareBackPress', this.handleBackButton);
    NavigationBar.setStatusBarColor ('white', false);
    NavigationBar.setColor ('white');
    const {navigation} = this.props;
    var userId = navigation.getParam ('userId', null);
    this.setState ({newPassMsg: false, confirmPassMsg: false, userId: userId});
  }

  validate = () => {
    let isValidate = true;
    if (this.state.newPassword == '') {
      this.setState ({newPassMsg: true});
      isValidate = false;
    } else {
      if (!CONFIG.PASSWORD_POLICY.test (this.state.newPassword)) {
        this.setState ({
          newPassMsg: true,
          newPassErrMsg: STRINGS.resetPassword.newPassErrMsg,
        });
        isValidate = false;
      }
      if (
        this.state.newPassword.length <= 1 ||
        this.state.newPassword.length > 16
      ) {
        this.setState ({
          newPassMsg: true,
          newPassErrMsg: STRINGS.resetPassword.newPassErrMsg,
        });
        isValidate = false;
      }
    }
    if (this.state.confirmPassword === '') {
      this.setState ({confirmPassMsg1: true, confirmPassMsg2: false});
      isValidate = false;
    } else {
      if (this.state.newPassword == '' && this.state.confirmPassword !== '') {
        this.setState ({confirmPassMsg1: false, confirmPassMsg2: false});
        isValidate = false;
      }
      if (this.state.confirmPassword !== this.state.newPassword) {
        this.setState ({confirmPassMsg2: true, confirmPassMsg1: false});
        isValidate = false;
      }
    }
    if (isValidate) {
      this.changePassword ();
    }
  };

  componentWillUnmount () {
    // Remove the event listener
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.handleBackButton
    );
  }

  handleBackButton = () => {
    let count = this.state.count;
    if (count == 1) {
      BackHandler.exitApp ();
      return true;
    } else {
      this.showToast ('Press back again to leave');
      this.setState ({count: 1});
      return true;
    }
  };

  changePassword = () => {
    this.setState ({loader: true});
    let payload = {
      userid: this.state.userId,
      password: this.state.newPassword,
    };
    LOGINAPI.postSignupData (
      payload,
      CONFIG.BASE_URL + CONFIG.RECOVER_PASSWORD_URL
    )
      .then (
        function (res) {
          console.warn('res: ',res);
          this.setState ({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                this.showToast ('Password reset successfully');
                this.navigateToSignIn ();
                break;
              case CONFIG.statusUnauthorized:
                  this.setState ({
                    newPassMsg: true,
                    newPassErrMsg: 'New password should not be same as old password',
                  });
                break;
              case CONFIG.statusError: 
                this.showToast ('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          this.setState ({loader: false});
          this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  render () {
    return (
      <Fragment>
        <SafeAreaView style={Styles.screen}>
          <StatusBar backgroundColor="white" barStyle="dark-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
          <ScrollView>
            <View>
              <Image
                source={require ('../assets/drink_a_drink_logo.png')}
                style={styles.logoStyle}
              />
            </View>
            <ImageBackground
              source={require ('../assets/Mugs2.png')}
              style={styles.imgBackground}
            >
              <View style={styles.header}>
                <Text style={styles.headerTextStyle}>
                  {STRINGS.resetPassword.resetPassword}
                </Text>
              </View>
              <View style={styles.inputStyles}>
                <View style={styles.alignRowCenter}>
                  <View
                    style={
                      this.state.newPassMsg == true
                        ? styles.inputDirectionStylesWithError
                        : styles.inputDirectionStyles
                    }
                  >
                    <Image
                      source={require ('../assets/pwd.png')}
                      style={styles.iconStyle}
                    />

                    <TextInput
                      style={styles.textInput}
                      onChangeText={newPassword => {
                        this.setState ({
                          newPassword: newPassword,
                          newPassMsg: false,
                        });
                      }}
                      placeholderTextColor="black"
                      value={this.state.newPassword}
                      placeholder={STRINGS.resetPassword.newPassword}
                      secureTextEntry={true}
                    />
                  </View>
                </View>
                {this.state.newPassMsg &&
                  <View style={styles.alignRowCenter}>
                    <View style={styles.errorRowWidth}>
                      <Text style={Styles.error}>
                        {this.state.newPassErrMsg}
                      </Text>
                    </View>
                  </View>}
                <View style={styles.alignRowCenter}>
                  <View
                    style={
                      this.state.confirmPassMsg1 == true ||
                        this.state.confirmPassMsg2
                        ? styles.inputDirectionStylesWithError
                        : styles.inputDirectionStyles
                    }
                  >
                    <Image
                      source={require ('../assets/pwd.png')}
                      style={styles.iconStyle}
                    />

                    <TextInput
                      style={styles.textInput}
                      onChangeText={confirmPassword => {
                        this.setState ({
                          confirmPassword: confirmPassword,
                          confirmPassMsg1: false,
                          confirmPassMsg2: false,
                        });
                      }}
                      placeholderTextColor="black"
                      value={this.state.confirmPassword}
                      placeholder={STRINGS.resetPassword.confirmPassword}
                      secureTextEntry={true}
                    />
                  </View>
                </View>
                {this.state.confirmPassMsg1 &&
                  <View style={styles.alignRowCenter}>
                    <View style={styles.errorRowWidth}>
                      <Text style={Styles.error}>
                        {STRINGS.resetPassword.confirmPassMsg1}
                      </Text>
                    </View>
                  </View>}
                {this.state.confirmPassMsg2 &&
                  <View style={styles.alignRowCenter}>
                    <View style={styles.errorRowWidth}>
                      <Text style={Styles.error}>
                        {STRINGS.resetPassword.confirmPassMsg2}
                      </Text>
                    </View>
                  </View>}
              </View>
              <View style={styles.submitSection}>
                <TouchableOpacity
                  style={styles.submitBtn}
                  onPress={this.validate}
                >
                  <Text style={styles.submitText}>
                    {STRINGS.resetPassword.reset}
                  </Text>
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
        </SafeAreaView>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateUserDetails: profileDetails => {
      dispatch (updateUserDetails (profileDetails));
    },
  };
};
export default connect (mapStateToProps, mapDispatchToProps) (resetPassword);

const styles = EStyleSheet.create ({
  inputDirectionStyles: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderBottomWidth: 1,
    borderBottomColor: '#000',
    height: '45rem',
    marginBottom: '20rem',
    width: '70%',
  },
  inputDirectionStylesWithError: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderBottomWidth: 1,
    borderBottomColor: '#000',
    height: '45rem',
    marginBottom: '10rem',
    width: '70%',
  },
  iconStyle: {
    padding: '10rem',
    margin: '5rem',
    height: '30rem',
    width: '30rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  inputStyles: {
    marginTop: '60rem',
  },
  imgBackground: {
    flex: 1,
    width: null,
    height: null,
  },
  logoStyle: {
    height: '125rem',
    width: '327rem',
    margin: '30rem',
  },
  header: {
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '30rem',
  },
  headerTextStyle: {
    fontSize: fontSizeValue * 23,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  textInput: {
    flex: 1,
    width: '100%',
    color: '#000000',
    paddingLeft: '10rem',
    fontSize: fontSizeValue * 10.7,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  submitSection: {
    height: '46rem',
    marginTop: '40rem',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitBtn: {
    width: '142rem',
    height: '46rem',
    backgroundColor: '#47170d',
    borderRadius: '6rem',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  submitText: {
    fontSize: fontSizeValue * 14,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
  },
  alignRowCenter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  errorRowWidth: {
    width: '70%',
  },
});
