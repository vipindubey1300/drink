import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  FlatList,
  ActivityIndicator,
  Platform,
  Image,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import {connect} from 'react-redux';
import Toast, {DURATION} from 'react-native-easy-toast';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import * as HOMEAPI from '../services/api-services/home-api';
import CONFIG from '../config/config';
import {updateNotificationCount} from '../services/redux/actions/notificationActions';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();
class notifications extends Component {
  constructor (props) {
    super (props);
    this.state = {
      title: STRINGS.notifications.notificationsTitle,
      backButton: true,
      bellIcon: false,
      showEmptyText: false,
      loader: false,
      notificationData: [],
      notifCount: 0,
      token: '',
    };
  }

  showNotification = ({item}) => {
    return (
      <View style={styles.cardMargin}>
        <View
          style={
            item.is_read == 0
              ? styles.cardBackgroundUnread
              : styles.cardBackgroundRead
          }
        >
          <View style={styles.iconColumn}>
            <Image
              source={require ('../assets/redBell.png')}
              style={styles.accIconStyle}
            />
          </View>
          <View style={styles.textColumn}>
            <Text style={styles.notificationText}>
              {item.notification_content}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  renderFooter = () => {
    return <View style={styles.listFooterStyle} />;
  };

  renderHeader = () => {
    return <View style={styles.listHeaderStyle} />;
  };

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  componentDidMount () {
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('#f2f2f2');
    this.setState ({loader: true});
    if (this.state.notificationData.length == 0) {
      this.setState ({showEmptyText: true});
    }
    var profile = this.props.userReducer.profile;
    if (profile) {
      var token = profile.token;
      this.setState ({token: token});
    }
    HOMEAPI.getRequestData (
      token,
      CONFIG.BASE_URL + CONFIG.VIEW_NOTIFICATIONS_URL
    )
      .then (
        function (res) {
          this.setState ({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                let dataArray = res.data;
                this.setState ({
                  notificationData: dataArray,
                  showEmptyText: false,
                });
                break;
              case CONFIG.statusUnauthorized:
                this.showToast (
                  'Your account has been deactivated. Please logout and login with valid user credentials'
                );
                this.props.navigation.navigate ('Home');
                break;
              case CONFIG.statusError:
                this.showToast ('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          this.setState ({loader: false});
          this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  }

  componentWillUnmount () {
    if (!this.state.notificationData.length == 0) {
      this.setState ({loader: true});
      let payload = {
        notifications: this.state.notificationData,
      };
      HOMEAPI.postRequestData (
        payload,
        this.state.token,
        CONFIG.BASE_URL + CONFIG.MARK_READ_NOTIFICATION_URL
      )
        .then (
          function (res) {
            this.setState ({loader: false});
            if (res != undefined) {
              let code = res.code;
              switch (code) {
                case CONFIG.statusOk:
                  let notificationCount = 0;
                  this.props.updateNotificationCount(notificationCount);
                  break;
                case CONFIG.statusUnauthorized:
                  this.showToast (
                    'Your account has been deactivated. Please logout and login with valid user credentials'
                  );
                  this.props.navigation.navigate ('Home');
                  break;
                case CONFIG.statusError:
                  this.showToast ('Something went wrong. Please try again!');
                  break;
              }
            } else {
              this.showToast ('Something went wrong. Please try again!');
            }
          }.bind (this)
        )
        .catch (
          function (err) {
            this.setState ({loader: false});
            this.showToast ('Something went wrong. Please try again!');
          }.bind (this)
        );
    }
  }

  render () {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}
        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}
          />
          <View style={styles.background}>
            {!this.state.showEmptyText &&
              <FlatList
                showsVerticalScrollIndicator={false}
                data={this.state.notificationData}
                renderItem={this.showNotification}
                keyExtractor={item => item.id.toString ()}
                ListFooterComponent={this.renderFooter}
                ListHeaderComponent={this.renderHeader}
              />}
            {this.state.showEmptyText &&
              <View style={styles.emptyBackground}>
                <Text style={styles.emptyCartStyle}>
                  {'No new notifications'}
                </Text>
              </View>}
            <Toast
              position="bottom"
              style={Styles.toastStyle}
              textStyle={{color: 'white'}}
              ref={ref => {
                this.toast = ref;
              }}
            />
          </View>
          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation} />
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {userReducer,notificationReducer} = state;
  return {userReducer,notificationReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateCartCount: cartCount => {
      dispatch (updateCartCount (cartCount));
    },
    updateNotificationCount: notificationCount => {
      dispatch (updateNotificationCount (notificationCount));
    },
  };
};

export default connect (mapStateToProps,mapDispatchToProps) (notifications);

const styles = EStyleSheet.create ({
  background: {
    backgroundColor: '#d2d2d6',
    flex: 1,
  },
  listFooterStyle: {
    marginBottom: '30rem',
  },
  listHeaderStyle: {
    marginTop: '20rem',
  },
  emptyCartStyle: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  emptyBackground: {
    backgroundColor: '#d2d2d6',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardBackgroundRead: {
    flexDirection: 'row',
    backgroundColor: 'white',
    marginLeft: '10rem',
    marginRight: '10rem',
    marginTop: '10rem',
    elevation: 10,
  },
  cardBackgroundUnread: {
    flexDirection: 'row',
    backgroundColor: 'rgba(18,176,7,0.1)',
    marginLeft: '10rem',
    marginRight: '10rem',
    marginTop: '10rem',
    elevation: 1,
  },
  accIconStyle: {
    padding: '10rem',
    margin: '15rem',
    height: '50rem',
    width: '50rem',
    resizeMode: 'contain',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconColumn: {
    flexDirection: 'column',
  },
  textColumn: {
    flexDirection: 'column',
    width: '75%',
    justifyContent: 'center',
  },
  notificationText: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
    paddingTop: '10rem',
    paddingBottom: '10rem',
  },
  cardMargin: {
    marginBottom: '5rem',
  },
});
