import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  Image,
  ScrollView,
  FlatList,
  ActivityIndicator,
  Platform,
  BackHandler,
  Alert,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {StackActions, NavigationActions} from 'react-navigation';
import {SliderBox} from 'react-native-image-slider-box';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import {Divider} from 'react-native-elements';
import {TouchableOpacity} from 'react-native-gesture-handler';
import NavigationBar from 'react-native-navbar-color';
import {SearchBar} from 'react-native-elements';
import Toast, {DURATION} from 'react-native-easy-toast';
import STRINGS from '../../utils/strings';
import {fontSize} from '../../components/global/Fontsize';
import Styles from '../../styles/styles';
import Header from '../../components/shared/header';
import Footer from '../../components/shared/footer';
import ImageContainer from '../../components/shared/imageContainer';
import FilterComponent from '../../components/shared/filter';
import DrinkDisplayRow from '../../components/shared/drinksDisplayRow';
import * as HOMEAPI from '../../services/api-services/home-api';
import CONFIG from '../../config/config';
import {
  productUpdateDetails,
} from '../../services/redux/actions/productActions';
import {updateUserDetails} from '../../services/redux/actions/userActions';
import {
  updateNotificationCount,
} from '../../services/redux/actions/notificationActions';
import {updateCartCount} from '../../services/redux/actions/cartActions';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class home extends Component {
  constructor (props) {
    super (props);
    this.state = {
      title: STRINGS.home.hometitle,
      search: '',
      backButton: false,
      drinkName: 'Grape Juice',
      topDrink: STRINGS.home.topDrinks,
      specialDrink: STRINGS.home.specialDrinks,
      bellIcon: true,
      token: '',
      loader: false,
      glassImages: [],
      products: [],
      categories: [],
      specialDrinks: [],
      topDrinks: [],
      count: 0,
    };
  }

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  // showGlassImages = ({item}) => {
  //   return (
  //     <View style={styles.imgBorder}>
  //       <ImageContainer imageUrl={item.slider_image} />
  //     </View>
  //   );
  // };

  navigateToProductListing = (title, data) => {
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.handleBackButton
    );
    if (data.length == 0) {
      this.props.navigation.navigate ('ProductListing', {
        title: title,
      });
    } else {
      this.props.navigation.navigate ('ProductListing', {
        title: title,
        searchList: data,
      });
    }
  };

  showLogoutAlert = () => {
    Alert.alert (
      '',
      'Your account is deactivated. Please login with valid credentials',
      [{text: 'OK', onPress: () => this.logoutUser ()}],
      {cancelable: false}
    );
  };

  displayResults = () => {
    if (!this.state.search == '') {
      this.setState ({loader: true});
      let params = {
        keyword: this.state.search,
      };
      let elipsis = '...';
      let title = '';
      if (this.state.search.length > 16) {
        let resStr = this.state.search.substr (0, 16);
        let resultString = resStr.concat (elipsis);
        title = resultString;
      } else {
        title = this.state.search;
      }
      HOMEAPI.getSearchData (
        this.state.token,
        CONFIG.BASE_URL + CONFIG.SEARCH_PRODUCT_URL,
        params
      )
        .then (
          function (res) {
            this.setState ({loader: false});
            if (res != undefined) {
              let code = res.code;
              switch (code) {
                case CONFIG.statusOk:
                  this.navigateToProductListing (title, res.data);
                  break;
                case CONFIG.statusUnauthorized:
                  this.showToast (
                    'Your account has been deactivated. Please logout and login with valid user credentials'
                  );
                  this.showLogoutAlert ();
                  break;
                case CONFIG.statusError:
                  this.showToast ('Search box cannot be empty!');
                  break;
              }
            } else {
              this.showToast ('Something went wrong. Please try again!');
            }
          }.bind (this)
        )
        .catch (
          function (err) {
            this.setState ({loader: false});
            this.showToast ('Something went wrong. Please try again!');
          }.bind (this)
        );
    } else {
      this.showToast ('Search box cannot be empty!');
    }
  };

  logoutUser = async () => {
    this.props.updateUserDetails ({});
    await AsyncStorage.setItem ('isProfileLoggedIn', JSON.stringify (false));
    await AsyncStorage.setItem ('isProfileOTPVerified', JSON.stringify (false));
    const resetAction = StackActions.reset ({
      index: 0,
      actions: [NavigationActions.navigate ({routeName: 'SignIn'})],
    });
    this.props.navigation.dispatch (resetAction);
  };

  componentDidMount = () => {
    BackHandler.addEventListener ('hardwareBackPress', this.handleBackButton);
    NavigationBar.setStatusBarColor ('#47170d', false);
    NavigationBar.setColor ('#f2f2f2');
    var profile = this.props.userReducer.profile;
    if (profile) {
      var token = profile.token;
      this.setState ({token: token});
    }
    this.dashboardAPICall (token);
    this.onLoad (token);
  };

  setSearchState = () => {
    this.setState ({search: ''});
  };

  componentWillUnmount () {
    // Remove the event listener
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.handleBackButton
    );
    this.didFocusListener.remove();
  }

  handleBackButton = () => {
    let count = this.state.count;
    if (count == 1) {
      BackHandler.exitApp ();
      return true;
    } else {
      this.showToast ('Press back again to leave');
      this.setState ({count: 1});
      return true;
    }
  };

  onLoad = token => {
    this.didFocusListener = this.props.navigation.addListener ('didFocus', () => {
      BackHandler.addEventListener ('hardwareBackPress', this.handleBackButton);
      this.setState ({count: 0});
      this.setSearchState ();
      // this.dashboardAPICall(token);
      this.getCountCall(token);
    });
  };

  getCountCall = token => {
    // this.setState ({loader: true});
    let notificationCount = 0;
    HOMEAPI.getRequestData (token, CONFIG.BASE_URL + CONFIG.GET_COUNT_URL)
      .then (
        function (res) {
          // this.setState ({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                let cartCount = res.data.cart_count;
                this.props.updateCartCount (cartCount);
                notificationCount = res.data.notification_count;
                this.props.updateNotificationCount (notificationCount);
                break;
              case CONFIG.statusUnauthorized:
                this.showToast (
                  'Your account has been deactivated. Please logout and login with valid user credentials'
                );
                this.showLogoutAlert ();
                break;
              case CONFIG.statusError:
                this.showToast ('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          this.setState ({loader: false});
          this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  dashboardAPICall = token => {
    this.setState ({loader: true});
    var productsList = {};
    let notificationCount = 0;
    HOMEAPI.getRequestData (token, CONFIG.BASE_URL + CONFIG.DASHBOARD_URL)
      .then (
        function (res) {
          this.setState ({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                var glassImages = [];
                res.data.slider.map (item => {
                  glassImages.push (item.slider_image);
                });
                this.setState ({
                  glassImages: glassImages,
                  categories: res.data.categories,
                  products: res.data.products,
                  specialDrinks: res.data.special_drinks,
                  topDrinks: res.data.top_drinks,
                });
                productsList = res.data;
                this.props.productUpdateDetails (productsList);
                // let cartCount = res.data.cart_count;
                // this.props.updateCartCount (cartCount);
                // notificationCount = res.data.notification_count;
                // this.props.updateNotificationCount (notificationCount);
                break;
              case CONFIG.statusUnauthorized:
                this.showToast (
                  'Your account has been deactivated. Please logout and login with valid user credentials'
                );
                this.showLogoutAlert ();
                break;
              case CONFIG.statusError:
                this.showToast ('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          this.setState ({loader: false});
          this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  showCategories = ({item}) => {
    return (
      <FilterComponent
        item={item}
        navigation={this.props.navigation}
        handleBackButton={this.handleBackButton}
      />
    );
  };

  render () {
    return (
      <Fragment>
        {/* <SafeAreaView style={[Styles.screen,{backgroundColor:'#47170d'}]}> */}
         <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            handleBackButton={this.handleBackButton}
            notifCount={this.state.notifCount}
          />
          <View style={styles.searchBar}>
            <View style={styles.inputDirectionStylesUser}>
              <TextInput
                style={styles.textInput}
                onChangeText={search => {
                  this.setState ({search: search});
                }}
                placeholderTextColor={'#47170d'}
                value={this.state.search}
                placeholder={STRINGS.home.search}
                onSubmitEditing={this.displayResults}
              />
              <TouchableOpacity onPress={this.displayResults}>
                <Image
                  source={require ('../../assets/search.png')}
                  style={styles.searchIconStyle}
                />
              </TouchableOpacity>
            </View>
          </View>
          {this.state.loader &&
            <View style={styles.emptyScreen}>
              <Text style={styles.loadingText}>{'Loading...'}</Text>
            </View>}
          {!this.state.loader &&
            <ScrollView
              style={styles.layoutMargins}
              showsVerticalScrollIndicator={false}
            >
              <View style={styles.images}>
                {/* <FlatList
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  data={this.state.glassImages}
                  renderItem={this.showGlassImages}
                  keyExtractor={item => item.id.toString ()}
                /> */}
                <SliderBox
                  images={this.state.glassImages}
                  resizeMode="stretch"
                  circleLoop
                  autoplay
                  useScrollView
                 />
              </View>
              <Divider style={styles.dividerStyleCategories} />
              <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={this.state.categories}
                renderItem={this.showCategories}
                keyExtractor={item => item.id.toString ()}
              />
              <Divider style={styles.dividerStyleCategories} />
              <View style={styles.drinkDisplayView}>
                <DrinkDisplayRow
                  drinkTitle={this.state.topDrink}
                  drinkName={this.state.drinkName}
                  data={this.state.topDrinks}
                  navigation={this.props.navigation}
                  handleBackButton={this.handleBackButton} 
                />
              </View>
              <Divider style={styles.dividerStyle} />
              <View>
                <DrinkDisplayRow
                  drinkTitle={this.state.specialDrink}
                  drinkName={this.state.drinkName}
                  data={this.state.specialDrinks}
                  navigation={this.props.navigation}
                  handleBackButton={this.handleBackButton}
                />
              </View>
              <Divider style={styles.dividerStyle} />
            </ScrollView>}
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
          <View style={Styles.footerOverlay}>
            <Footer
              navigation={this.props.navigation}
              handleBackButton={this.handleBackButton}
            />
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {userReducer, productsReducer, notificationReducer} = state;
  return {userReducer, productsReducer, notificationReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateUserDetails: profileDetails => {
      dispatch (updateUserDetails (profileDetails));
    },
    productUpdateDetails: productsList => {
      dispatch (productUpdateDetails (productsList));
    },
    updateNotificationCount: notificationCount => {
      dispatch (updateNotificationCount (notificationCount));
    },
    updateCartCount: cartCount => {
      dispatch (updateCartCount (cartCount));
    },
  };
};

export default connect (mapStateToProps, mapDispatchToProps) (home);

const styles = EStyleSheet.create ({
  searchBar: {
    flexDirection: 'row',
    backgroundColor: '#47170d',
    height: '70rem',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputDirectionStylesUser: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: '40rem',
    width: '90%',
    borderRadius: '5rem',
    paddingLeft: '10rem',
    paddingRight: '10rem',
  },
  images: {
    height: '150rem',
    marginTop: '10rem',
    marginBottom: '10rem',
    flexDirection: 'row',
  },
  layoutMargins: {
    marginLeft: '5rem',
    marginRight: '5rem',
  },
  emptyScreen: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  loadingText: {
    fontSize: fontSizeValue * 11,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  imgBorder: {
    borderWidth: 2,
    borderColor: '#47170d',
    marginRight: '8rem',
  },
  imageStyle: {
    height: '50%',
    width: '50%',
  },
  dividerStyle: {
    backgroundColor: '#ccc',
    height: 1,
    width: '97%',
    marginLeft: '5rem',
    marginTop: '20rem',
    marginBottom: '15rem',
  },
  drinkDisplayView: {
    marginTop: '15rem',
  },
  searchIconStyle: {
    padding: '10rem',
    margin: '10rem',
    height: '25rem',
    width: '25rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    width: '100%',
    color: '#47170d',
    paddingLeft: '10rem',
    fontSize: fontSizeValue * 11,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  dividerStyleCategories: {
    backgroundColor: '#47170d',
    height: 1,
    width: '97%',
    marginLeft: '5rem',
  },
});
