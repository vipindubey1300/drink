import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  ScrollView,
  FlatList,
  ActivityIndicator,
  Image,
  StyleSheet,
  Platform,
  BackHandler,
} from 'react-native';
import Toast, {DURATION} from 'react-native-easy-toast';
import NavigationBar from 'react-native-navbar-color';
import {connect} from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import CartListCard from '../components/shared/cartCard';
import {Divider} from 'react-native-elements';
import * as HOMEAPI from '../services/api-services/home-api';
import CONFIG from '../config/config';
import {productUpdateDetails} from '../services/redux/actions/productActions';
import {updateUserDetails} from '../services/redux/actions/userActions';
import {updateCartCount} from '../services/redux/actions/cartActions';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();
class cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: STRINGS.cart.cartTitle,
      backButton: true,
      bellIcon: true,
      tax: 0.0,
      subtotal: 0.0,
      other: 0.0,
      cartTotal: 0.0,
      requireCheck: false,
      showDeleteButton: 'true',
      loader: false,
      cartData: [],
      emptyCart: true,
      myLoder:false,
    };
  }

  showToast = (toastMsg) => {
    this.toast.show(toastMsg);
  };

  componentDidMount() {
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('#f2f2f2');
    var profile = this.props.userReducer.profile;
    if (profile) {
      var token = profile.token;
    }
    this.getCartItems(token);
    this.onLoad(token);
  }

  onLoad = (token) => {
    this.didFocusListener = this.props.navigation.addListener('didFocus', () =>
      this.getCartItems(token),
    );
  };

  componentWillUnmount() {
    // Remove the event listener
    this.didFocusListener.remove();
  }

  getCartItems = (token) => {
    this.setState({loader: true});
    HOMEAPI.getRequestData(token, CONFIG.BASE_URL + CONFIG.GET_CART_URL)
      .then(
        function (res) {
          this.setState({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                let cartData = res.data;
                if (res.data.length == 0) {
                  this.setState({emptyCart: true});
                  let cartCount = 0;
                  this.props.updateCartCount(cartCount);
                } else {
                  var cartTotal = res.total;
                  var tax = res.tax;
                  var other = res.other;
                  var subtotal = cartTotal + tax + other;
                  let cartCount = cartData.length;
                  this.props.updateCartCount(cartCount);
                  this.setState({
                    cartData: cartData,
                    subtotal: subtotal,
                    cartTotal: cartTotal,
                    tax: tax,
                    other: other,
                    emptyCart: false,
                  });
                  if (subtotal < 40) {
                    this.setState({
                      requireCheck: true,
                    });
                  } else {
                    this.setState({
                      requireCheck: false,
                    });
                  }
                }
                break;
              case CONFIG.statusUnauthorized:
                this.setState({emptyCart: true});
                this.showToast(
                  'Your account has been deactivated. Please logout and login with valid user credentials',
                );
                this.props.navigation.navigate('Home');
                break;
              case CONFIG.statusError:
                this.setState({emptyCart: true});
                this.showToast('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.setState({emptyCart: true});
            this.showToast('Something went wrong. Please try again!');
          }
        }.bind(this),
      )
      .catch(
        function (err) {
          this.setState({loader: false});
          this.setState({emptyCart: true});
          this.showToast('Something went wrong. Please try again!');
        }.bind(this),
      );
  };

  showProductsList = ({item}) => {
    let elipsis = '...';
    let drinkName = '';
    if (item.product_title.length > 50) {
      var resStr = item.product_title.substr(0, 50);
      var resultString = resStr.concat(elipsis);
      drinkName = resultString;
    } else {
      drinkName = item.product_title;
    }
    return (
      <CartListCard
        item={item}
        imageUrl={item.product_image}
        drinkName={drinkName}
        offer={item.product_discount}
        deliveryTime={item.product_delivery_time}
        quantity={item.quantity}
        productId={item.product_id}
        cart={'cart'}
        lode={this.MyOnlode}
        getCartItem={this.getCartItems}
        showDeleteButton={this.state.showDeleteButton}
        updateData={(val) => this.updateData(val)}
        navigation={this.props.navigation}
      />
    );
  };
MyOnlode=(data)=>{
 this.setState({loader:data})
}
  listFooterComponent = () => {
    return (
      <View>
        <Divider style={styles.dividerStyle} />
        <View style={styles.totalRowSection}>
          <View style={styles.totalRow}>
            <Text style={styles.cartTotal}>{STRINGS.cart.cartTotal}</Text>
            <Text style={styles.cartTotal}>
              {'$' + parseFloat(this.state.cartTotal).toFixed(2)}
            </Text>
          </View>
          <View style={styles.totalRow}>
            <Text style={styles.tax}>{STRINGS.cart.tax}</Text>
            <Text style={styles.tax}>
              {'$' + parseFloat(this.state.tax).toFixed(2)}
            </Text>
          </View>
          <View style={styles.totalRow}>
            <Text style={styles.other}>{STRINGS.cart.other}</Text>
            <Text style={styles.other}>
              {'$' + parseFloat(this.state.other).toFixed(2)}
            </Text>
          </View>
        </View>
        <Divider style={styles.subtotalDividerStyle} />
        <View style={styles.totalRow}>
          <Text style={styles.subtotal}>{STRINGS.cart.subtotal}</Text>
          <Text style={styles.subtotal}>
            {'$' + parseFloat(this.state.subtotal).toFixed(2)}
          </Text>
        </View>
        <TouchableOpacity
          style={[
            styles.checkoutBtnStyle,
            this.state.requireCheck === true
              ? styles.redButton
              : styles.greenButton,
          ]}
          onPress={this.navigateToCheckOut}>
          <Text style={styles.checkout}>{STRINGS.cart.checkout}</Text>
        </TouchableOpacity>



        {this.state.requireCheck === true ? (
          <Text style={styles.requiredText}>{STRINGS.cart.requiredText}</Text>
        ) : null}

<View style={{width:200,height:100}}></View>
      </View>
    );
  };

  navigateToCheckOut = () => {
    if (this.state.requireCheck === false) {
      this.props.navigation.navigate('CheckOutStep1', {
        cartData: this.state.cartData,
        subtotal: this.state.subtotal,
        other: this.state.other,
        cartTotal: this.state.cartTotal,
        tax: this.state.tax,
      });
    }
  };

  render() {
    return (
      <Fragment>


        {/* <SafeAreaView style={Styles.screen}> */}
        <View style={{flex:1}}>

          
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
         
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}></Header>

          {!this.state.emptyCart && (
            <View style={styles.background}>
              <View style={styles.cartListCard}>
              
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={this.state.cartData}
                  renderItem={this.showProductsList}
                  keyExtractor={(item) => item.id.toString()}
                  ListFooterComponent={ this.listFooterComponent}
                />  
               
               
              </View>
             
            </View> 
          )}    
          
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={(ref) => {
              this.toast = ref;
            }}
          />
         
          <View style={{position:'absolute',bottom:0,left:0,right:0}}>
            <Footer navigation={this.props.navigation}></Footer>
            
          </View>

        {this.state.loader &&
          <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.1)', justifyContent: 'center' }
          ]}>
            <ActivityIndicator size="large" color="#47170d" />
          </View>
        }


       {    
             this.state.loader ? 
             <View style={{ 
               backgroundColor: '#d2d2d6',
             justifyContent: 'center',
             alignItems: 'center',}}> 
             </View> 
             : 
            this.state.emptyCart && (
            <View style={styles.emptyBackground}>
              <Image
                source={require('../assets/emptycart.png')}
                style={styles.emptyCartImgStyle}
              />
              <View>
                <Text style={styles.emptyCartStyle}>
                  {'Your cart is empty!'}
                </Text>
              </View>
            </View>
          )
          }

          </View>
         

        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const {userReducer, productsReducer} = state;
  return {userReducer, productsReducer};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUserDetails: (profileDetails) => {
      dispatch(updateUserDetails(profileDetails));
    },
    productUpdateDetails: (products) => {
      dispatch(productUpdateDetails(products));
    },
    updateCartCount: cartCount => {
      dispatch (updateCartCount (cartCount));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(cart);

const styles = EStyleSheet.create({
  background: {
    backgroundColor: '#d2d2d6',
    flex: 1,
  },
  emptyBackground: {
    backgroundColor: '#d2d2d6',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyCartStyle: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  cartListCard: {
    marginTop: '30rem',
  },
  dividerStyle: {
    height: '5rem',
    backgroundColor: 'white',
  },
  totalRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: '20rem',
    marginRight: '20rem',
  },
  totalRowSection: {
    marginTop: '30rem',
    marginBottom: '20rem',
  },
  cartTotal: {
    fontSize: fontSizeValue * 13,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
    letterSpacing: '0.6rem',
  },
  tax: {
    fontSize: fontSizeValue * 13,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
    letterSpacing: '0.6rem',
  },
  other: {
    fontSize: fontSizeValue * 13,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
    letterSpacing: '0.6rem',
  },
  subtotal: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
    letterSpacing: '0.6rem',
  },
  subtotalDividerStyle: {
    height: '1rem',
    backgroundColor: '#47170d',
    width: '95%',
    marginLeft: '8rem',
    marginBottom: '10rem',
  },
  checkoutBtnStyle: {
    width: '290rem',
    height: '40rem',
    borderRadius: '6rem',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: '50rem',
    marginLeft: '45rem',
  },
  redButton: {
    backgroundColor: '#d90d0d',
  },
  greenButton: {
    backgroundColor: '#12b007',
    marginBottom: '60rem',
  },
  checkout: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
    letterSpacing: '0.6rem',
  },
  requiredText: {
    fontSize: fontSizeValue * 13,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#a10606',
    marginTop: '5rem',
    marginBottom: '60rem',
    textAlign: 'center',
  },
  emptyCartImgStyle: {
    height: '200rem',
    width: '200rem',
  },
});
