import React, {Component, Fragment} from 'react';
import {
  View,
  ImageBackground,
  Dimensions,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {connect} from 'react-redux';
import NavigationBar from 'react-native-navbar-color';
import SplashScreen from 'react-native-splash-screen';
import EStyleSheet from 'react-native-extended-stylesheet';
import AsyncStorage from '@react-native-community/async-storage';
import Styles from '../../styles/styles';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});

class splashScreen extends Component {
  constructor() {
    super();
    this.state = {
      isVerified: '',
    };
  }

  getAsyncData = async () => {
    var profile = this.props.userReducer.profile;
    if (profile) {
      var isLoggedIn = profile.isLoggedIn;
    }
    var isVerified = await AsyncStorage.getItem('isVerified');
    let isOTPVerified = await AsyncStorage.getItem('isOTPVerified');
    let isProfileOTPVerified = await AsyncStorage.getItem(
      'isProfileOTPVerified',
    );
    let isProfileLoggedIn = await AsyncStorage.getItem('isProfileLoggedIn');
    if (isVerified == null || isVerified === JSON.stringify(false)) {
      setTimeout(() => {
        this.props.navigation.navigate('AgeVerification');
      }, 100);
    } else if (
      isVerified === JSON.stringify(true) &&
      isOTPVerified === JSON.stringify(false) &&
      isLoggedIn === undefined
    ) {
      setTimeout(() => {
        this.props.navigation.navigate('SignUp');
      }, 100);
    } else if (
      isVerified === JSON.stringify(true) &&
      isOTPVerified === JSON.stringify(true) &&
      isProfileOTPVerified === JSON.stringify(false) &&
      isProfileLoggedIn == JSON.stringify(false) &&
      isLoggedIn == undefined
    ) {
      setTimeout(() => {
        this.props.navigation.navigate('SignIn');
      }, 100);
    } else if (
      (isVerified === JSON.stringify(true) &&
        isOTPVerified === JSON.stringify(true) &&
        isLoggedIn === true) ||
      isProfileOTPVerified === JSON.stringify(true) ||
      isProfileLoggedIn == JSON.stringify(true)
    ) {
      setTimeout(() => {
        this.props.navigation.navigate('Home');
      }, 100);
    } else {
      setTimeout(() => {
        this.props.navigation.navigate('SignIn');
      }, 100);
    }
  };

  componentDidMount = () => {
    NavigationBar.setStatusBarColor('#0f1109',false)
    SplashScreen.hide();
    this.getAsyncData();
  };

  render() {
    return (
     <Fragment>
        <SafeAreaView style={Styles.splashScreen}>
          <StatusBar backgroundColor="#0f1109" barStyle="light-content" />
          <ImageBackground
            source={require('../../assets/launch_screen.jpg')}
            style={styles.imgBackground}
          />
        </SafeAreaView>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUserDetails: (profileDetails) => {
      dispatch(updateUserDetails(profileDetails));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(splashScreen);

const styles = EStyleSheet.create({
  imgBackground: {
    flex: 1,
    width: null,
    height: null,
  },
});
