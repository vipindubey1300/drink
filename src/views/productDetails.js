import React, {Component, Fragment} from 'react';
import {
  ImageBackground,
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  ScrollView,
  Image,
  ActivityIndicator,
  Platform,
  BackHandler,
} from 'react-native';
import Toast, {DURATION} from 'react-native-easy-toast';
import NavigationBar from 'react-native-navbar-color';
import {connect} from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import * as HOMEAPI from '../services/api-services/home-api';
import CONFIG from '../config/config';
import {productUpdateDetails} from '../services/redux/actions/productActions';
import {updateUserDetails} from '../services/redux/actions/userActions';
import {updateCartCount} from '../services/redux/actions/cartActions';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();
class productDetails extends Component {
  constructor (props) {
    super (props);
    this.state = {
      title: '',
      backButton: true,
      bellIcon: true,
      price: 0.0,
      quantity: 0,
      item: {},
      token: '',
      originalPrice: 0.0,
      productId: 0,
      previousQuantity: 0,
    };
  }

  componentDidMount () {
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('#f2f2f2');
    var profile = this.props.userReducer.profile;
    if (profile) {
      let token = profile.token;
      this.setState ({token: token});
    }
    var cartCount = this.props.cartReducer.cartCount;
    const {navigation} = this.props;
    var quantity = navigation.getParam ('quantity', null);
    var title = navigation.getParam ('title', null);
    var item = navigation.getParam ('item', null);
    var productId = navigation.getParam ('productId', null);
    let floatPrice = parseFloat (item.discounted_price);
    let price = quantity * floatPrice;
    let originalPrice = 0
    if(item.discounted_price == 0){
      originalPrice = item.product_price
    } else {
      originalPrice = item.discounted_price;
    }
    this.setState ({
      productId: productId,
      quantity: quantity,
      previousQuantity: quantity,
      item: item,
      price: price,
      originalPrice: originalPrice,
      cartCount: cartCount,
    });
    let elipsis = '...';
    if (title.length > 16) {
      let resStr = title.substr (0, 16);
      let resultString = resStr.concat (elipsis);
      this.setState ({title: resultString});
    } else {
      this.setState ({title: title});
    }
  }

  addItem = () => {
    let quantity = this.state.quantity + 1;
    let previousQuantity = this.state.quantity;
    this.setState ({
      quantity: quantity,
      previousQuantity: previousQuantity,
      price: quantity * this.state.originalPrice,
    });
  };

  removeItem = () => {
    let quantity = this.state.quantity - 1;
    let previousQuantity = this.state.quantity;
    if (this.state.quantity != 0) {
      this.setState ({
        quantity: quantity,
        previousQuantity: previousQuantity,
        price: quantity * this.state.originalPrice,
      });
    }
  };

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  addToCart = () => {
    if (this.state.quantity == 0) {
      this.showToast ('Please select quantity');
    } else {
      this.setState ({loader: true});
      let payload = {
        product_id: this.state.productId,
        quantity: this.state.quantity,
      };
      HOMEAPI.postRequestData (
        payload,
        this.state.token,
        CONFIG.BASE_URL + CONFIG.ADD_CART_ITEM_URL
      )
        .then (
          function (res) {
            this.setState ({loader: false});
            if (res != undefined) {
              let code = res.code;
              switch (code) {
                case CONFIG.statusOk:
                  let cartCount = this.state.cartCount + 1;
                  this.props.updateCartCount (cartCount);
                  this.showToast ('Item added to cart successfully');
                  break;
                case CONFIG.statusAlreadyAdded:
                  this.showToast ('Item already exist in the cart');
                  this.updateProductQuantity (this.state.quantity);
                  break;
                case CONFIG.statusUnauthorized:
                  this.showToast (
                    'Your account has been deactivated. Please logout and login with valid user credentials'
                  );
                  this.props.navigation.navigate ('Home');
                  break;
                case CONFIG.statusError:
                  this.showToast ('Something went wrong. Please try again!');
                  break;
              }
            } else {
              this.showToast ('Something went wrong. Please try again!');
            }
          }.bind (this)
        )
        .catch (
          function (err) {
            this.setState ({loader: false});
            this.showToast ('Something went wrong. Please try again!');
          }.bind (this)
        );
    }
  };

  updateProductQuantity = quantity => {
    this.setState ({loader: true});
    let payload = {
      product_id: this.state.productId,
      quantity: quantity,
    };
    HOMEAPI.postRequestData (
      payload,
      this.state.token,
      CONFIG.BASE_URL + CONFIG.UPDATE_PRODUCT_QUANTITY_URL
    )
      .then (
        function (res) {
          this.setState ({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                this.showToast ('Item quantity updated successfully');
                break;
              case CONFIG.statusUnauthorized:
                this.showToast (
                  'Your account has been deactivated. Please logout and login with valid user credentials'
                );
                this.setState ({quantity: this.state.previousQuantity});
                this.props.navigation.navigate ('Home');
                break;
              case CONFIG.statusError:
                this.showToast ('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          this.setState ({loader: false});
          this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  render () {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.listingScreen}> */}
        <View style={[Styles.listingScreen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
         <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}></Header>

          <ScrollView>
            <View style={styles.backgroundLayout}>
              <View style={styles.imageView}>
                <Image
                  source={{uri: this.state.item.product_image}}
                  style={styles.imageStyle}
                />
              </View>
              <View style={styles.detailsView}>
                <Text style={styles.title}>
                  {this.state.item.product_title}
                  <Text style={styles.offerTextGreen}>
                    {' (' + this.state.item.product_discount + ')'}
                  </Text>
                </Text>
                <Text style={styles.details}>
                  {this.state.item.product_description}
                </Text>
              </View>
              <View style={styles.addRemoveStyle}>
                <TouchableOpacity onPress={this.removeItem}>
                  <Image
                    source={require ('../assets/remove.png')}
                    style={styles.iconStyle}
                  />
                </TouchableOpacity>


                {Platform.OS=='ios' ? 
               <View style={{position:'absolute',top:2}}>
                <Text style={{color:'#47170d',fontSize:23,top:-3,position:'absolute',left:25}}>|</Text>
                <Text style={[styles.quantityText,{position:'absolute',left:32,top:5}]}>{this.state.quantity}</Text>
                <Text style={{color:'#47170d',fontSize:23,top:-3,position:'absolute',left:65}}>|</Text>
               </View>
               : <Text style={styles.quantityText}>{this.state.quantity}</Text>
                }
                <TouchableOpacity onPress={this.addItem}>
                 <Image
                    source={require ('../assets/add.png')}
                    style={styles.iconStyle}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.parentPriceView}>
                <View style={styles.priceView}>
                  <Text style={styles.price}>
                    {'$' + parseFloat (this.state.price).toFixed (2)}
                  </Text>
                </View>
              </View>
              <TouchableOpacity
                style={styles.addCartBtnStyle}
                onPress={this.addToCart}
              >
                <Text style={styles.addToCart}>
                  {STRINGS.productDetails.addToCart}
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation} />
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {userReducer, productsReducer, cartReducer} = state;
  return {userReducer, productsReducer, cartReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateUserDetails: profileDetails => {
      dispatch (updateUserDetails (profileDetails));
    },
    productUpdateDetails: products => {
      dispatch (productUpdateDetails (products));
    },
    updateCartCount: cartCount => {
      dispatch (updateCartCount (cartCount));
    },
  };
};

export default connect (mapStateToProps, mapDispatchToProps) (productDetails);

const styles = EStyleSheet.create ({
  backgroundLayout: {
    marginLeft: '20rem',
    marginRight: '20rem',
  },
  addCartBtnStyle: {
    width: '290rem',
    height: '40rem',
    borderRadius: '6rem',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginLeft: '25rem',
    marginBottom: '40rem',
    backgroundColor: '#47170d',
  },
  addToCart: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
    letterSpacing: '0.6rem',
  },
  price: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
    letterSpacing: '0.6rem',
  },
  imageView: {
    height: '200rem',
    width: '340rem',
    marginTop: '25rem',
    marginBottom: '20rem',
  },
  imageStyle: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  parentPriceView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '30rem',
    marginBottom: '30rem',
  },
  priceView: {
    width: '120rem',
    height: '45rem',
    backgroundColor: 'white',
    borderRadius: '8rem',
    borderColor: '#47170d',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  addRemoveStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#efefef',
    width: '100rem',
    height: '30rem',
  },
  quantityText: {
    fontSize: fontSizeValue * 12,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Bold' : 'Calisto MT',
    fontWeight:'bold',
    color: 'black',
    paddingLeft: '12rem',
    paddingRight: '12rem',
    borderLeftWidth: 1,
    borderLeftColor: '#47170d',
    borderRightWidth: 1,
    borderRightColor: '#47170d',
   
  }, 
  iconStyle: {
    padding: '8rem',
    margin: '5rem', 
    height: '8rem',
    width: '8rem',
    resizeMode: 'contain',
    alignItems: 'center',
  },
  title: {
    fontSize: fontSizeValue * 20,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  details: {
    fontSize: fontSizeValue * 12,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
    textAlign: 'justify',
    marginTop: '10rem',
    marginBottom: '20rem',
  },
  offerTextGreen: {
    fontSize: fontSizeValue * 12,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#12b007',
  },
});
