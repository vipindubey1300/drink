import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import {connect} from 'react-redux';
import Toast, {DURATION} from 'react-native-easy-toast';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import {Divider} from 'react-native-elements';
import * as HOMEAPI from '../services/api-services/home-api';
import CONFIG from '../config/config';
import {updateCartCount} from '../services/redux/actions/cartActions';
// import RemotePushController  from '../services/RemotePushController';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class checkOutStep2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: STRINGS.checkout.checkout,
      backButton: true,
      bellIcon: true,
      codSelect: false,
      cardSwipeSelect: false,
      ecocashSelect: false,
      address: '',
      subtotal: 0.0,
      cartData: [],
      selectedPayOption: '',
      other: 0.0,
      tax: 0.0,
      cartTotal: '0.0',
      token: '',
      loader: false,
      transaction_id: 0,
      pollurl: '',
    };
    const intervalId = 0;
  }

  navigateToNotifications = () => {
    this.props.navigation.navigate('Notifications');
  };

  selectCOD = () => {
    this.setState({
      codSelect: !this.state.codSelect,
      cardSwipeSelect: false,
      ecocashSelect: false,
      selectedPayOption: 'Cash on Delivery',
    });
  };

  selectCardSwipe = () => {
    this.setState({
      cardSwipeSelect: !this.state.cardSwipeSelect,
      ecocashSelect: false,
      codSelect: false,
      selectedPayOption: 'Card Swipe',
    });
  };

  selectEcocash = () => {
    this.setState({
      ecocashSelect: !this.state.ecocashSelect,
      cardSwipeSelect: false,
      codSelect: false,
      selectedPayOption: 'Ecocash/Zipit',
    });
  };

  showToast = (toastMsg) => {
    this.toast.show(toastMsg);
  };

  componentDidMount() {
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('#f2f2f2');
    var profile = this.props.userReducer.profile;
    if (profile) {
      var token = profile.token;
      this.setState({token: token});
    }
    const {navigation} = this.props;
    var cartData = navigation.getParam('cartData', null);
    var subtotal = navigation.getParam('subtotal', null);
    var address = navigation.getParam('address', null);
    var cartTotal = navigation.getParam('cartTotal', null);
    var tax = navigation.getParam('tax', null);
    var other = navigation.getParam('other', null);
    this.setState({
      cartData: cartData,
      subtotal: subtotal,
      address: address,
      other: other,
      tax: tax,
      cartTotal: cartTotal,
    });
  }

  componentWillUnmount() {
    clearInterval(this._interval);
  }

  placeOrder = (pollurl, transaction_id) => {
    if (!this.state.selectedPayOption == '') {
      this.setState({loader: true});
      var payload = {};
      var ecocash = 0;
      if (this.state.selectedPayOption == 'Ecocash/Zipit') {
        ecocash = 1;
      } else {
        ecocash = 0;
      }
      payload = {
        products: this.state.cartData,
        address: this.state.address,
        subtotal: this.state.subtotal,
        selectedPayOption: this.state.selectedPayOption,
        pollurl: pollurl,
        transaction_id: transaction_id,
        ecocash: ecocash,
      };
      HOMEAPI.postRequestData(
        payload,
        this.state.token,
        CONFIG.BASE_URL + CONFIG.ORDER_URL,
      )
        .then(
          function (res) {
            console.warn('res: ',res);
            this.setState({loader: false});
            if (res != undefined) {
              let code = res.code;
              switch (code) {
                case CONFIG.statusOk:
                  this.showToast(res.message);
                  let cartCount = 0;
                  this.props.updateCartCount(cartCount);
                  setTimeout(() => {
                    this.props.navigation.navigate('Home');
                  }, 1000);
                  break;
                case CONFIG.statusUnauthorized:
                  this.showToast(
                    'Your account has been deactivated. Please logout and login with valid user credentials',
                  );
                  this.props.navigation.navigate('Home');
                  break;
                  case CONFIG.statusAlreadyAdded:
                  this.showToast(
                    'Payment Request failed.Retry to place an order !',
                  );
                  setTimeout(() => {
                    this.props.navigation.navigate('Home');
                  }, 1500);
                  break;
                case CONFIG.statusError:
                  this.showToast('Something went wrong. Please try again!');
                  break;
              }
            } else {
              this.showToast('Something went wrong. Please try again!');
            }
          }.bind(this),
        )
        .catch(
          function (err) {
            this.setState({loader: false});
            this.showToast('Something went wrong. Please try again!');
          }.bind(this),
        );
    } else {
      this.showToast('Please select pay option');
    }
  };

  paymentInfoApi = () => {
    if (!this.state.selectedPayOption == '') {
      this.setState({loader: true});
      let payload = {
        products: this.state.cartData,
        subtotal: this.state.subtotal,
        selectedPayOption: this.state.selectedPayOption,
      };
      HOMEAPI.postRequestData(
        payload,
        this.state.token,
        CONFIG.BASE_URL + CONFIG.PAYMENT_URL,
      )
        .then(
          function (res) {
            if (res != undefined) {
              let code = res.code;
              switch (code) {
                case CONFIG.statusOk:
                  this.setState({
                    pollurl: res.pollurl,
                    transaction_id: res.transaction_id,
                  });
                  this.showToast(res.message);
                  this._interval = setInterval(() => {
                    this.placeOrder(res.pollurl, res.transaction_id)
                  }, 20000);
                  break;
                case CONFIG.statusAlreadyAdded:
                  this.setState({loader: false});
                  this.showToast(
                    'Payment Request failed.Retry to place an order !',
                  );
                  break;
                case CONFIG.statusUnauthorized:
                  this.setState({loader: false});
                  this.showToast(
                    'Your account has been deactivated. Please logout and login with valid user credentials',
                  );
                  this.props.navigation.navigate('Home');
                  break;
                case CONFIG.statusError:
                  this.setState({loader: false});
                  this.showToast('Something went wrong. Please try again!');
                  break;
              }
            } else {
              this.showToast('Something went wrong. Please try again!');
            }
          }.bind(this),
        )
        .catch(
          function (err) {
            this.setState({loader: false});
            this.showToast('Something went wrong. Please try again!');
          }.bind(this),
        );
    } else {
      this.showToast('Please select pay option');
    }
  };

  render() {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}
        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />

          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader && (
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>
          )}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}></Header>
          <ScrollView style={styles.background}>
            <View style={styles.step1TextStyle}>
              <Text style={styles.step2Address}>
                {STRINGS.checkout.step2 + ' :- ' + STRINGS.checkout.pay}
              </Text>
            </View>
            <View style={styles.step2Style}>
              <View style={styles.circle}></View>
              <Divider style={styles.step2} />
              <View style={styles.circle}></View>
            </View>

            <View style={styles.addAddressRow}>
              <Image
                source={require('../assets/pay.png')}
                style={styles.payIcon}></Image>
            </View>
            <View style={styles.payOptionBtnContainer}>
              <TouchableOpacity
                style={
                  this.state.codSelect == true
                    ? styles.payOptionBtnWidBorder
                    : styles.payOptionBtn
                }
                onPress={this.selectCOD}>
                <Text style={styles.payOptionBtnType}>
                  {STRINGS.checkout.cashOnDelivery}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={
                  this.state.cardSwipeSelect == true
                    ? styles.payOptionBtnWidBorder
                    : styles.payOptionBtn
                }
                onPress={this.selectCardSwipe}>
                <Text style={styles.payOptionBtnType}>
                  {STRINGS.checkout.cardSwipe}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={
                  this.state.ecocashSelect == true
                    ? styles.payOptionBtnWidBorder
                    : styles.payOptionBtn
                }
                onPress={this.selectEcocash}>
                <Text style={styles.payOptionBtnType}>
                  {STRINGS.checkout.ecocash}
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={styles.submitBtnStyle}
              onPress={
                this.state.selectedPayOption == 'Ecocash/Zipit'
                  ? this.paymentInfoApi
                  : this.placeOrder
              }>
              <Text style={styles.submit}>{STRINGS.checkout.submit}</Text>
            </TouchableOpacity>
            {/* <RemotePushController/> */}
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={(ref) => {
              this.toast = ref;
            }}
          />
          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation}></Footer>
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCartCount: cartCount => {
      dispatch (updateCartCount (cartCount));
    },
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(checkOutStep2);

const styles = EStyleSheet.create({
  background: {
    backgroundColor: '#d2d2d6',
  },
  circle: {
    width: 12,
    height: 12,
    borderRadius: 24 / 2,
    backgroundColor: 'green',
    borderColor: 'black',
    borderWidth: 0.5,
  },
  step2: {
    backgroundColor: 'green',
    width: '93%',
    height: '4rem',
  },
  step2Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '15rem',
    marginLeft: '30rem',
    marginRight: '30rem',
  },
  step1TextStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '30rem',
    marginLeft: '30rem',
  },
  step2Address: {
    fontSize: fontSizeValue * 18,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Bold' : 'Calisto MT',
    fontWeight:'bold',
    color: '#47170d',
  },
  submitBtnStyle: {
    backgroundColor: '#47170d',
    width: '290rem',
    height: '40rem',
    borderRadius: '6rem',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: '50rem',
    marginLeft: '45rem',
  },
  submit: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
    letterSpacing: '0.6rem',
  },
  addAddressRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '20rem',
  },
  payIcon: {
    width: '120rem',
    height: '120rem',
  },
  payOptionBtn: {
    backgroundColor: 'white',
    borderColor: '#ccc',
    borderWidth: 1,
    height: '45rem',
    width: '300rem',
    alignItems: 'center',
    justifyContent: 'center',
  },
  payOptionBtnWidBorder: {
    backgroundColor: 'white',
    borderColor: '#47170d',
    borderWidth: 2,
    height: '45rem',
    width: '300rem',
    alignItems: 'center',
    justifyContent: 'center',
  },
  payOptionBtnType: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  payOptionBtnContainer: {
    marginTop: '20rem',
    marginBottom: '10rem',
    alignItems: 'center',
    flexDirection: 'column',
    height: '180rem',
    justifyContent: 'space-between',
  },
});
