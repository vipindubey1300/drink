import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  ScrollView,
  ActivityIndicator,
  Platform,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import AsyncStorage from '@react-native-community/async-storage';
import {StackActions, NavigationActions} from 'react-navigation';
import {connect} from 'react-redux';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Toast, {DURATION} from 'react-native-easy-toast';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import * as HOMEAPI from '../services/api-services/home-api';
import {updateUserDetails} from '../services/redux/actions/userActions';
import CONFIG from '../config/config';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class changePassword extends Component {
  constructor (props) {
    super (props);
    this.state = {
      title: STRINGS.account.changePassword,
      backButton: true,
      bellIcon: false,
      password: '',
      newPassword: '',
      confirmPassword: '',
      token: '',
      passMsg: false,
      newPassMsg: false,
      confirmPassMsg: false,
      newPasswordMsg: STRINGS.account.newPasswordMsg,
      confirmPasswordMsg: STRINGS.account.newPasswordMsg,
      passwordMsg: STRINGS.account.newPasswordMsg,
      emailId: '',
      phnNumber: '',
      name: '',
      avator: '',
    };
  }

  componentDidMount () {
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('#f2f2f2');
    var profile = this.props.userReducer.profile;
    if (profile) {
      var token = profile.token;
      this.setState ({
        token: token,
        name: profile.name,
        emailId: profile.email,
        phnNumber: profile.mobile_number,
      });
    }
    this.setState ({passMsg: false, newPassMsg: false, confirmPassMsg: false});
  }

  validate = () => {
    let isValidate = true;
    if (
      this.state.password.length < 8 ||
      this.state.password.length >= 16 ||
      !CONFIG.PASSWORD_POLICY.test (this.state.password)
    ) {
      this.setState ({
        passMsg: true,
        passwordMsg: STRINGS.signUp.passMsg,
      });
      isValidate = false;
    }
    if (
      this.state.newPassword.length < 8 ||
      this.state.newPassword.length >= 16 ||
      !CONFIG.PASSWORD_POLICY.test (this.state.newPassword)
    ) {
      this.setState ({
        newPassMsg: true,
        newPasswordMsg: STRINGS.signUp.passMsg,
      });
      isValidate = false;
    }
    if (
      this.state.confirmPassword == '' ||
      this.state.confirmPassword !== this.state.newPassword
    ) {
      if (this.state.confirmPassword == '') {
        let msg = 'Field is required';
        this.setState ({confirmPasswordMsg: msg, confirmPassMsg: true});
      } else {
        let msg = 'Password enetered do not match';
        this.setState ({confirmPasswordMsg: msg, confirmPassMsg: true});
      }
      isValidate = false;
    }
    if (this.state.password === this.state.newPassword) {
      if (this.state.newPassword == '' || this.state.password == '') {
        this.setState ({
          newPassMsg: true,
          newPasswordMsg: 'Field is required',
          passMsg: true,
          passwordMsg: 'Field is required',
        });
      } else {
        this.setState ({
          newPassMsg: true,
          newPasswordMsg: 'New password cannot be same as old password',
        });
      }
      isValidate = false;
    }
    if (isValidate) {
      this.changePassword ();
    }
  };

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  logoutUser = async () => {
    this.props.updateUserDetails ({});
    await AsyncStorage.setItem ('isProfileLoggedIn', JSON.stringify (false));
    await AsyncStorage.setItem ('isProfileOTPVerified', JSON.stringify (false));
    const resetAction = StackActions.reset ({
      index: 0,
      actions: [NavigationActions.navigate ({routeName: 'SignIn'})],
    });
    this.props.navigation.dispatch (resetAction);
  };

  changePassword = () => {
    this.setState ({loader: true});
    let password = this.state.password.trim ();
    let new_password = this.state.newPassword.trim ();
    let payload = {
      password: password,
      new_password: new_password,
    };
    let profile = {};
    HOMEAPI.postRequestData (
      payload,
      this.state.token,
      CONFIG.BASE_URL + CONFIG.CHANGE_PASSWORD_URL
    )
      .then (
        function (res) {
          this.setState ({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                this.showToast ('Password changed successfully');
                // profile.email = this.state.emailId;
                // profile.name = this.state.name;
                // profile.mobile_number = this.state.phnNumber;
                // profile.token = this.state.token;
                // profile.avator = this.state.avator;
                // profile.password = this.state.newPassword;
                // profile.isLoggedIn = true;
                // this.props.updateUserDetails(profile);
                // this.props.navigation.navigate('Account');
                this.logoutUser ();
                break;
              case CONFIG.statusUnauthorized:
                this.setState ({passMsg: true});
                this.setState ({passwordMsg: 'Old password does not match'});
                break;
              case CONFIG.statusError:
                let errors = res.errors;
                errors.map (error => {
                  if (error == 'The new password field is required') {
                    this.setState ({newPasswordMsg: error, newPassMsg: true});
                  }
                  if (error == 'The password field is required') {
                    this.setState ({passwordMsg: error, passMsg: true});
                  }
                  if (error == 'The password must be at least 6 characters.') {
                    this.setState ({
                      passwordMsg: STRINGS.signUp.passMsg,
                      passMsg: true,
                    });
                  }
                });
                break;
            }
          } else {
            this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          this.setState ({loader: false});
          this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  render () {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}
        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}
          />
          <ScrollView style={styles.background}>
            <View style={styles.formBackground}>
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={password => {
                    this.setState ({password: password, passMsg: false});
                  }}
                  placeholderTextColor="black"
                  value={this.state.password}
                  placeholder={STRINGS.account.password}
                />
              </View>
              {this.state.passMsg &&
                <Text style={Styles.error}>{this.state.passwordMsg}</Text>}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={newPassword => {
                    this.setState ({
                      newPassword: newPassword,
                      newPassMsg: false,
                    });
                  }}
                  placeholderTextColor="black"
                  value={this.state.newPassword}
                  placeholder={STRINGS.account.newPassword}
                />
              </View>
              {this.state.newPassMsg &&
                <Text style={Styles.error}>{this.state.newPasswordMsg}</Text>}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={confirmPassword => {
                    this.setState ({
                      confirmPassword: confirmPassword,
                      confirmPassMsg: false,
                    });
                  }}
                  placeholderTextColor="black"
                  value={this.state.confirmPassword}
                  placeholder={STRINGS.account.confirmPassword}
                />
              </View>
              {this.state.confirmPassMsg &&
                <Text style={Styles.error}>
                  {this.state.confirmPasswordMsg}
                </Text>}
              <TouchableOpacity
                style={styles.submitBtnStyle}
                onPress={this.validate}
              >
                <Text style={styles.submit}>{STRINGS.account.save}</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation} />
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateUserDetails: profileDetails => {
      dispatch (updateUserDetails (profileDetails));
    },
  };
};
export default connect (mapStateToProps, mapDispatchToProps) (changePassword);

const styles = EStyleSheet.create ({
  background: {
    backgroundColor: '#d2d2d6',
  },
  submitBtnStyle: {
    backgroundColor: '#47170d',
    width: '290rem',
    height: '40rem',
    borderRadius: '6rem',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: '50rem',
    marginLeft: '25rem',
    marginBottom: '40rem',
  },
  submit: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
    letterSpacing: '0.6rem',
  },
  formBackground: {
    backgroundColor: 'white',
    marginTop: '30rem',
    paddingTop: '30rem',
    marginLeft: '10rem',
    marginRight: '10rem',
    paddingRight: '10rem',
    paddingLeft: '10rem',
  },
  textInput: {
    flex: 1,
    width: '100%',
    color: '#000000',
    paddingLeft: '10rem',
    fontSize: fontSizeValue * 12,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  inputBorderStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 2,
    borderBottomColor: '#ccc',
    height: '45rem',
    marginBottom: '10rem',
  },
});
