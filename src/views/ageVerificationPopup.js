import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Modal,
  View,
  Text,
  BackHandler,
  Alert,
  ImageBackground,
  Platform,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import Styles from '../styles/styles';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import RNExitApp from 'react-native-exit-app';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class productListing extends Component {
  constructor (props) {
    super (props);
    this.state = {
      selectedAgeIndex: 0,
      selectedValue: 0,
      date: '',
      visible: true,
    };
  }

  onSelect (index) {
    if (index == 1) {
      this.setState ({
        selectedAgeIndex: 1,
        selectedValue: 1,
      });
      this.alertFunction ();
    } else {
      this.setState ({
        selectedAgeIndex: 0,
        selectedValue: 0,
      });
    }
    this.setState ({
      text: `Selected index: ${index}`,
    });
  }

  onOkBtn = () => {
    
    Platform.OS == 'android' ? BackHandler.exitApp () : RNExitApp.exitApp()
  };

  onCancelButton = () => {
    this.setState ({
      selectedAgeIndex: 0,
      selectedValue: 0,
    });
  };

  alertFunction = () => {
    Alert.alert (
      'Age Verification',
      "Sorry! You can't proceed further as legal age to use the app is 18",
      [
        {
          text: 'Cancel',
          onPress: this.onCancelButton,
        },
        {text: 'OK', onPress: this.onOkBtn},
      ],
      {cancelable: false}
    );
  };

  onValidate = date => {
    const format = 'DD/MMMM/YYYY'; // Your date format
    const resultFormat = 'years'; // Result format (years, months, days)
    const age = moment ().diff (moment (date, format), resultFormat, true);
    if (age >= 18) {
      this.setState ({visible: false});
      this.props.navigation.navigate ('SignUp');
    } else {
      this.alertFunction ();
    }
  };

  componentDidMoun(){
    NavigationBar.setStatusBarColor('rgba(75,75,75,0.7)',false)
    NavigationBar.setColor('rgba(75,75,75,0.7)');
  }

  render () {
    return (
      <Fragment>
        <SafeAreaView style={[Styles.screen]}>
          <StatusBar
            backgroundColor="rgba(75,75,75,0.7)"
            barStyle="light-content"
          />
          <ImageBackground
            source={require ('../assets/signUp.jpg')}
            style={styles.imgBackground}
          >
            <Modal
              closeOnClick={true}
              transparent={true}
              visible={this.state.visible}
            >
              <View style={styles.modal}>
                <View style={[styles.modalCustomized]}>
                  <View>
                    <Text style={styles.confirmText}>
                      {STRINGS.signUp.confirmText}
                    </Text>
                  </View>
                  <View>
                    <RadioGroup
                      style={styles.selectAgeOption}
                      size={30}
                      thickness={2}
                      color={'#bc8804'}
                      activeColor={'#bc8804'}
                      selectedIndex={this.state.selectedAgeIndex}
                      onSelect={index => this.onSelect (index)}
                    >
                      <RadioButton
                        style={styles.radioButtonYes}
                        value={STRINGS.signUp.yes}
                        color={'#47170d'}
                        size={8}
                      >
                        <Text
                          style={[
                            styles.ageOptionText,
                            this.state.selectedValue === 0
                              ? styles.selectedButton
                              : styles.notSelectedButton,
                          ]}
                        >
                          {STRINGS.signUp.yes}
                        </Text>
                      </RadioButton>

                      <RadioButton
                        style={styles.radioButtonNo}
                        value={STRINGS.signUp.no}
                        color={'#47170d'}
                        size={8}
                      >
                        <Text
                          style={[
                            styles.ageOptionText,
                            this.state.selectedValue === 1
                              ? styles.selectedButton
                              : styles.notSelectedButton,
                          ]}
                        >
                          {STRINGS.signUp.no}
                        </Text>
                      </RadioButton>
                    </RadioGroup>
                  </View>
                  <View style={styles.datepicker}>
                    <DatePicker
                      customStyles={{
                        dateInput: styles.dateInput,
                        dateText: styles.dateText,
                        dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          alignContent: 'flex-start',
                        },
                        placeholderText: {
                          fontSize: fontSizeValue * 15,
                          fontFamily: Platform.OS == 'android'
                            ? 'Calisto-Regular'
                            : 'System',
                          color: '#47170d',
                          marginLeft: '5%',
                        },
                      }}
                      readOnly={true}
                      style={[styles.datePickerSelectInput]}
                      date={this.state.date}
                      mode="date"
                      placeholder={'Select date'}
                      maxDate={new Date ()}
                      format="DD / MMM / YYYY"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      iconSource={require ('../assets/calendar.png')}
                      onDateChange={date => {
                        this.setState ({date: date}, () =>
                          this.onValidate (date)
                        );
                      }}
                    />
                  </View>
                </View>
              </View>
            </Modal>
          </ImageBackground>
        </SafeAreaView>
      </Fragment>
    );
  }
}

export default productListing;

const styles = EStyleSheet.create ({
  modal: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    zIndex: 12,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    zIndex: 10,
  },
  modalCustomized: {
    backgroundColor: 'white',
    padding: '24rem',
    height: '230rem',
    borderRadius: '6rem',
    marginRight: '20rem',
    marginLeft: '20rem',
    borderWidth: '2rem',
    borderColor: '#bc8804',
    justifyContent: 'space-between',
  },
  confirmText: {
    fontSize: fontSizeValue * 14,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Bold' : 'Calisto MT',
    fontWeight:'bold',
    color: '#47170d',
    // fontWeight:'bold',
  },
  selectAgeOption: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  radioButtonYes: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  radioButtonNo: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectedButton: {
    fontFamily: Platform.OS == 'android' ? 'Calisto-Bold' : 'Calisto MT',
    fontWeight:'bold',
  },
  notSelectedButton: {
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  ageOptionText: {
    fontSize: fontSizeValue * 16,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
    marginLeft: '10rem',
    justifyContent: 'space-between',
  },
  dateText: {
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    alignSelf: 'flex-start',
    color: '#5a5a5a',
    fontSize: 15 * fontSizeValue,
  },
  dateInput: {
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    borderWidth: 0,
    paddingLeft: '15rem',
    alignItems: 'flex-start',
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    alignSelf: 'flex-start',
    color: '#5a5a5a',
    fontSize: 15 * fontSizeValue,
    letterSpacing: '0.26rem',
  },
  datePickerSelectInput: {
    width: '80%',
    marginLeft: '45rem', 
    
  },
  dateIcon: {
    position: 'absolute',
    right: '225rem',
    marginLeft: 0,
    height: '40rem',
    width: '40rem',
  },
  datepicker: {
    flexDirection: 'row',
  },
  imgBackground: {
    flex: 1,
    width: null,
    height: null,
  },
});
