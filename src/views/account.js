import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  Image,
  ScrollView,
  ActivityIndicator,
  Platform,
  Alert
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import NavigationBar from 'react-native-navbar-color';
import Toast, {DURATION} from 'react-native-easy-toast';
import EStyleSheet from 'react-native-extended-stylesheet';
import {StackActions, NavigationActions} from 'react-navigation';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {updateUserDetails} from '../services/redux/actions/userActions';
import {connect} from 'react-redux';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import * as HOMEAPI from '../services/api-services/home-api';
import CONFIG from '../config/config';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();
class account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: STRINGS.account.accountTitle,
      backButton: true,
      bellIcon: true,
      loader: false,
      avator: require('../assets/account.png'),
      name: '',
      email: '',
      phnNumner: '',
      privacyPolicyContent: '',
      termsandconditionContent: '',
    };
  }

  navigateToChangePwd = () => {
    this.props.navigation.navigate('ChangePassword');
  };

  navigateToAddress = () => {
    this.props.navigation.navigate('AddAddress', {previousRoute: 'Account'});
  };

  navigateToTermsConditions = () => {
    this.props.navigation.navigate('TermsAndConditions', {
      termsandconditionContent: this.state.termsandconditionContent,
    });
  };

  navigateToprivacyPolicy = () => {
    this.props.navigation.navigate('PrivacyPolicy', {
      privacyPolicyContent: this.state.privacyPolicyContent,
    });
  };

  logoutUser = async() => {
    this.props.updateUserDetails({});
    await AsyncStorage.setItem('isProfileLoggedIn', JSON.stringify(false));
    await AsyncStorage.setItem('isProfileOTPVerified', JSON.stringify(false));
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'SignIn'})],
    });
    this.props.navigation.dispatch(resetAction);
  };

  editProfile = () => {
    this.props.navigation.navigate('Profile');
  };

  showToast = (toastMsg) => {
    this.toast.show(toastMsg);
  };

  componentDidMount() {
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('#f2f2f2');
    var profile = this.props.userReducer.profile;
    if (profile) {
      var token = profile.token;
      this.setState({
        name: profile.name,
        email: profile.email,
        phnNumber: profile.mobile_number,
      });
      this.setState({loader: true});
      HOMEAPI.getRequestData(token, CONFIG.BASE_URL + CONFIG.GET_PAGE_URL)
        .then(
          function (res) {
            this.setState({loader: false});
            if (res != undefined) {
              let code = res.code;
              switch (code) {
                case CONFIG.statusOk:
                  let privacyPolicyContent = res.data[0].page_content;
                  let termsandconditionContent = res.data[1].page_content;
                  this.setState({
                    termsandconditionContent: termsandconditionContent,
                    privacyPolicyContent: privacyPolicyContent,
                  });
                  break;
                case CONFIG.statusUnauthorized:
                  this.showToast('Your account has been deactivated. Please logout and login with valid user credentials');
                  break;
                case CONFIG.statusError:
                  this.showToast('Something went wrong. Please try again!');
                  break;
              }
            } else {
              this.showToast('Something went wrong. Please try again!');
            }
          }.bind(this),
        )
        .catch(
          function (err) {
            this.setState({loader: false});
            this.showToast('Something went wrong. Please try again!');
          }.bind(this),
        );
    }
    this.onLoad();
    this.getData();
  }

  getData = () => {
    var profile = this.props.userReducer.profile;
    if (profile) {
      this.setState({
        name: profile.name,
        email: profile.email,
        phnNumber: profile.mobile_number,
      });
    }
  };

  componentWillUnmount() {
    // Remove the event listener
    this.didFocusListener.remove();
  }

  onLoad = () => {
    this.didFocusListener = this.props.navigation.addListener('didFocus', () => this.getData());
  };

  render() {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}
        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader && (
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>
          )}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}></Header>
          <ScrollView style={styles.background}>
            <View style={styles.accInfo}>
              <View style={styles.accIcon}>
                <Image source={this.state.avator} style={styles.accIconStyle} />
              </View>
              <View style={styles.accDetails}>
                <Text style={styles.nameStyle}>{this.state.name}</Text>
                <Text style={styles.detailStyle}>{this.state.email}</Text>
                <Text style={styles.detailStyle}>{this.state.phnNumber}</Text>
              </View>
              <View style={styles.editDetails}>
                <TouchableOpacity onPress={this.editProfile}>
                  <Image
                    source={require('../assets/edit.png')}
                    style={styles.editIconStyle}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.accOptions}
                onPress={this.navigateToChangePwd}>
                <View>
                  <Image
                    source={require('../assets/pwd.png')}
                    style={styles.iconStyle}
                    tintColor="#47170d"
                  />
                </View>
                <View>
                  <Text style={styles.optionNameStyle}>
                    {STRINGS.account.changePwd}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.accOptions}
                onPress={this.navigateToAddress}>
                <View>
                  <Image
                    source={require('../assets/address.png')}
                    style={styles.iconStyle}
                  />
                </View>
                <View>
                  <Text style={styles.optionNameStyle}>
                    {STRINGS.account.address}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.accOptions}
                onPress={this.navigateToprivacyPolicy}>
                <View>
                  <Image
                    source={require('../assets/policy.png')}
                    style={styles.iconStyle}
                  />
                </View>
                <View>
                  <Text style={styles.optionNameStyle}>
                    {STRINGS.account.privacyPolicy}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.accOptions}
                onPress={this.navigateToTermsConditions}>
                <View>
                  <Image
                    source={require('../assets/termsandcondition.png')}
                    style={styles.iconStyle}
                  />
                </View>
                <View>
                  <Text style={styles.optionNameStyle}>
                    {STRINGS.account.termsAndConditions}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.accOptions}
                onPress={() => {
                  Alert.alert(
                    '',
                    'Are you sure you want to log out from the application ?',
                    [
                      {text: 'Cancel', style: 'destructive'},
                      {text: 'Yes', onPress: () => this.logoutUser()}
                    ],
                    { cancelable: false }
                  );
                }}>
                <View>
                  <Image
                    source={require('../assets/logout.png')}
                    style={styles.iconStyle}
                  />
                </View>
                <View>
                  <Text style={styles.optionNameStyle}>
                    {STRINGS.account.logout}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <Toast
              position="bottom"
              style={Styles.toastStyle}
              textStyle={{color: 'white'}}
              ref={(ref) => {
                this.toast = ref;
              }}
            />
          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation}></Footer>
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUserDetails: (profileDetails) => {
      dispatch(updateUserDetails(profileDetails));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(account);

const styles = EStyleSheet.create({
  background: {
    backgroundColor: '#d2d2d6',
  },
  accInfo: {
    flexDirection: 'row',
    width: '95%',
    borderColor: 'black',
    borderWidth: 0.5,
    marginTop: '30rem',
    marginLeft: '10rem',
    elevation: 1,
    paddingTop: '10rem',
    paddingBottom: '10rem',
    // height: '120rem',
    backgroundColor: 'white',
  },
  accIcon: {
    flexDirection: 'column',
    justifyContent: 'center',
    width: '30%',
  },
  accDetails: {
    flexDirection: 'column',
    justifyContent: 'center',
    width: '55%',
  },
  editDetails: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    width: '15%',
  },
  editIconStyle: {
    padding: '10rem',
    margin: '15rem',
    height: '30rem',
    width: '30rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  accIconStyle: {
    marginLeft: '15rem',
    height: '80rem',
    width: '80rem',
    alignItems: 'center',
  },
  nameStyle: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
  },
  optionNameStyle: {
    fontSize: fontSizeValue * 13,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
    marginLeft: '20rem',
  },
  detailStyle: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
    marginTop: '5rem',
  },
  accOptions: {
    flexDirection: 'row',
    borderColor: 'black',
    borderWidth: 0.5,
    elevation: 1,
    height: '60rem',
    backgroundColor: 'white',
    marginTop: '10rem',
    marginLeft: '15rem',
    marginRight: '15rem',
    alignItems: 'center',
  },
  iconStyle: {
    padding: '10rem',
    margin: '15rem',
    height: '35rem',
    width: '35rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
});
