import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  Image,
  ScrollView,
  ActivityIndicator,
  Platform,
  Alert,
  PermissionsAndroid,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import {Divider} from 'react-native-elements';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import {connect} from 'react-redux';
import Toast, {DURATION} from 'react-native-easy-toast';
import {updateUserDetails} from '../services/redux/actions/userActions';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import * as HOMEAPI from '../services/api-services/home-api';
import CONFIG from '../config/config';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class checkOutStep1 extends Component {
  constructor (props) {
    super (props);
    this.state = {
      title: STRINGS.checkout.checkout,
      backButton: true,
      bellIcon: true,
      selectedAddressIndex: 0,
      selectedValue: '',
      loader: false,
      addressList: [],
      cartData: [],
      subtotal: 0.0,
      other: 0.0,
      tax: 0.0,
      cartTotal: 0.0,
      token: '',
    };
  }

  navigateToCheckOutStep2 = () => {
    if (this.state.addressList.length !== 0) {
      this.props.navigation.navigate ('CheckOutStep2', {
        cartData: this.state.cartData,
        subtotal: this.state.subtotal,
        address: this.state.selectedValue,
        other: this.state.other,
        cartTotal: this.state.cartTotal,
        tax: this.state.tax,
      });
    } else {
      this.showToast ('Add address to proceed!');
    }
  };

  navigateToAddAddress = () => {
    this.props.navigation.navigate ('AddAddress', {
      previousRoute: 'CheckOutStep1',
    });
  };

  onSelect (index, value) {
    this.setState ({
      text: `Selected index: ${index} , value: ${value}`,
      selectedAddressIndex: index,
      selectedValue: value,
    });
  }

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  getLocation=async() => {
  //   console.warn ('inside geolocation: ', navigator.geolocation);
  //   try {
  //   const granted = await PermissionsAndroid.request(
  //     PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  //     {
  //       title: "Dial A Drink App Location Permission",
  //       message:
  //         "Cool Photo App needs access to your camera " +
  //         "so you can take awesome pictures.",
  //       buttonNeutral: "Ask Me Later",
  //       buttonNegative: "Cancel",
  //       buttonPositive: "OK"
  //     }
  //   );
  //   if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //     console.log("You can use the Location");
  //     Geolocation.getCurrentPosition((position) => {
  //       console.warn("Position " + JSON.stringify(position))});
  //   } else {
  //     console.log("Location permission denied");
  //   }
  // } catch (err) {
  //   console.warn(err);
  // }
 
  
    // Geolocation.getCurrentPosition(
    //   (position) => {
    //     console.warn("Position " + position.coords.latitude + " " + position.coords.longitude);
    //     var lat = position.coords.latitude;
    //     var lng = position.coords.longitude
    //     // var add = { lat:lat, lng: lng };
    //     // Geocoder.geocodePosition(add).then(res => {
    //     //   // res is an Array of geocoding object (see below)
    //     //   console.warn('res: ',res);
    //   // })
    //   // .catch(err => console.log(err))
    //   },
    //   (error) => Alert.alert('Error', JSON.stringify(error)),
    //   {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    // );
  };

  getData = () => {
    const {navigation} = this.props;
    var profile = this.props.userReducer.profile;
    if (profile) {
      var token = profile.token;
      this.setState ({token: token});
    }
    var cartData = navigation.getParam ('cartData', null);
    var subtotal = navigation.getParam ('subtotal', null);
    var cartTotal = navigation.getParam ('cartTotal', null);
    var tax = navigation.getParam ('tax', null);
    var other = navigation.getParam ('other', null);
    this.setState ({
      loader: true,
      subtotal: subtotal,
      cartData: cartData,
      other: other,
      tax: tax,
      cartTotal: cartTotal,
    });
    HOMEAPI.getRequestData (token, CONFIG.BASE_URL + CONFIG.VIEW_ADDRESS_URL)
      .then (
        function (res) {
          this.setState ({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                if (res.data.length != 0) {
                  var initialAddress =
                    res.data[0].name +
                    ',' +
                    res.data[0].house_name_number +
                    ',' +
                    res.data[0].lane +
                    '' +
                    res.data[0].city +
                    ',' +
                    res.data[0].phone_number;
                } else {
                  var initialAddress = '';
                }
                this.setState ({
                  addressList: res.data,
                  selectedValue: initialAddress,
                });
                break;
              case CONFIG.statusUnauthorized:
                this.showToast (
                  'Your account has been deactivated. Please logout and login with valid user credentials'
                );
                this.props.navigation.navigate ('Home');
                break;
              case CONFIG.statusError:
                this.showToast ('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          this.setState ({loader: false});
          this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  componentDidMount () {
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('#f2f2f2');
    // this.getLocation ();
    this.onLoad ();
    this.getData ();
  }

  componentWillUnmount () {
    // Remove the event listener
    this.didFocusListener.remove();
  }

  onLoad = () => {
    this.didFocusListener = this.props.navigation.addListener ('didFocus', () => this.getData ());
  };

  deleteAddress = address_id => {
    this.setState ({loader: true});
    let payload = {
      address_id: address_id,
    };
    HOMEAPI.postRequestData (
      payload,
      this.state.token,
      CONFIG.BASE_URL + CONFIG.DELETE_ADDRESS_URL
    )
      .then (
        function (res) {
          this.setState ({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                this.showToast ('Address deleted successfully');
                this.getData ();
                break;
              case CONFIG.statusUnauthorized:
                this.showToast (
                  'Your account has been deactivated. Please logout and login with valid user credentials'
                );
                this.props.navigation.navigate ('Home');
                break;
              case CONFIG.statusError:
                this.showToast ('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          this.setState ({loader: false});
          this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  render () {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}
        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />

          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}
          />
          <ScrollView style={styles.background}>
            <View style={styles.step1TextStyle}>
              <Text style={styles.step1Address}>
                {STRINGS.checkout.step1 + ' :- ' + STRINGS.checkout.address}
              </Text>
            </View>
            <View style={styles.step1Style}>
              <View style={styles.circle} />
              <Divider style={styles.step1} />
              <View style={styles.circle} />
            </View>
            {this.state.addressList !== ''
              ? <View style={styles.selectOrEditRow}>
                  <View style={styles.radioBtn}>
                    <RadioGroup
                      size={19}
                      thickness={1.2}
                      color={'#47170d'}
                      activeColor={'#47170d'}
                      selectedIndex={this.state.selectedAddressIndex}
                      onSelect={(index, value) => this.onSelect (index, value)}
                    >
                      {this.state.addressList.map (item => {
                        return (
                          <RadioButton
                            value={
                              item.name +
                                ', ' +
                                item.house_name_number +
                                ', ' +
                                item.lane +
                                ', ' +
                                item.city +
                                ', ' +
                                item.phone_number
                            }
                            key={item.id}
                            style={styles.radioStyle}
                          >
                            <View style={styles.addresses}>
                              <View style={styles.addressWidth}>
                                {item.name &&
                                  <Text style={styles.name}>{item.name}</Text>}
                                {item.house_name_number &&
                                  <Text style={styles.other}>
                                    {item.house_name_number}
                                  </Text>}
                                {item.lane &&
                                  <Text style={styles.other}>{item.lane}</Text>}
                                {item.city &&
                                  <Text style={styles.other}>{item.city}</Text>}
                                {item.phone_number &&
                                  <Text style={styles.other}>
                                    {item.phone_number}
                                  </Text>}
                              </View>
                              <View
                                style={{
                                  flexDirection: 'column',
                                  justifyContent: 'space-between',
                                }}
                              >
                                <TouchableOpacity
                                  style={styles.editAddressrow}
                                  onPress={() => {
                                    this.props.navigation.navigate (
                                      'EditAddress',
                                      {
                                        name: item.name,
                                        houseName: item.house_name_number,
                                        lane: item.lane,
                                        city: item.city,
                                        phnNumber: item.phone_number,
                                        address_id: item.id,
                                      }
                                    );
                                  }}
                                >
                                  <Image
                                    source={require ('../assets/edit.png')}
                                    tintColor="#47170d"
                                    style={styles.editAddressIcon}
                                  />
                                </TouchableOpacity>

                                <TouchableOpacity
                                  style={styles.editAddressrow}
                                  onPress={() => {
                                    Alert.alert (
                                      '',
                                      'Are you sure you want to delete the address ?',
                                      [
                                        {
                                          text: 'CANCEL',
                                          onPress: () =>
                                            console.log ('NO Pressed'),
                                          style: 'destructive',
                                          
                                        },
                                        {
                                          text: 'YES',
                                          onPress: () =>
                                            this.deleteAddress (item.id),
                                        },
                                      ],
                                      {cancelable: false}
                                    );
                                  }}
                                >
                                  <Image
                                    source={require ('../assets/delete.png')}
                                    tintColor="#47170d"
                                    style={styles.editAddressIcon}
                                  />
                                </TouchableOpacity>
                              </View>
                            </View>
                          </RadioButton>
                        );
                      })}
                    </RadioGroup>
                  </View>
                </View>
              : null}
            <View>
              <TouchableOpacity
                style={styles.addAddressRow}
                onPress={this.navigateToAddAddress}
              >
                <Image
                  source={require ('../assets/plus.png')}
                  tintColor="#47170d"
                  style={styles.addAddressIcon}
                />
                <Text style={styles.addAddress}>
                  {STRINGS.checkout.addAddress}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.submitBtnStyle}
                onPress={this.navigateToCheckOutStep2}
              >
                <Text style={styles.submit}>{STRINGS.checkout.submit}</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation} />
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateUserDetails: profileDetails => {
      dispatch (updateUserDetails (profileDetails));
    },
  };
};
export default connect (mapStateToProps, mapDispatchToProps) (checkOutStep1);

const styles = EStyleSheet.create ({
  background: {
    backgroundColor: '#d2d2d6',
    flex: 1,
  },
  circle: {
    width: 12,
    height: 12,
    borderRadius: 24 / 2,
    backgroundColor: 'green',
    borderColor: 'black',
    borderWidth: 0.5,
  },
  step1: {
    backgroundColor: 'green',
    width: '40%',
    height: '4rem',
  },
  step1Style: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '15rem',
    marginLeft: '30rem',
  },
  step1TextStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '30rem',
    marginLeft: '30rem',
  },
  step1Address: {
    fontSize: fontSizeValue * 18,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Bold' : 'Calisto MT',
    fontWeight:'bold',
    color: '#47170d',
  },
  submitBtnStyle: {
    backgroundColor: '#47170d',
    width: '290rem',
    height: '40rem',
    borderRadius: '6rem',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: '50rem',
    marginLeft: '45rem',
    marginBottom: '50rem',
  },
  submit: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
    letterSpacing: '0.6rem',
  },
  addAddress: {
    fontSize: fontSizeValue * 18,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Bold' : 'Calisto MT',
    fontWeight:'bold',
    color: '#47170d',
    marginLeft: '15rem',
  },
  addAddressRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: '30rem',
    marginTop: '20rem',
  },
  editAddressrow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  addAddressIcon: {
    width: '20rem',
    height: '20rem',
  },
  editAddressIcon: {
    width: '30rem',
    height: '30rem',
  },
  addresses: {
    flexDirection: 'row',
    paddingLeft: '20rem',
    marginBottom: '10rem',
    justifyContent: 'space-between',
    width: '300rem',
  },
  addressDivider: {
    backgroundColor: 'white',
    height: '1rem',
    width: '97%',
    marginLeft: '5rem',
    marginRight: '5rem',
  },
  name: {
    fontSize: fontSizeValue * 16,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
  },
  other: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
    marginTop: '3rem',
  },
  selectOrEditRow: {
    flexDirection: 'row',
    flex: 1,
    marginTop: '30rem',
    paddingLeft: '5rem',
    paddingRight: '5rem',
  },
  radioBtn: {
    flex: 1,
  },
  radioStyle: {
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    marginBottom: '10rem',
    paddingLeft: '25rem',
  },
  addressWidth: {
    width: '80%',
  },
});
