import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  Image,
  ScrollView,
  ActivityIndicator,
  Platform,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import Toast, {DURATION} from 'react-native-easy-toast';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import * as HOMEAPI from '../services/api-services/home-api';
import {updateUserDetails} from '../services/redux/actions/userActions';
import CONFIG from '../config/config';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: STRINGS.account.profile,
      backButton: true,
      bellIcon: false,
      name: '',
      emailId: '',
      phnNumber: '',
      avator: '',
      password: '',
      token: '',
      nameErrMsg: '',
      nameMsg: false,
      emailErrMsg: '',
      emailMsg: false,
      mobileErrMsg: '',
      mobileMsg: false,
    };
  }

  showToast = (toastMsg) => {
    this.toast.show(toastMsg);
  };

  componentDidMount() {
    NavigationBar.setStatusBarColor('#47170d', false);
    NavigationBar.setColor('#f2f2f2');
    let profile = this.props.userReducer.profile;
    if (profile) {
      this.setState({
        token: profile.token,
        name: profile.name,
        emailId: profile.email,
        phnNumber: profile.mobile_number,
        password: profile.password,
        oldEmail: profile.email,
        oldNumber: profile.mobile_number,
      });
    }
  }

  validate = () => {
    let isValidate = true;
    let toLowerCaseEmail = this.state.emailId.toLowerCase();
    let email = toLowerCaseEmail.trim();
    this.setState({email: email});
    if (!CONFIG.NAME_FORMAT.test(this.state.name)) {
      this.setState({
        nameMsg: true,
        nameErrMsg: 'Invalid name',
      });
      isValidate = false;
    }
    if (this.state.name.length <= 1 || this.state.name.length >= 30) {
      this.setState({
        nameMsg: true,
        nameErrMsg: 'Length of characters should be between 2-30',
      });
      isValidate = false;
    }
    if (email.length >= 30 || email == '' || !CONFIG.EMAIL_FORMAT.test(email)) {
      if (email == '') {
        this.setState({
          emailMsg: true,
          emailErrMsg: STRINGS.signUp.emailMsg,
        });
      } else if (!CONFIG.EMAIL_FORMAT.test(email)) {
        this.setState({
          emailMsg: true,
          emailErrMsg: 'Invalid Email Id',
        });
      } else {
        this.setState({
          emailMsg: true,
          emailErrMsg: 'Email Id can be maximum 25 characters',
        });
      }
      isValidate = false;
    }
    if (
      this.state.phnNumber.length < 7 ||
      this.state.phnNumber.length >= 16 ||
      !CONFIG.MOBILE_NUM_FORMAT.test(this.state.phnNumber)
    ) {
      if (this.state.phnNumber == '') {
        this.setState({
          mobileMsg: true,
          mobileErrMsg: STRINGS.signUp.mobileMsg,
        });
      } else {
        this.setState({
          mobileMsg: true,
          mobileErrMsg: 'Enter valid mobile number between 7-15 digits',
        });
      }
      isValidate = false;
    }
    if (isValidate) {
      this.editProfile();
    }
  };

  editProfile = () => {
    this.setState({loader: true});
    let toLowerCaseEmail = this.state.emailId.toLowerCase();
    let email = toLowerCaseEmail.trim();
    let payload = {
      name: this.state.name,
      email: email,
      mobile_number: this.state.phnNumber,
      avator: this.state.avator,
    };
    let profile = {};
    HOMEAPI.postRequestData(
      payload,
      this.state.token,
      CONFIG.BASE_URL + CONFIG.EDIT_PROFILE_URL,
    )
      .then(
        function (res) {
          this.setState({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                if (res.email_verify == false && res.mobile_verify == false) {
                  profile.email = email;
                  profile.name = this.state.name;
                  profile.mobile_number = this.state.phnNumber;
                  profile.token = this.state.token;
                  profile.password = this.state.password;
                  profile.avator = this.state.avator;
                  profile.isLoggedIn = true;
                  this.props.updateUserDetails(profile);
                  this.showToast('Profile updated successfully');
                  setTimeout(() => {
                    this.props.navigation.goBack();
                  }, 1000);
                } else {
                  AsyncStorage.setItem(
                    'isProfileOTPVerified',
                    JSON.stringify(false),
                  );
                  AsyncStorage.setItem('previousStorageRoute', 'Profile');
                  if (res.email_verify === true) {
                    AsyncStorage.setItem(
                      'emailVerifyStorage',
                      JSON.stringify(true),
                    );
                  } else {
                    AsyncStorage.setItem(
                      'emailVerifyStorage',
                      JSON.stringify(false),
                    );
                  }
                  if (res.mobile_verify === true) {
                    AsyncStorage.setItem(
                      'mobileVerifyStorage',
                      JSON.stringify(true),
                    );
                  } else {
                    AsyncStorage.setItem(
                      'mobileVerifyStorage',
                      JSON.stringify(false),
                    );
                  }
                  this.showToast('Profile updated successfully');
                  this.props.navigation.navigate('VerifyOtp', {
                    previousRoute: 'Profile',
                    email_verify: res.email_verify,
                    mobile_verify: res.mobile_verify,
                    email: this.state.email,
                    phnNumber: this.state.phnNumber,
                  });
                }
                break;
              case CONFIG.statusUnauthorized:
                this.showToast(
                  'Your account has been deactivated. Please logout and login with valid user credentials',
                );
                this.props.navigation.navigate('Home');
                break;
              case CONFIG.statusError:
                this.showToast('Something went wrong. Please try again!');
                break;
              default:
                this.setState({
                  emailMsg: true,
                  emailErrMsg: 'Invalid Email Id',
                });
                break;
            }
          } else {
            this.showToast('Something went wrong. Please try again!');
          }
        }.bind(this),
      )
      .catch(
        function (err) {
          this.setState({loader: false});
          this.showToast('Something went wrong. Please try again!');
        }.bind(this),
      );
  };

  render() {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}
        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />

          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader && (
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>
          )}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}
          />
          <ScrollView style={styles.background}>
            <View style={styles.formBackground}>
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(name) => {
                    this.setState({name: name, nameMsg: false});
                  }}
                  maxLength={25}
                  placeholderTextColor="black"
                  value={this.state.name}
                  placeholder={STRINGS.checkout.name}
                />
              </View>
              {this.state.nameMsg && (
                <View style={styles.alignRowCenter}>
                  <Text style={Styles.error}>{this.state.nameErrMsg}</Text>
                </View>
              )}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(emailId) => {
                    this.setState({emailId: emailId, emailMsg: false});
                  }}
                  placeholderTextColor="black"
                  value={this.state.emailId}
                  placeholder={STRINGS.account.emailId}
                />
              </View>
              {this.state.emailMsg && (
                <View style={styles.alignRowCenter}>
                  <View style={styles.errorRowWidth}>
                    <Text style={Styles.error}>{this.state.emailErrMsg}</Text>
                  </View>
                </View>
              )}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(phnNumber) => {
                    this.setState({phnNumber: phnNumber, mobileMsg: false});
                  }}
                  keyboardType="phone-pad"
                  placeholderTextColor="black"
                  value={this.state.phnNumber}
                  returnKeyType={ 'done' }
                  placeholder={STRINGS.account.phnNumber}
                />
              </View>
              {this.state.mobileMsg && (
                <View style={styles.alignRowCenter}>
                  <View style={styles.errorRowWidth}>
                    <Text style={Styles.error}>{this.state.mobileErrMsg}</Text>
                  </View>
                </View>
              )}
              <TouchableOpacity
                style={styles.submitBtnStyle}
                onPress={this.validate}>
                <Text style={styles.submit}>{STRINGS.account.save}</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={(ref) => {
              this.toast = ref;
            }}
          />
          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation} />
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUserDetails: (profileDetails) => {
      dispatch(updateUserDetails(profileDetails));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(profile);

// export default profile

const styles = EStyleSheet.create({
  background: {
    backgroundColor: '#d2d2d6',
  },
  submitBtnStyle: {
    backgroundColor: '#47170d',
    width: '290rem',
    height: '40rem',
    borderRadius: '6rem',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: '50rem',
    marginLeft: '25rem',
    marginBottom: '40rem',
  },
  submit: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
    letterSpacing: '0.6rem',
  },
  formBackground: {
    backgroundColor: 'white',
    marginTop: '30rem',
    paddingTop: '30rem',
    marginLeft: '10rem',
    marginRight: '10rem',
    paddingRight: '10rem',
    paddingLeft: '10rem',
  },
  textInput: {
    flex: 1,
    width: '100%',
    color: '#000000',
    paddingLeft: '10rem',
    fontSize: fontSizeValue * 12,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  inputBorderStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 2,
    borderBottomColor: '#ccc',
    height: '45rem',
    marginBottom: '10rem',
  },
  alignRowCenter: {
    marginLeft: '10rem',
  },
});
