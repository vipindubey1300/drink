import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  Platform,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {connect} from 'react-redux';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import NavigationBar from 'react-native-navbar-color';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Toast, {DURATION} from 'react-native-easy-toast';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import * as HOMEAPI from '../services/api-services/home-api';
import {updateUserDetails} from '../services/redux/actions/userActions';
import CONFIG from '../config/config';
import HTML from 'react-native-render-html';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class privacyPolicy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: STRINGS.account.privacyPolicy,
      backButton: true,
      bellIcon: false,
      token: '',
      loader: false,
      privacyPolicyContent: '',
    };
  }

  componentDidMount() {
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('#f2f2f2');
    var profile = this.props.userReducer.profile;
    if (profile) {
      var token = profile.token;
      this.setState({token: token});
    }
    const {navigation} = this.props;
    var privacyPolicyContent = navigation.getParam(
      'privacyPolicyContent',
      null,
    );
    this.setState({privacyPolicyContent: privacyPolicyContent});
  }

  render() {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}
        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader && (
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>
          )}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}
          />
          <ScrollView style={styles.background}>
            <View style={styles.formBackground}>
              {!this.state.privacyPolicyContent == '' && (
                <HTML html={this.state.privacyPolicyContent} />
              )}
              {/* <Text style={styles.termsTextStyle}>
                {
                  this.state.privacyPolicyContent
                }
              </Text> */}
            </View>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={(ref) => {
              this.toast = ref;
            }}
          />
          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation} />
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUserDetails: (profileDetails) => {
      dispatch(updateUserDetails(profileDetails));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(privacyPolicy);

const styles = EStyleSheet.create({
  background: {
    backgroundColor: '#d2d2d6',
  },
  formBackground: {
    backgroundColor: 'white',
    marginTop: '30rem',
    marginLeft: '10rem',
    marginRight: '10rem',
    paddingRight: '10rem',
    paddingLeft: '10rem',
    marginBottom: '30rem',
  },
  termsTextStyle: {
    fontSize: fontSizeValue * 12,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
  },
});
