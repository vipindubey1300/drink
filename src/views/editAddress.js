import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import {updateUserDetails} from '../services/redux/actions/userActions';
import {connect} from 'react-redux';
import Toast, {DURATION} from 'react-native-easy-toast';
import {TouchableOpacity} from 'react-native-gesture-handler';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import * as HOMEAPI from '../services/api-services/home-api';
import CONFIG from '../config/config';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class EditAddress extends Component {
  constructor (props) {
    super (props);
    this.state = {
      title: STRINGS.checkout.editAddress,
      backButton: true,
      bellIcon: false,
      name: '',
      houseName: '',
      lane: '',
      city: '',
      phnNumber: '',
      loader: false,
      token: '',
      nameFieldMsg: '',
      houseNumberMsg: '',
      laneFieldMsg: '',
      cityFieldMsg: '',
      phnNumberFieldMsg: '',
      address_id: '',
      nameMsg: false,
      cityMsg: false,
      laneMsg: false,
      houseMsg: false,
      phnNumberMsg: false,
      orgName: '',
      orgHouseName: '',
      orgLane: '',
      orgCity: '',
      orgPhnNumber: '',
    };
  }

  componentDidMount () {
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('#f2f2f2');
    var profile = this.props.userReducer.profile;
    const {navigation} = this.props;
    var name = navigation.getParam ('name', '');
    var houseName = navigation.getParam ('houseName', '');
    var lane = navigation.getParam ('lane', '');
    var city = navigation.getParam ('city', '');
    var phnNumber = navigation.getParam ('phnNumber', '');
    var address_id = navigation.getParam ('address_id', '');
    this.setState ({
      name: name,
      houseName: houseName,
      lane: lane,
      city: city,
      phnNumber: phnNumber,
      address_id: address_id,
      orgName: name,
      orgHouseName: houseName,
      orgLane: lane,
      orgCity: city,
      orgPhnNumber: phnNumber,
      address_id: address_id,
    });
    if (profile) {
      var token = profile.token;
      this.setState ({
        token: token,
      });
    }
  }

  navigateToCheckOutStep1 = () => {
    setTimeout (() => {
      this.props.navigation.goBack ();
    }, 1000);
  };

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  validate = () => {
    let houseName = this.state.houseName.trim ();
    let lane = this.state.houseName.trim ();
    let isValidate = true;
    if (
      !CONFIG.NAME_FORMAT.test (this.state.name) ||
      this.state.name.length <= 1 ||
      this.state.name.length >= 25
    ) {
      if (this.state.name == '')
        this.setState ({
          nameFieldMsg: 'The name field is required',
          nameMsg: true,
        });
      else if (this.state.name.length <= 1 || this.state.name.length >= 30) {
        this.setState ({
          nameMsg: true,
          nameFieldMsg: 'Length of characters should be between 2-30',
        });
      } else {
        this.setState ({
          nameMsg: true,
          nameFieldMsg: 'Invalid name',
        });
      }
      isValidate = false;
    }
    if (houseName.length <= 1 || houseName.length >= 30 || houseName === '') {
      if (houseName === '') {
        this.setState ({
          houseNumberMsg: 'The house name and number field is required',
          houseMsg: true,
        });
      } else {
        this.setState ({
          houseMsg: true,
          houseNumberMsg: 'Length of characters should be between 2-30',
        });
      }
      isValidate = false;
    }
    if (lane.length <= 1 || lane.length >= 30 || lane === '') {
      if (lane === '') {
        this.setState ({
          laneFieldMsg: 'The lane field is required',
          laneMsg: true,
        });
      } else {
        this.setState ({
          laneMsg: true,
          laneFieldMsg: 'Length of characters should be between 2-30',
        });
      }
      isValidate = false;
    }
    if (!CONFIG.CITY_FORMAT.test (this.state.city)) {
      if (this.state.city === '') {
        this.setState ({
          cityFieldMsg: 'The city field is required',
          cityMsg: true,
        });
      } else if (this.state.city.length <= 1 || this.state.city.length >= 30) {
        this.setState ({
          cityMsg: true,
          cityFieldMsg: 'Length of characters should be between 2-30',
        });
      } else {
        this.setState ({
          cityMsg: true,
          cityFieldMsg: 'Invalid City Name',
        });
      }
      isValidate = false;
    }
    if (
      this.state.phnNumber.length < 7 ||
      this.state.phnNumber.length >= 16 ||
      this.state.phnNumber == '' || !CONFIG.MOBILE_NUM_FORMAT.test(this.state.phnNumber)
    ) {
      if (this.state.phnNumber == '') {
        this.setState ({
          phnNumberFieldMsg: 'The mobile number field is required',
          phnNumberMsg: true,
        });
      } else {
        this.setState ({
          phnNumberMsg: true,
          phnNumberFieldMsg: 'Enter valid mobile number between 7-15 digits',
        });
      }
      isValidate = false;
    }
    if (isValidate) {
      this.editAddress ();
    }
  };

  editAddress = () => {
    if (
      this.state.name == this.state.orgName &&
      this.state.houseName == this.state.orgHouseName &&
      this.state.lane == this.state.orgLane &&
      this.state.city == this.state.orgCity &&
      this.state.phnNumber == this.state.orgPhnNumber
    ) {
      this.showToast ('Nothing to edit !');
      setTimeout (() => {
        this.props.navigation.goBack ();
      }, 2000);
    } else {
      this.setState ({loader: true});
      let payload = {
        address_id: this.state.address_id,
        name: this.state.name,
        house_name_number: this.state.houseName,
        lane: this.state.lane,
        city: this.state.city,
        phone_number: this.state.phnNumber,
      };
      HOMEAPI.postRequestData (
        payload,
        this.state.token,
        CONFIG.BASE_URL + CONFIG.EDIT_ADDRESS_URL
      )
        .then (
          function (res) {
            this.setState ({loader: false});
            if (res != undefined) {
              let code = res.code;
              switch (code) {
                case CONFIG.statusOk:
                  this.showToast ('Address updated successfully');
                  this.navigateToCheckOutStep1 ();
                  break;
                case CONFIG.statusUnauthorized:
                  this.showToast ('Your account has been deactivated. Please logout and login with valid user credentials');
                  this.props.navigation.navigate('Home');
                  break;
                case CONFIG.statusError:
                  this.showToast ('Something went wrong. Please try again!');
                  break;
              }
            } else {
              this.showToast ('Something went wrong. Please try again!');
            }
          }.bind (this)
        )
        .catch (
          function (err) {
            this.setState ({loader: false});
            this.showToast ('Something went wrong. Please try again!');
          }.bind (this)
        );
    }
  };

  render () {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}

        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}
          />
          <ScrollView style={styles.background}>
            <View style={styles.formBackground}>
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={name => {
                    this.setState ({name: name,nameMsg:false});
                  }}
                  placeholderTextColor="black"
                  value={this.state.name}
                  placeholder={STRINGS.checkout.name}
                />
              </View>
              {this.state.nameMsg &&
                <View style={styles.alignRowCenter}>
                  <Text style={Styles.error}>{this.state.nameFieldMsg}</Text>
                </View>}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={houseName => {
                    this.setState ({houseName: houseName, houseMsg: false});
                  }}
                  placeholderTextColor="black"
                  value={this.state.houseName}
                  placeholder={STRINGS.checkout.houseName}
                />
              </View>
              {this.state.houseMsg &&
                <View style={styles.alignRowCenter}>
                  <Text style={Styles.error}>{this.state.houseNumberMsg}</Text>
                </View>}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={lane => {
                    this.setState ({lane: lane, laneMsg: false});
                  }}
                  placeholderTextColor="black"
                  value={this.state.lane}
                  placeholder={STRINGS.checkout.lane}
                />
              </View>
              {this.state.laneMsg &&
                <View style={styles.alignRowCenter}>
                  <Text style={Styles.error}>{this.state.laneFieldMsg}</Text>
                </View>}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={city => {
                    this.setState ({city: city, cityMsg: false});
                  }}
                  placeholderTextColor="black"
                  value={this.state.city}
                  placeholder={STRINGS.checkout.city}
                />
              </View>
              {this.state.cityMsg &&
                <View style={styles.alignRowCenter}>
                  <Text style={Styles.error}>{this.state.cityFieldMsg}</Text>
                </View>}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={phnNumber => {
                    this.setState ({phnNumber: phnNumber, phnNumberMsg: false});
                  }}
                  keyboardType="phone-pad"
                  placeholderTextColor="black"
                  returnKeyType={ 'done' }
                  value={this.state.phnNumber}
                  placeholder={STRINGS.checkout.phnNumber}
                />
              </View>
              {this.state.phnNumberMsg &&
                <View style={styles.alignRowCenter}>
                  <Text style={Styles.error}>
                    {this.state.phnNumberFieldMsg}
                  </Text>
                </View>}
              <TouchableOpacity
                style={styles.submitBtnStyle}
                onPress={this.validate}
              >
                <Text style={styles.submit}>{STRINGS.checkout.add}</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation} />
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateUserDetails: profileDetails => {
      dispatch (updateUserDetails (profileDetails));
    },
  };
};
export default connect (mapStateToProps, mapDispatchToProps) (EditAddress);

const styles = EStyleSheet.create ({
  background: {
    backgroundColor: '#d2d2d6',
  },
  submitBtnStyle: {
    backgroundColor: '#47170d',
    width: '290rem',
    height: '40rem',
    borderRadius: '6rem',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: '50rem',
    marginLeft: '25rem',
    marginBottom: '40rem',
  },
  submit: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
    letterSpacing: '0.6rem',
  },
  formBackground: {
    backgroundColor: 'white',
    paddingTop: '30rem',
    marginTop: '30rem',
    marginLeft: '10rem',
    marginRight: '10rem',
    paddingRight: '10rem',
    paddingLeft: '10rem',
  },
  textInput: {
    flex: 1,
    width: '100%',
    color: '#000000',
    paddingLeft: '10rem',
    fontSize: fontSizeValue * 12,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  inputBorderStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 2,
    borderBottomColor: '#ccc',
    height: '45rem',
    marginBottom: '10rem',
  },
  alignRowCenter: {
    marginLeft: '10rem',
  },
});
