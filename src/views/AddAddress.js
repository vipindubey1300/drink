import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Image,
  Text,
  Alert,
  TextInput,
  ScrollView,
  ActivityIndicator,
  Platform,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import {updateUserDetails} from '../services/redux/actions/userActions';
import {connect} from 'react-redux';
import Toast, {DURATION} from 'react-native-easy-toast';
import {TouchableOpacity} from 'react-native-gesture-handler';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import * as HOMEAPI from '../services/api-services/home-api';
import CONFIG from '../config/config';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoder';
// import Geolocation from 'react-native-geolocation-service'; 
// import Geocoder from 'react-native-geocoding'; 
 //Geolocation.setRNConfiguration(config);


const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class AddAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: STRINGS.checkout.addAddress,
      backButton: true,
      bellIcon: false,
      name: '',
      houseName: '',
      lane: '',
      city: '',
      phnNumber: '',
      loader: false,
      previousRoute: '',
      token: '',
      nameFieldMsg: '',
      houseNumberMsg: '',
      laneFieldMsg: '',
      cityFieldMsg: '',
      phnNumberFieldMsg: '',
      nameMsg: false,
      cityMsg: false,
      laneMsg: false,
      houseMsg: false,
      phnNumberMsg: false,
      initialPosition: 'unknown',
    lastPosition: 'unknown',
    latitude:'',
    longitude:'',
    };
  }

  componentDidMount() {
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('#f2f2f2');
    var profile = this.props.userReducer.profile;
    const {navigation} = this.props;
    if (profile) {
      var token = profile.token;
      this.setState({
        token: token,
      });
    }

  // this.MyLocation();
  }

  MyLocation= async ()=>{
    try {
    
      Geolocation.getCurrentPosition(
        async (position) => {
          const initialPosition = JSON.stringify(position);
          this.setState({initialPosition:initialPosition});
        
          const NY = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
             };
            // console.log('position',NY)
          const res = await Geocoder.geocodePosition(NY);
         console.log('Ashish kumarvvvvvvv-------',res)
    // console.log('Ashish -------',res)

   
    var addr  = res[0].formattedAddress
    var streetName = res[0].feature ? res[0].feature : ''
    var locality = res[0].subLocality ? res[0].subLocality : ''
    var city = res[0].locality ? res[0].locality : ''
    var country = res[0].country ? res[0].country : ''

   this.setState({lane:streetName,city:city})
//houseName:locality
    var profile = this.props.userReducer.profile;
    if (profile) {
       var token = profile.token;
      this.setState({
        name: profile.name,
        phnNumber: profile.mobile_number,
      });
    }
        },
        error => Alert.alert('Error', JSON.stringify(error)),
        // {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
        {
          enableHighAccuracy: false,
          timeout: 5000, 
          maximumAge: 10000
         }

      );

  }
  catch(err) {
      console.log(err);
  }
  }

  componentWillUnmount() {
    this.watchID != null && Geolocation.clearWatch(this.watchID);
  }


  navigateToCheckOutStep1 = () => {
    setTimeout(() => {
      this.props.navigation.goBack();
    }, 1000);
  };

  showToast = (toastMsg) => {
    this.toast.show(toastMsg);
  };

  validate = () => {
    let houseName = this.state.houseName.trim();
    let lane = this.state.lane.trim();
    let isValidate = true;
    if (
      !CONFIG.NAME_FORMAT.test(this.state.name) ||
      this.state.name.length <= 1 ||
      this.state.name.length >= 30
    ) {
      if (this.state.name == '')
        this.setState({
          nameFieldMsg: 'The name field is required',
          nameMsg: true,
        });
      else if (this.state.name.length <= 1 || this.state.name.length >= 30) {
        this.setState({
          nameMsg: true,
          nameFieldMsg: 'Length of characters should be between 2-30',
        });
      } else {
        this.setState({
          nameMsg: true,
          nameFieldMsg: 'Invalid name',
        });
      }
      isValidate = false;
    }
    if (houseName.length <= 1 || houseName.length >= 30) {
      if (houseName === '') {
        this.setState({
          houseNumberMsg: 'The house name and number field is required',
          houseMsg: true,
        });
      } else {
        this.setState({
          houseMsg: true,
          houseNumberMsg: 'Length of characters should be between 2-30',
        });
      }
      isValidate = false;
    }
    if (lane.length <= 1 || lane.length >= 30) {
      if (lane === '') {
        this.setState({
          laneFieldMsg: 'The lane field is required',
          laneMsg: true,
        });
      } else {
        this.setState({
          laneMsg: true,
          laneFieldMsg: 'Length of characters should be between 2-30',
        });
      }
      isValidate = false;
    }
    if (!CONFIG.CITY_FORMAT.test(this.state.city)) {
      if (this.state.city === '') {
        this.setState({
          cityFieldMsg: 'The city field is required',
          cityMsg: true,
        });
      } else if (this.state.city.length <= 1 || this.state.city.length >= 30) {
        this.setState({
          cityMsg: true,
          cityFieldMsg: 'Length of characters should be between 2-30',
        });
      } else {
        this.setState({
          cityMsg: true,
          cityFieldMsg: 'Invalid City Name',
        });
      }
      isValidate = false;
    }
    if (this.state.phnNumber.length < 7 || this.state.phnNumber.length >= 16 || !CONFIG.MOBILE_NUM_FORMAT.test(this.state.phnNumber)) {
      if (this.state.phnNumber == '') {
        this.setState({
          phnNumberFieldMsg: 'The mobile number field is required',
          phnNumberMsg: true,
        });
      } else {
        this.setState({
          phnNumberMsg: true,
          phnNumberFieldMsg: 'Enter valid mobile number between 7-15 digits',
        });
      }
      isValidate = false;
    }
    if (isValidate) {
      this.addAddress();
    }
  };

  addAddress = () => {
    this.setState({loader: true});
    let payload = {
      name: this.state.name.trim(),
      house_name_number: this.state.houseName.trim(),
      lane: this.state.lane.trim(),
      city: this.state.city.trim(),
      phone_number: this.state.phnNumber,
    };
    HOMEAPI.postRequestData(
      payload,
      this.state.token,
      CONFIG.BASE_URL + CONFIG.ADD_ADDRESS_URL,
    )
      .then(
        function (res) {
          this.setState({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                this.showToast('Address added successfully');
                this.navigateToCheckOutStep1();
                break;
              case CONFIG.statusUnauthorized:
                this.showToast('Your account has been deactivated. Please logout and login with valid user credentials');
                this.props.navigation.navigate('Home');
                break;
              case CONFIG.statusError:
                this.showToast('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast('Something went wrong. Please try again!');
          }
        }.bind(this),
      )
      .catch(
        function (err) {
          this.setState({loader: false});
          this.showToast('Something went wrong. Please try again!');
        }.bind(this),
      );
  };

  render() {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}
        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader && (
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>
          )}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}></Header>
          <ScrollView style={styles.background}>
          <TouchableOpacity style={{marginTop:10,flexDirection:'row',backgroundColor:"#47170d",width:180,padding:10,marginLeft:12,borderRadius:7}} onPress={()=>{this.MyLocation()}}>
            <Text style={{color:'white'}}>Select Your Location</Text>
  <View style={{width:20,height:20,marginLeft:5,marginRight:10}}>

            <Image source={require('../assets/location.png')}style={{width:20,height:20,borderRadius:10}}></Image>
            
</View>
            </TouchableOpacity>

            <View style={styles.formBackground}>

           
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(name) => {
                    this.setState({name: name,nameMsg:false});
                  }}
                  placeholderTextColor="black"
                  value={this.state.name}
                  placeholder={STRINGS.checkout.name}
                />
              </View>
              {this.state.nameMsg && (
                <View style={styles.alignRowCenter}>
                  <Text style={Styles.error}>{this.state.nameFieldMsg}</Text>
                </View>
              )}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(houseName) => {
                    this.setState({houseName: houseName, houseMsg: false});
                  }}
                  placeholderTextColor="black"
                  value={this.state.houseName}
                  placeholder={STRINGS.checkout.houseName}
                />
              </View>
              {this.state.houseMsg && (
                <View style={styles.alignRowCenter}>
                  <Text style={Styles.error}>{this.state.houseNumberMsg}</Text>
                </View>
              )}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(lane) => {
                    this.setState({lane: lane, laneMsg: false});
                  }}
                  placeholderTextColor="black"
                  value={this.state.lane}
                  placeholder={STRINGS.checkout.lane}
                />
              </View>
              {this.state.laneMsg && (
                <View style={styles.alignRowCenter}>
                  <Text style={Styles.error}>{this.state.laneFieldMsg}</Text>
                </View>
              )}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(city) => {
                    this.setState({city: city, cityMsg: false});
                  }}
                  placeholderTextColor="black"
                  value={this.state.city}
                  placeholder={STRINGS.checkout.city}
                />
              </View>
              {this.state.cityMsg && (
                <View style={styles.alignRowCenter}>
                  <Text style={Styles.error}>{this.state.cityFieldMsg}</Text>
                </View>
              )}
              <View style={styles.inputBorderStyle}>
                <TextInput
                  style={styles.textInput}
                  onChangeText={(phnNumber) => {
                    this.setState({phnNumber: phnNumber, phnNumberMsg: false});
                  }}
                  keyboardType="phone-pad"
                  placeholderTextColor="black"
                  value={this.state.phnNumber}
                  returnKeyType={ 'done' }
                  placeholder={STRINGS.checkout.phnNumber}
                />
              </View>
              {this.state.phnNumberMsg && (
                <View style={styles.alignRowCenter}>
                  <Text style={Styles.error}>
                    {this.state.phnNumberFieldMsg}
                  </Text>
                </View>
              )}
              <TouchableOpacity
                style={styles.submitBtnStyle}
                onPress={this.validate}>
                <Text style={styles.submit}>{STRINGS.checkout.add}</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={(ref) => {
              this.toast = ref;
            }}
          />
          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation}></Footer>
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUserDetails: (profileDetails) => {
      dispatch(updateUserDetails(profileDetails));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddAddress);

const styles = EStyleSheet.create({
  background: {
    backgroundColor: '#d2d2d6',
  },
  submitBtnStyle: {
    backgroundColor: '#47170d',
    width: '290rem',
    height: '40rem',
    borderRadius: '6rem',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: '50rem',
    marginLeft: '25rem',
    marginBottom: '40rem',
  },
  submit: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
    letterSpacing: '0.6rem',
  },
  formBackground: {
    backgroundColor: 'white',
    paddingTop: '30rem',
    marginTop: '30rem',
    marginLeft: '10rem',
    marginRight: '10rem',
    paddingRight: '10rem',
    paddingLeft: '10rem',
  },
  textInput: {
    flex: 1,
    width: '100%',
    color: '#000000',
    paddingLeft: '10rem',
    fontSize: fontSizeValue * 12,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  inputBorderStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 2,
    borderBottomColor: '#ccc',
    height: '45rem',
    marginBottom: '10rem',
  },
  alignRowCenter: {
    marginLeft: '10rem',
  },
});
