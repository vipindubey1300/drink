import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  Image,
  FlatList,
  ActivityIndicator,
  Platform,
  ScrollView,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import Toast, {DURATION} from 'react-native-easy-toast';
import {connect} from 'react-redux';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import {Divider} from 'react-native-elements';
import CartListCard from '../components/shared/cartCard';
import {productUpdateDetails} from '../services/redux/actions/productActions';
import {updateUserDetails} from '../services/redux/actions/userActions';
import * as HOMEAPI from '../services/api-services/home-api';
import CONFIG from '../config/config';
import FilterComponent from '../components/shared/filter';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class productListing extends Component {
  constructor (props) {
    super (props);
    this.state = {
      title: '',
      search: '',
      backButton: true,
      bellIcon: true,
      showAddButton: true,
      searchResults: [],
      token: '',
      noResultsFound: false,
      productsData: [],
      categories: [],
      item: {},
      selectedItem: null,
      isSelected: false,
      
    };
  }

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  componentDidMount = () => {
    NavigationBar.setStatusBarColor ('#47170d', false);
    NavigationBar.setColor ('#f2f2f2');
    this.getData();
    this.onLoad();
  };

  getData = () => {
    const {navigation} = this.props;
    var title = navigation.getParam ('title', null);
    var searchList = navigation.getParam ('searchList', null);
    var selectedItem = navigation.getParam ('selectedItem', null);
    var profile = this.props.userReducer.profile;
    var productsData = this.props.productsReducer.products;
    var categories = this.props.productsReducer.products.categories;
    {
      categories.map (item => {
        if (selectedItem == item.id) {
          this.setState ({isSelected: true});
        }
      });
    }
    this.setState ({
      productsData: productsData,
      categories: categories,
      title: title,
      selectedItem: selectedItem,
    });
    if (profile) {
      var token = profile.token;
      this.setState ({token: token});
    }
    if (searchList == null || searchList.length == 0) {
      if (productsData) {
        this.setState ({noResultsFound: true});
      }
    } else {
      this.setState ({searchResults: searchList, noResultsFound: false});
    }
  };

  onLoad = () => {
    this.didFocusListener = this.props.navigation.addListener ('didFocus', () => this.getData());
  };

  componentWillUnmount () {
    // Remove the event listener
    this.didFocusListener.remove();
  }

  updateSearch = search => {
    this.setState ({search});
  };

  showProductsList = ({item}) => {
    let elipsis = '...';
    let drinkName = '';
    if (item.product_title.length > 50) {
      var resStr = item.product_title.substr (0, 50);
      var resultString = resStr.concat (elipsis);
      drinkName = resultString;
    } else {
      drinkName = item.product_title;
    }
    return (
      <CartListCard
        imageUrl={item.product_image}
        drinkName={drinkName}
        product_quantity={item.product_quantity}
        // quantity={item.cart_quantity}
        offer={item.product_discount}
        deliveryTime={item.product_delivery_time}
        showAddButton={this.state.showAddButton}
        price={item.price}
        productId={item.id}
        cart={'listing'}
        item={item}
        navigation={this.props.navigation}
      />
    );
  };

  displayResults = () => {
    if (!this.state.search == '') {
      this.setState ({loader: true});
      let params = {
        keyword: this.state.search,
      };
      let elipsis = '...';
      let title = '';
      if (this.state.search.length > 16) {
        let resStr = this.state.search.substr (0, 16);
        let resultString = resStr.concat (elipsis);
        title = resultString;
      } else {
        title = this.state.search;
      }
      HOMEAPI.getSearchData (
        this.state.token,
        CONFIG.BASE_URL + CONFIG.SEARCH_PRODUCT_URL,
        params
      )
        .then (
          function (res) {
            this.setState ({loader: false});
            if (res != undefined) {
              let code = res.code;
              switch (code) {
                case CONFIG.statusOk:
                  if (res.data.length == 0) {
                    this.setState ({
                      title: title,
                      noResultsFound: true,
                    });
                  } else {
                    this.setState ({
                      title: title,
                      searchResults: res.data,
                    });
                  }
                  break;
                case CONFIG.statusUnauthorized:
                  this.showToast (
                    'Your account has been deactivated. Please logout and login with valid user credentials'
                  );
                  this.props.navigation.navigate ('Home');
                  break;
                case CONFIG.statusError:
                  this.showToast ('Search box cannot be empty!');
                  break;
              }
            } else {
              this.showToast ('Something went wrong. Please try again!');
            }
          }.bind (this)
        )
        .catch (
          function (err) {
            this.setState ({loader: false});
            this.showToast ('Something went wrong. Please try again!');
          }.bind (this)
        );
    } else {
      this.showToast ('Search box cannot be empty!');
    }
  };

  renderFooter = () => {
    return <View style={styles.listFooterStyle} />;
  };

  renderHeader = () => {
    return <View style={styles.listHeaderStyle} />;
  };

  showCategories = ({item}) => {
    const isSelected = this.state.selectedItem === item.id;
    return (
      <View>
        <View style={styles.filters}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={
              isSelected == false ? styles.category : styles.categoryBorder
            }
            onPress={() => {
              this.setState ({
                selectedItem: item.id,
              });
              if (item.category_name == 'All') {
                this.setState ({
                  title: STRINGS.home.all,
                  searchResults: this.state.productsData.products,
                  noResultsFound: false,
                });
              } else {
                let productSearchList = this.state.productsData.products.filter (
                  product => {
                    return product.category_name == item.category_name;
                  }
                );
                if (
                  productSearchList == null ||
                  productSearchList.length == 0
                ) {
                  this.setState ({
                    title: item.display_name,
                    noResultsFound: true,
                  });
                } else {
                  this.setState ({
                    title: item.display_name,
                    searchResults: productSearchList,
                    noResultsFound: false,
                  });
                }
              }
            }}
          >
            <Text style={styles.categoryStyle}>{item.category_name}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render () {
    return (
      <Fragment>
        {/* <SafeAreaView style={[Styles.listingScreen]}> */}
        <View style={[Styles.listingScreen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}
          ></Header>
          <View style={styles.searchBar1} />
          <View style={styles.searchBar2}>
            <View style={styles.inputDirectionStylesUser}>
              <TextInput
                style={styles.textInput}
                onChangeText={search => {
                  this.setState ({search: search});
                }}
                placeholderTextColor={'#47170d'}
                value={this.state.search}
                placeholder={STRINGS.productListing.search}
                onSubmitEditing={this.displayResults}
              />
              <TouchableOpacity onPress={this.displayResults}>
                <Image
                  source={require ('../assets/search.png')}
                  style={styles.searchIconStyle}
                />
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <Divider style={styles.dividerStyleCategories} />
            <FlatList
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              data={this.state.categories}
              renderItem={this.showCategories}
              keyExtractor={item => item.id.toString ()}
            />
            <Divider style={styles.dividerStyleCategories} />
          </View>
          {this.state.noResultsFound &&
            <View style={styles.noResults}>
              <Image
                source={require ('../assets/NoResults.png')}
                style={styles.emptyCartImgStyle}
              />
            </View>}
          {!this.state.noResultsFound &&
            <View style={styles.listContainer}>
              <FlatList
                showsVerticalScrollIndicator={false}
                scrollEnabled={true}
                data={this.state.searchResults}
                renderItem={this.showProductsList}
                keyExtractor={item => item.id.toString ()}
                ListFooterComponent={this.renderFooter}
                ListHeaderComponent={this.renderHeader}
              />
            </View>}
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation} />
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {userReducer, productsReducer} = state;
  return {userReducer, productsReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateUserDetails: profileDetails => {
      dispatch (updateUserDetails (profileDetails));
    },
    productUpdateDetails: products => {
      dispatch (productUpdateDetails (products));
    },
  };
};

export default connect (mapStateToProps, mapDispatchToProps) (productListing);

const styles = EStyleSheet.create ({
  searchBar1: {
    flexDirection: 'row',
    backgroundColor: '#47170d',
    height: '35rem',
    justifyContent: 'center',
  },
  searchBar2: {
    flexDirection: 'row',
    height: '35rem',
    justifyContent: 'center',
    backgroundColor: '#d2d2d6',
  },
  inputDirectionStylesUser: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: '40rem',
    width: '90%',
    borderRadius: '5rem',
    paddingLeft: '10rem',
    paddingRight: '10rem',
    top: '-23rem',
  },
  layoutMargins: {
    marginRight: '15rem',
  },
  searchIconStyle: {
    padding: '10rem',
    margin: '10rem',
    height: '25rem',
    width: '25rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    width: '100%',
    color: '#47170d',
    paddingLeft: '10rem',
    fontSize: fontSizeValue * 11,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  listContainer: {
    flex: 1,
  },
  noResults: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  listFooterStyle: {
    marginBottom: '20rem',
  },
  listHeaderStyle: {
    marginTop: '20rem',
  },
  filters: {
    flexDirection: 'row',
    marginLeft: '8rem',
  },
  category: {
    paddingTop: '10rem',
    paddingBottom: '10rem',
    paddingLeft: '20rem',
    paddingRight: '20rem',
    elevation: 20,
    borderRadius: '8rem',
    borderRightWidth: '4rem',
    borderLeftWidth: '4rem',
    borderTopWidth: '1.5rem',
    borderBottomWidth: '1.5rem',
    borderColor:'rgba(128,128,128,0.5)',

    marginBottom: '10rem',
    marginTop: '10rem',
    marginRight: '10rem',
    backgroundColor: 'white',
  },
  categoryBorder: {
    paddingTop: '10rem',
    paddingBottom: '10rem',
    paddingLeft: '20rem',
    paddingRight: '20rem',
    elevation: 1,
    borderRadius: '8rem',
    marginBottom: '10rem',
    marginTop: '10rem',
    marginRight: '10rem',
    backgroundColor: 'rgba(18,176,7,0.1)',
  },
  dividerStyleCategories: {
    backgroundColor: '#47170d',
    height: 1,
    width: '97%',
    marginLeft: '5rem',
  },
  categoryStyle: {
    color: 'black',
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  emptyCartImgStyle: {
    height: '200rem',
    width: '200rem',
  },
});
