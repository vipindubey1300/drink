import React, {Component, Fragment} from 'react';
import {
  ImageBackground,
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  Image,
  ScrollView,
  BackHandler,
  ActivityIndicator,
  Platform,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import Toast, {DURATION} from 'react-native-easy-toast';
import AsyncStorage from '@react-native-community/async-storage';
import {updateUserDetails} from '../services/redux/actions/userActions';
import {connect} from 'react-redux';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import CheckBox from 'react-native-check-box';
import Styles from '../styles/styles';
import CONFIG from '../config/config';
import * as LOGINAPI from '../services/api-services/login-api';
import ageVerificationPopup from './ageVerificationPopup'

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();
class signUp extends Component {
  constructor (props) {
    super (props);
    this.state = {
      name: '',
      phnNumber: '',
      email: '',
      password: '',
      isChecked: false,
      passMsg: false,
      emailMsg: false,
      mobileMsg: false,
      nameMsg: false,
      passErrMsg: STRINGS.signUp.passMsg,
      emailErrMsg: STRINGS.signUp.emailMsg,
      mobileErrMsg: STRINGS.signUp.mobileMsg,
      nameErrMsg: STRINGS.signUp.nameMsg,
      loader: false,
      count: 0,
    };
  }

  componentWillUnmount () {
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.handleBackButton
    );
    this.didFocusListener.remove();
  }

  componentDidMount () {
    BackHandler.addEventListener ('hardwareBackPress', this.handleBackButton);
    NavigationBar.setStatusBarColor('white',false)
    NavigationBar.setColor('white');
    this.setState ({
      passMsg: false,
      emailMsg: false,
      mobileMsg: false,
      nameMsg: false,
      loader: false,
    });
    this.onload ();
//this.varifyAge();
    
  }
  
  // varifyAge = ()=>{
  //   return(<ageVerificationPopup />)
    
  // }

  onload = () => {
    this.didFocusListener = this.props.navigation.addListener ('didFocus', () =>
      BackHandler.addEventListener ('hardwareBackPress', this.handleBackButton)
    );
  };

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  navigateToSignIn = async () => {
    await AsyncStorage.setItem ('isVerified', JSON.stringify (true));
    this.props.navigation.navigate ('SignIn');
  };

  handleBackButton = () => {
    
      let count = this.state.count;
    if (count == 1) {
      BackHandler.exitApp ();
      return true;
    } else {
      this.showToast ('Press back again to leave');
      this.setState ({count: 1});
      return true;
    } 
  
   
  };

  validate = () => {
    let isValidate = true;
    let toLowerCaseEmail = this.state.email.toLowerCase ();
    let email = toLowerCaseEmail.trim ();
    this.setState ({email: email});
    let checkMsg = 'Please agree to the terms and conditions';
    if (this.state.password.length < 8 || this.state.password.length >= 16) {
      if (!CONFIG.PASSWORD_POLICY.test (this.state.password)) {
        this.setState ({
          passMsg: true,
        });
      } else {
        this.setState ({
          passMsg: true,
        });
      }
      isValidate = false;
    }
    if (
      email.length >= 64 ||
      email == '' ||
      !CONFIG.EMAIL_FORMAT.test (email)
    ) {
      if (email == '') {
        this.setState ({
          emailMsg: true,
          emailErrMsg: STRINGS.signUp.emailMsg,
        });
      } else if (!CONFIG.EMAIL_FORMAT.test (email)) {
        this.setState ({
          emailMsg: true,
          emailErrMsg: 'Invalid Email Id',
        });
      } else {
        this.setState ({
          emailMsg: true,
          emailErrMsg: 'Email Id can be maximum 64 characters',
        });
      }
      isValidate = false;
    }
    if (!CONFIG.NAME_FORMAT.test (this.state.name)) {
      if (this.state.name == '')
        this.setState ({
          nameMsg: true,
        });
      else if (this.state.name.length <= 1 || this.state.name.length >= 30) {
        this.setState ({
          nameMsg: true,
          nameErrMsg: 'Length of characters should be between 2-30',
        });
      } else {
        this.setState ({
          nameMsg: true,
          nameErrMsg: 'Invalid name',
        });
      }
      isValidate = false;
    }
    if (
      this.state.phnNumber.length < 7 ||
      this.state.phnNumber.length >= 16 ||
      !CONFIG.MOBILE_NUM_FORMAT.test (this.state.phnNumber)
    ) {
      if (this.state.phnNumber == '') {
        this.setState ({
          mobileMsg: true,
          mobileErrMsg: STRINGS.signUp.mobileMsg,
        });
      } else {
        this.setState ({
          mobileMsg: true,
          mobileErrMsg: 'Enter valid mobile number between 7-15 digits',
        });
      }
      isValidate = false;
    }
    if (isValidate) {
      if (this.state.isChecked === true) {
        this.signUp ();
      } else {
        this.showToast (checkMsg);
      }
    }
  };

  signUp = () => {
    this.setState ({loader: true});
    let toLowerCaseEmail = this.state.email.toLowerCase ();
    let email = toLowerCaseEmail.trim ();
    let payload = {
      name: this.state.name,
      email: email,
      mobile_number: this.state.phnNumber,
      password: this.state.password,
    };
    let profile = {};
    LOGINAPI.postSignupData (payload, CONFIG.BASE_URL + CONFIG.SIGNUP_URL)
      .then (
        function (res) {
          this.setState ({loader: false});
          console.log('hellojghjfjahjkj------------------->>>>>>>>>>>>>>>',res)
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                profile.email = this.state.email;
                profile.name = this.state.name;
                profile.mobile_number = this.state.phnNumber;
                profile.token = res.data.token;
                profile.password = this.state.password;
                this.props.updateUserDetails (profile);
                this.props.navigation.navigate ('VerifyOtp', {
                  previousRoute: 'SignUp',
                  email_verify: true,
                  mobile_verify: true,
                });
                AsyncStorage.setItem ('isVerified', JSON.stringify (true));
                AsyncStorage.setItem ('isOTPVerified', JSON.stringify (false));
                AsyncStorage.setItem ('previousStorageRoute', 'SignUp');
                AsyncStorage.setItem (
                  'emailVerifyStorage',
                  JSON.stringify (true)
                );
                AsyncStorage.setItem (
                  'mobileVerifyStorage',
                  JSON.stringify (true)
                );
                this.props.navigation.navigate ('VerifyOtp', {
                  previousRoute: 'SignUp',
                  email_verify: true,
                  mobile_verify: true,
                });
                this.setState({phnNumber:'',password:'',name:'',email:''})
                break;
              case CONFIG.statusError:
                let errors = res.errors;
                errors.map (error => {
                  if (error == 'The email has already been taken.') {
                    this.setState ({emailErrMsg: error, emailMsg: true});
                  }
                  if (error == 'The mobile number has already been taken.') {
                    this.setState ({mobileErrMsg: error, mobileMsg: true});
                  }
                  if (error == 'The password field is required.') {
                    this.setState ({passErrMsg: error, passMsg: true});
                  }
                });
                break;
            }
          } else {
            this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          this.setState ({loader: false});
          this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  navigateToTerms = () => {
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.handleBackButton
    );
    this.props.navigation.navigate ('Terms');
  };

  render () {
    return (
      <Fragment>
        <SafeAreaView style={Styles.screen}>
          <StatusBar backgroundColor="white" barStyle="dark-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
          <ScrollView>

        

            <View>
              <Image
                source={require ('../assets/drink_a_drink_logo.png')}
                style={styles.logoStyle}
              />
            </View>
            <ImageBackground
              source={require ('../assets/Mugs2.png')}
              style={styles.imgBackground}
            >
              <View style={styles.header}>
                <Text style={styles.headerTextStyle}>
                  {STRINGS.signUp.signUp}
                </Text>
              </View>
              <View style={styles.inputStyles}>
                <View style={styles.alignRowCenter}>
                  <View style={styles.inputDirectionStylesUser}>
                    <Image
                      source={require ('../assets/user.png')}
                      style={styles.iconStyle}
                    />

                    <TextInput
                      style={styles.textInput}
                      onChangeText={name => {
                        this.setState ({name: name, nameMsg: false});
                      }}
                      placeholderTextColor="black"
                      value={this.state.name}
                      placeholder={STRINGS.signUp.name}
                    />
                  </View>
                </View>
                {this.state.nameMsg &&
                  <View style={styles.alignRowCenter}>
                    <View style={styles.errorRowWidth}>
                      <Text style={Styles.error}>{this.state.nameErrMsg}</Text>
                    </View>
                  </View>}
                <View style={styles.alignRowCenter}>
                  <View style={styles.inputDirectionStylesUser}>
                    <Image
                      source={require ('../assets/phone.png')}
                      style={styles.iconStyle}
                    />

                    <TextInput
                      style={styles.textInput}
                      onChangeText={phnNumber => {
                        this.setState ({
                          phnNumber: phnNumber,
                          mobileMsg: false,
                        });
                      }}
                      keyboardType="phone-pad"
                      placeholderTextColor="black"
                      value={this.state.phnNumber}
                      returnKeyType={ 'done' }
                      placeholder={STRINGS.signUp.phnNumber}
                    />
                  </View>
                </View>
                {this.state.mobileMsg &&
                  <View style={styles.alignRowCenter}>
                    <View style={styles.errorRowWidth}>
                      <Text style={Styles.error}>
                        {this.state.mobileErrMsg}
                      </Text>
                    </View>
                  </View>}
                <View style={styles.alignRowCenter}>
                  <View style={styles.inputDirectionStylesUser}>
                    <Image
                      source={require ('../assets/mailId.png')}
                      style={styles.iconStyle}
                    />

                    <TextInput
                      style={styles.textInput}
                      onChangeText={email => {
                        this.setState ({email: email, emailMsg: false});
                      }}
                      placeholderTextColor="black"
                      value={this.state.email}
                      placeholder={STRINGS.signUp.emailId}
                    />
                  </View>
                </View>
                {this.state.emailMsg &&
                  <View style={styles.alignRowCenter}>
                    <View style={styles.errorRowWidth}>
                      <Text style={Styles.error}>{this.state.emailErrMsg}</Text>
                    </View>
                  </View>}
                <View style={styles.alignRowCenter}>
                  <View style={styles.inputDirectionStylesUser}>
                    <Image
                      source={require ('../assets/pwd.png')}
                      style={styles.iconStyle}
                    />

                    <TextInput
                      style={styles.textInput}
                      onChangeText={password => {
                        this.setState ({password: password, passMsg: false});
                      }}
                      placeholderTextColor="black"
                      value={this.state.password}
                      placeholder={STRINGS.signUp.password}
                      secureTextEntry={true}
                    />
                  </View>
                </View> 
                {this.state.passMsg &&
                  <View style={styles.alignRowCenter}>
                    <View style={styles.errorRowWidth}>
                      <Text style={Styles.error}>{this.state.passErrMsg}</Text>
                    </View>
                  </View>}
              </View>
              <View style={styles.termsRow}>
                <View style={styles.innerTermsRow}>
                  <TouchableOpacity onPress={()=>{this.setState ({
                          isChecked: !this.state.isChecked,
                        });}}>
                    <CheckBox
                      onClick={() => {
                        this.setState ({
                          isChecked: !this.state.isChecked,
                        });
                      }}
                      isChecked={this.state.isChecked}
                      checkedImage={
                        <Image
                          source={require ('../assets/checkedBox.png')}
                          style={styles.CheckboxIcon}
                        />
                      }
                      unCheckedImage={
                        <Image
                          source={require ('../assets/checkbox.png')}
                          style={styles.CheckboxIcon}
                        />
                      }
                    />
                  </TouchableOpacity>
                  <View style={styles.textRow}>
                    <View>
                      <Text style={styles.termsAndConditionText}>
                        {STRINGS.signUp.termsAndConditionText1}
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={styles.innerTermsSection}
                      onPress={this.navigateToTerms}
                    >
                      <Text style={styles.termsConditionsText}>
                        {STRINGS.signUp.termsAndConditionText2}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.innerTermsSection}
                      onPress={this.navigateToTerms}
                    >
                      <Text style={styles.termsConditionsText}>
                        {STRINGS.signUp.termsAndConditionText3}
                      </Text>
                    </TouchableOpacity>
                    <View>
                      <Text style={styles.termsAndConditionText}>
                        {STRINGS.signUp.termsAndConditionText4}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.signUpSection}>
                <View>
                  <Text style={styles.signupText}>
                    {STRINGS.signUp.alreadyHaveAnAcc}
                  </Text>
                </View>
                <TouchableOpacity
                  style={styles.innerSignupSection}
                  onPress={this.navigateToSignIn}
                >
                  <Text style={styles.innerSignupText}>
                    {'' + STRINGS.signUp.signIn}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.submitSection}>
                <TouchableOpacity
                  style={styles.submitBtn}
                  onPress={this.validate}
                >
                  <Text style={styles.submitText}>
                    {STRINGS.signUp.signUp.toUpperCase () + ' '}
                   
                  </Text>
                   <Image
                      source={require ('../assets/arrow.png')}
                      style={styles.arrowIcon}
                    />
                </TouchableOpacity>
              </View>
            </ImageBackground>
            <View style={styles.emptyView} />
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
        </SafeAreaView>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateUserDetails: profileDetails => {
      dispatch (updateUserDetails (profileDetails));
    },
  };
};
export default connect (mapStateToProps, mapDispatchToProps) (signUp);

const styles = EStyleSheet.create ({
  inputDirectionStylesUser: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderBottomWidth: 1,
    borderBottomColor: '#000',
    height: '45rem',
    marginBottom: '10rem',
    width: '70%',
  },
  iconStyle: {
    padding: '10rem',
    margin: '5rem',
    height: '30rem',
    width: '30rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  inputStyles: {
    marginTop: '30rem',
  },
  imgBackground: {
    flex: 1,
    width: null,
    height: null,
  },
  logoStyle: {
    height: '125rem',
    width: '327rem',
    margin: '30rem', 
    marginTop:'10rem',
  },
  header: { 
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '10rem',
  },
  headerTextStyle: {
    fontSize: fontSizeValue * 28,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  textInput: {
    flex: 1,
    width: '100%',
    color: '#000000',
    paddingLeft: '10rem',
    fontSize: fontSizeValue * 10.7,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    backgroundColor: 'transparent',
  },
  submitSection: {
    height: '46rem',
    marginTop: '20rem',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitBtn: {
    width: '142rem',
    height: '46rem',
    backgroundColor: '#47170d',
    borderRadius: '6rem',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding:'12rem'
  },
  signUpSection: {
    height: '25rem',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitText: {
    fontSize: fontSizeValue * 14,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
    // paddingBottom: '5rem',
   
  },
  signupText: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  innerSignupText: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  innerSignupSection: {
    marginLeft: '5rem',
    borderBottomColor: '#47170d',
    borderBottomWidth: 0.8,
  },
  arrowIcon: {
    width: '25rem',
    height: '25rem',
   
  },
  termsAndConditionText: {
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    fontSize: fontSize() * 8,
    color: '#000000',
  },
  textRow: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingLeft: '10rem',
  },
  termsRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: '70rem',
  },
  innerTermsRow: {
    flexDirection: 'row',
    flex: 0.7,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '15rem',
  },
  CheckboxIcon: {
    height: '15.95rem',
    width: '15.95rem',
  },
  emptyView: {
    height: '30rem',
  },
  alignRowCenter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  errorRowWidth: {
    width: '70%' 
  }, 
  termsConditionsText: {
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    fontSize: fontSize() * 9,
    color: '#80001a'
  },
  innerTermsSection: {
    marginLeft: '5rem',
    borderBottomColor: '#80001a',
    borderBottomWidth: 0.8,
  },
});
