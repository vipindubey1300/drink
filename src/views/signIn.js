import React, {Component, Fragment} from 'react';
import {
  ImageBackground,
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  Image,
  ScrollView,
  ActivityIndicator,
  Platform,
  BackHandler,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import firebase from '@react-native-firebase/app';
import messaging from '@react-native-firebase/messaging';
import {connect} from 'react-redux';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import CONFIG from '../config/config';
import AsyncStorage from '@react-native-community/async-storage';
import Toast, {DURATION} from 'react-native-easy-toast';
import * as LOGINAPI from '../services/api-services/login-api';
import {updateUserDetails} from '../services/redux/actions/userActions';
import NavigationBar from 'react-native-navbar-color'

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class signIn extends Component {
  constructor (props) {
    super (props);
    this.state = {
      email: '',
      password: '',
      passMsg: false,
      emailMsg: false,
      loader: false,
      emailErrMsg: STRINGS.signIn.emailMsg,
      passErrMsg: STRINGS.signUp.passMsg,
      device_token: '',
      count: 0,
    };
  }

  componentDidMount () {
    BackHandler.addEventListener ('hardwareBackPress', this.handleBackButton);
    NavigationBar.setStatusBarColor('white',false)
    NavigationBar.setColor('white');
    this.onLoad ();
    this.getData ();
  }

  componentWillUnmount () {
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.handleBackButton
    );
  }

  handleBackButton = () => {
    let count = this.state.count;
    if (count == 1) {
      BackHandler.exitApp ();
      return true;
    } else {
      this.showToast ('Press back again to leave');
      this.setState ({count: 1});
      return true;
    }
  };

  getData = () => {
    var profile = this.props.userReducer.profile;
    if (profile) {
      this.setState ({
        email: profile.email,
        password: profile.password,
      });
    }
    this.setState ({passMsg: false, emailMsg: false});
    messaging ()
      .getToken ()
      .then (device_token => {
        this.setState ({device_token: device_token});
      })
      .catch (
        function (err) {
          this.setState ({loader: false});
         // this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  componentWillUnmount () {
    // Remove the event listener
    this.didFocusListener.remove();
  }

  onLoad = () => {
    this.didFocusListener = this.props.navigation.addListener ('didFocus', () => this.getData ());
  };

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  navigateToSignUp = async () => {
    await AsyncStorage.setItem ('isVerified', JSON.stringify (false));
    let isVerified = await AsyncStorage.getItem ('isVerified');
    let value = isVerified;
    this.props.navigation.navigate ('AgeVerification');
  };

  navigateToForgotPwd = () => {
    this.props.navigation.navigate ('ForgotPassword');
  };

  navigateToHome = () => {
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.handleBackButton
    );
    this.props.navigation.navigate ('Home');
  };

  validate = () => {
    let isValidate = true;

    if (!CONFIG.PASSWORD_POLICY.test (this.state.password)) {
      this.setState ({
        passMsg: true,
      });
      isValidate = false;
    }
    if (this.state.email == null) {
      this.setState ({
        emailMsg: true,
        emailErrMsg: 'Email id field is required',
      });
      isValidate = false;
    } else {
      if (this.state.email !== null) {
        var toLowerCaseEmail = this.state.email.toLowerCase ();
        var email = toLowerCaseEmail.trim ();
      }
      if (!CONFIG.SIGNIN_FORMAT.test(email)) {
        this.setState ({
          emailMsg: true,
          emailErrMsg: 'Please enter valid email Id or phone number',
        });
        isValidate = false;
      }
      if (email.length > 64) {
        this.setState ({
          emailMsg: true,
          emailErrMsg: 'Maximum character length exceeded',
        });
        isValidate = false;
      }
    }
    if (isValidate) {
      this.signIn ();
    }
  };

  signIn = () => {
    this.setState ({loader: true});
    var appClientOs = Platform.OS;
    if (appClientOs == 'android') {
      var device_type = 1;
    }
    if (appClientOs == 'ios') {
      var device_type = 2;
    }
    let toLowerCaseEmail = this.state.email.toLowerCase ();
    let email = toLowerCaseEmail.trim ();
    let mobile_number = null;
    if (CONFIG.EMAIL_FORMAT.test (email)) {
      email = this.state.email;
      mobile_number = null;
    } 
    if (CONFIG.MOBILE_NUM_FORMAT.test (email)) {
      email = null;
      mobile_number = this.state.email;
    }
    let payload = {
      email: email,
      mobile_number: mobile_number,
      password: this.state.password,
      device_token: this.state.device_token,
      device_type: device_type,
    };
    let profile = {};
    LOGINAPI.postSignupData (payload, CONFIG.BASE_URL + CONFIG.SIGNIN_URL)
      .then (
        function (res) {
          this.setState ({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                AsyncStorage.setItem ('isOTPVerified', JSON.stringify (true));
                AsyncStorage.setItem (
                  'isProfileOTPVerified',
                  JSON.stringify (true)
                );
                profile.email = res.data.email;
                profile.name = res.data.name;
                profile.mobile_number = res.data.mobile_number;
                profile.token = res.data.token;
                profile.password = this.state.password;
                profile.isLoggedIn = true;
                this.props.updateUserDetails (profile);
                this.navigateToHome();
                break;
              case CONFIG.statusUnauthorized:
                this.showToast ('Invalid User ID and Password');
                break;
              case CONFIG.statusError:
                let errors = res.errors;
                errors.map (error => {
                  if (error == 'The email field is required.') {
                    this.setState ({emailErrMsg: 'User ID field is required', emailMsg: true});
                  }
                  if (error == 'The password field is required.') {
                    this.setState ({passErrMsg: 'Password field is required', passMsg: true});
                  }
                });
                break;
            }
          } else {
            //this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          this.setState ({loader: false});
         // this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  render () {
    return (
      <Fragment>
        <SafeAreaView style={Styles.screen}>
          <StatusBar backgroundColor="white" barStyle="dark-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
          <ScrollView>
            <View>
              <Image
                source={require ('../assets/drink_a_drink_logo.png')}
                style={styles.logoStyle}
              />
            </View>
            <ImageBackground
              source={require ('../assets/Mugs2.png')}
              style={styles.imgBackground}
            >
              <View style={styles.header}>
                <Text style={styles.headerTextStyle}>
                  {STRINGS.signIn.signIn}
                </Text>
              </View>
              <View style={styles.inputStyles}>
                <View style={styles.alignRowCenter}>
                  <View style={styles.inputDirectionStylesUser}>
                    <Image
                      source={require ('../assets/user.png')}
                      style={styles.iconStyle}
                    />

                    <TextInput
                      style={styles.textInput}
                      onChangeText={email => {
                        this.setState ({email: email, emailMsg: false});
                      }}
                      placeholderTextColor="black"
                      value={this.state.email}
                      placeholder={'Email ID '}
                    />
                  </View>
                </View>
                {this.state.emailMsg &&
                  <View style={styles.alignRowCenter}>
                    <View style={styles.errorRowWidth}>
                      <Text style={Styles.error}>{this.state.emailErrMsg}</Text>
                    </View>
                  </View>}
                <View style={styles.alignRowCenter}>
                  <View style={styles.inputDirectionStylesPwd}>
                    <Image
                      source={require ('../assets/pwd.png')}
                      style={styles.iconStyle}
                    />

                    <TextInput
                      style={styles.textInput}
                      onChangeText={password => {
                        this.setState ({password: password, passMsg: false});
                      }}
                      placeholderTextColor="black"
                      value={this.state.password}
                      placeholder={STRINGS.signIn.password}
                      secureTextEntry={true}
                    />
                  </View>
                </View>
                {this.state.passMsg &&
                  <View style={styles.alignRowCenter}>
                    <View style={styles.errorRowWidth}>
                      <Text style={Styles.error}>{this.state.passErrMsg}</Text>
                    </View>
                  </View>}
                <View style={styles.alignForgotRowCenter}>
                  <TouchableOpacity
                    style={styles.forgotPasswordSection}
                    onPress={this.navigateToForgotPwd}
                  >
                    <Text style={styles.forgotPassword}>
                      {STRINGS.signIn.forgotPassword}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.signUpSection}>
                <View>
                  <Text style={styles.signupText}>
                    {STRINGS.signIn.dontHaveAnAcc}
                  </Text>
                </View>
                <TouchableOpacity
                  style={styles.innerSignupSection}
                  onPress={this.navigateToSignUp}
                >
                  <Text style={styles.innerSignupText}>
                    {'' + STRINGS.signIn.signUp}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.submitSection}>
                <TouchableOpacity
                  style={styles.submitBtn}
                  onPress={this.validate}
                >
                  <Text style={styles.submitText}>
                    {STRINGS.signIn.signIn.toUpperCase () + ' '}
                    
                  </Text>
                      <Image
                      source={require ('../assets/arrow.png')}
                      style={styles.arrowIcon}
                    />

                </TouchableOpacity>
              </View>
            </ImageBackground>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
        </SafeAreaView>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateUserDetails: profileDetails => {
      dispatch (updateUserDetails (profileDetails));
    },
  };
};
export default connect (mapStateToProps, mapDispatchToProps) (signIn);

// export default signIn;

const styles = EStyleSheet.create ({
  inputDirectionStylesUser: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderBottomWidth: 1,
    borderBottomColor: '#000',
    height: '45rem',
    marginBottom: '10rem',
    width: '70%',
  },
  inputDirectionStylesPwd: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderBottomWidth: 1,
    borderBottomColor: '#000',
    height: '45rem',
    marginBottom: '10rem',
    width: '70%',
  },
  iconStyle: {
    padding: '10rem',
    margin: '5rem',
    height: '30rem',
    width: '30rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  inputStyles: {
    marginTop: '50rem',
  },
  alignRowCenter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgBackground: {
    flex: 1,
    width: null,
    height: null,
  },
  logoStyle: {
    height: '125rem',
    width: '327rem',
    margin: '30rem',
  },
  header: {
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '30rem',
  },
  headerTextStyle: {
    fontSize: fontSizeValue * 28,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  textInput: {
    flex: 1,
    width: '100%',
    color: '#000000',
    paddingLeft: '10rem',
    fontSize: fontSizeValue * 10.7,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  forgotPasswordSection: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    width: '82%',
  },
  forgotPassword: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  alignForgotRowCenter: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  submitSection: {
    height: '46rem',
    marginTop: '20rem',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitBtn: {
    width: '142rem',
    height: '46rem',
    backgroundColor: '#47170d',
    borderRadius: '6rem',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding:'12rem'
  },
  signUpSection: {
    marginTop: '73rem',
    marginLeft: '25rem',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  submitText: {
    fontSize: fontSizeValue * 14,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
  },
  signupText: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  innerSignupText: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
    textDecorationLine: 'underline',
  },
  innerSignupSection: {
    marginLeft: '5rem',
  },
  arrowIcon: {
    width: '25rem',
    height: '25rem',
  },
  errorRowWidth: {width: '70%'},
});
