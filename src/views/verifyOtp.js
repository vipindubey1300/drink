import React, {Component, Fragment} from 'react';
import {
  ImageBackground,
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  Image,
  ScrollView,
  ActivityIndicator,
  BackHandler,
  Platform,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import STRINGS from '../utils/strings';
import Toast, {DURATION} from 'react-native-easy-toast';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import * as LOGINAPI from '../services/api-services/login-api';
import CONFIG from '../config/config';
import {updateUserDetails} from '../services/redux/actions/userActions';
import NavigationBar from 'react-native-navbar-color'

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class verifyOtp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailOtp: '',
      mobileOtp: '',
      loader: false,
      previousRoute: '',
      email: '',
      name: '',
      token: '',
      password: '',
      avator: '',
      mobile_number: '',
      isOTPVerified: false,
      mobile_verify: false,
      email_verify: false,
      isProfileOTPVerified: false,
      newEmail: '',
      newNumber: '',
      count: 0
    };
  }

  componentDidMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    NavigationBar.setStatusBarColor('white',false)
    NavigationBar.setColor('white');
    const {navigation} = this.props;
    let previousStorageRoute = await AsyncStorage.getItem(
      'previousStorageRoute',
    );
    let emailVerifyStorage = await AsyncStorage.getItem('emailVerifyStorage');
    let mobileVerifyStorage = await AsyncStorage.getItem('mobileVerifyStorage');
    let isProfileOTPVerified = await AsyncStorage.getItem(
      'isProfileOTPVerified',
    );
    let previousRoute = navigation.getParam('previousRoute', null);
    let email_verify = navigation.getParam('email_verify', null);
    let mobile_verify = navigation.getParam('mobile_verify', null);
    let Email = navigation.getParam('email', null);
    let Number = navigation.getParam('phnNumber', null);
    this.setState({
      newEmail: Email,
      newNumber: Number
    })
    if (mobileVerifyStorage == 'true') {
      var mobileVerify = true;
    } else {
      var mobileVerify = false;
    }
    if (emailVerifyStorage == 'true') {
      var emailVerify = true;
    } else {
      var emailVerify = false;
    }
    if (previousRoute !== null) {
      this.setState({
        previousRoute: previousRoute,
        mobile_verify: mobile_verify,
        email_verify: email_verify,
      });
    } else {
      this.setState({
        previousRoute: previousStorageRoute,
        mobile_verify: mobileVerify,
        email_verify: emailVerify,
      });
    }
    setTimeout(() => {
      this.setState({isOTPVerified: true});
    }, 5000);

    let profile = this.props.userReducer.profile;
    if (profile) {
      this.setState({
        token: profile.token,
        name: profile.name,
        password: profile.password,
        email: profile.email,
        avator: profile.avator,
        mobile_number: profile.mobile_number,
      });
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    let count = this.state.count;
    if (count == 1) {
      BackHandler.exitApp ();
      return true;
    } else {
      this.showToast ('Press back again to leave');
      this.setState ({count: 1});
      return true;
    }
  };

  showToast = (toastMsg) => {
    this.toast.show(toastMsg);
  };

  callVerifyOtp = () => {
    let previousRoute = this.state.previousRoute;
    switch (previousRoute) {
      case 'SignUp':
        if (
          this.state.email_verify == true &&
          this.state.mobile_verify == true &&
          this.state.emailOtp !== '' &&
          this.state.mobileOtp !== ''
        ) {
          this.verifyOTP();
        } else {
          this.showToast('OTP cannot be empty');
        }
        break;
      case 'Profile':
        if (
          this.state.mobile_verify == true &&
          this.state.email_verify == false &&
          this.state.mobileOtp !== ''
        ) {
          this.verifyOTP();
        } else {
          if (this.state.mobileOtp == '') {
            this.showToast('OTP cannot be empty');
          }
        }

        if (
          this.state.email_verify == true &&
          this.state.mobile_verify == false &&
          this.state.emailOtp !== ''
        ) {
          this.verifyOTP();
        } else {
          if (this.state.emailOtp == '') {
            this.showToast('OTP cannot be empty');
          }
        }

        if (
          this.state.email_verify == true &&
          this.state.mobile_verify == true &&
          this.state.emailOtp !== '' &&
          this.state.mobileOtp !== ''
        ) {
          this.verifyOTP();
        } else {
          this.showToast('OTP cannot be empty');
        }
        break;
    }
  };

  verifyOTP = () => {
    this.setState({loader: true});
    let emailOtp = this.state.emailOtp;
    let mobileOtp = this.state.mobileOtp;
    var is_edit_profile = false;
    if (this.state.previousRoute == 'Profile') {
      is_edit_profile = true;
    } else {
      is_edit_profile = false;
    }
    let payload = {
      email_otp: emailOtp,
      mobile_otp: mobileOtp,
      is_edit_profile: is_edit_profile,
    };
    let profile = {};
    LOGINAPI.postSignupData(
      payload,
      CONFIG.BASE_URL + CONFIG.VERIFY_ACCOUNT_OTP_URL,
    )
      .then(
        function (res) {    
          this.setState({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                AsyncStorage.setItem('isOTPVerified', JSON.stringify(true));
                let msg = res.message;
                this.showToast(msg);
                if (this.state.previousRoute == 'SignUp') {
                  setTimeout(() => {
                    this.props.navigation.navigate('SignIn');
                  }, 200);
                }
                if (this.state.previousRoute == 'Profile') {
                  profile.email = this.state.newEmail;
                  profile.name = this.state.name;
                  profile.mobile_number = this.state.newNumber;
                  profile.token = this.state.token;
                  profile.password = this.state.password;
                  profile.avator = this.state.avator;
                  profile.isLoggedIn = true;
                  this.props.updateUserDetails(profile);
                  AsyncStorage.setItem(
                    'isProfileOTPVerified',
                    JSON.stringify(true),
                  );
                  AsyncStorage.setItem(
                    'isProfileLoggedIn',
                    JSON.stringify(true),
                  );
                  setTimeout(() => {
                    this.props.navigation.navigate('Home');
                  }, 200);
                }
                break;
              case CONFIG.statusError:
                let error = res.message;
                this.setState({
                  emailOtp: '',
                  mobileOtp: '',
                });
                this.showToast(error);
            }
          } else {
            this.setState({emailOtp: '', mobileOtp: ''});
            this.showToast('Something went wrong. Please try again!');
          }
        }.bind(this),
      )
      .catch(
        function (err) {
          this.setState({loader: false});
          this.setState({emailOtp: '', mobileOtp: ''});

          this.showToast('Something went wrong. Please try again!');
        }.bind(this),
      );
  };

  resendOTP = () => {
    this.setState({loader: true});
    var is_edit_profile = false;
    if (this.state.previousRoute == 'Profile') {
      var payload = {
        email: this.state.newEmail,
        mobile_number: this.state.newNumber,
        is_edit_profile: true,
      };
    } else {
      var payload = {
        email: this.state.email,
        mobile_number: this.state.mobile_number,
        is_edit_profile: false,
      };
    }
    LOGINAPI.postSignupData(payload, CONFIG.BASE_URL + CONFIG.RESEND_URL)
      .then(
        function (res) {
          this.setState({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                this.showToast('OTP has been sent successfully');
                this.setState({emailOtp: '', mobileOtp: ''});
                break;
              case CONFIG.statusUnauthorized:
                this.showToast('User does not exists with these details');
                break;
              case CONFIG.statusError:
                this.showToast('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast('Something went wrong. Please try again!');
          }
        }.bind(this),
      )
      .catch(
        function (err) {
          this.setState({loader: false});
          this.showToast('Something went wrong. Please try again!');
        }.bind(this),
      );
  };

  render() {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}

        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="white" barStyle="dark-content" />
          {this.state.loader && (
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>
          )}
          <ScrollView>
            <View>
              <Image
                source={require('../assets/drink_a_drink_logo.png')}
                style={styles.logoStyle}
              />
            </View>
            <ImageBackground
              source={require('../assets/Mugs2.png')}
              style={styles.imgBackground}>
              <View style={styles.header}>
                <Text style={styles.headerTextStyle}>
                  {STRINGS.otp.verifyOtp}
                </Text>
              </View>
              <View style={styles.otpTextStyle}>
                <Text style={styles.otpText}>{STRINGS.otp.verifyOtpText}</Text>
              </View>
              {this.state.mobile_verify && (
                <View>
                  <View style={styles.enterOTPHereStyle}>
                    <Text style={styles.enterOTPHere}>
                      {STRINGS.otp.enterMobileOTPHere}
                    </Text>
                  </View>
                  <View style={styles.otpView}>
                    <View style={styles.otpViewWidth}>
                      <OTPInputView
                        style={styles.otpInput}
                        pinCount={6}
                        code={this.state.mobileOtp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                        onCodeChanged={(mobileOtp) => {
                          this.setState({mobileOtp});
                        }}
                        codeInputFieldStyle={styles.underlineStyleBase}
                        codeInputHighlightStyle={
                          styles.underlineStyleHighLighted
                        }
                      />
                    </View>
                  </View>
                </View>
              )}
              {this.state.email_verify && (
                <View>
                  <View style={styles.enterOTPHereStyle}>
                    <Text style={styles.enterOTPHere}>
                      {STRINGS.otp.enterEmailOTPHere}
                    </Text>
                  </View>
                  <View style={styles.otpView}>
                    <View style={styles.otpViewWidth}>
                      <OTPInputView
                        style={styles.otpInput}
                        pinCount={6}
                        code={this.state.emailOtp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                        onCodeChanged={(emailOtp) => {
                          this.setState({emailOtp});
                        }}
                        codeInputFieldStyle={styles.underlineStyleBase}
                        codeInputHighlightStyle={
                          styles.underlineStyleHighLighted
                        }
                      />
                    </View>
                  </View>
                </View>
              )}
              <View>
                {this.state.isOTPVerified && (
                  <TouchableOpacity
                    style={styles.resendSection}
                    onPress={this.resendOTP}>
                    <Text style={styles.resendBtnTxt}>
                      {STRINGS.forgotPassword.resendOtp}
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
              <View style={styles.submitSection}>
                <TouchableOpacity
                  style={styles.submitBtn}
                  onPress={this.callVerifyOtp}>
                  <Text style={styles.submitText}>
                    {STRINGS.forgotPassword.submit + ' '}
                   
                  </Text>
                  <Image
                      source={require('../assets/arrow.png')}
                      style={styles.arrowIcon}></Image>
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={(ref) => {
              this.toast = ref;
            }}
          />
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const {userReducer} = state;
  return {userReducer};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUserDetails: (profileDetails) => {
      dispatch(updateUserDetails(profileDetails));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(verifyOtp);

const styles = EStyleSheet.create({
  imgBackground: {
    flex: 1,
    width: null,
    height: null,
  },
  logoStyle: {
    height: '125rem',
    width: '327rem',
    margin: '30rem',
  },
  header: {
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '30rem',
  },
  headerTextStyle: {
    fontSize: fontSizeValue * 23,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  otpTextStyle: {
    width: '75%',
    alignItems: 'center',
    marginTop: '40rem',
    marginLeft: '45rem',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  otpText: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#000000',
    textAlign: 'center',
  },
  enterOTPHereStyle: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    marginTop: '20rem',
  },
  enterOTPHere: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Bold' : 'Calisto MT',
    fontWeight:'bold',
    color: '#000000',
  },
  otpView: {
    flexDirection: 'row',
    marginTop: '10rem',
    justifyContent: 'center',
  },
  otpViewWidth: {
    width: '85%',
  },
  underlineStyleBase: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '42rem',
    height: '42rem',
    borderWidth: 2,
    borderColor: 'black',
    borderRadius: '6rem',
    color: 'black',
    fontSize: fontSizeValue * 12,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  otpInput: {
    width: '100%',
    height: '100rem',
  },
  submitSection: {
    height: '46rem',
    marginTop: '30rem',
    marginBottom: '30rem',
    justifyContent: 'center',
    alignItems: 'center',
  },
  resendSection: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitBtn: {
    width: '142rem',
    height: '46rem',
    backgroundColor: '#47170d',
    borderRadius: '6rem',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding:'12rem',
  },
  resendBtnTxt: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
    textDecorationLine: 'underline',
  },
  submitText: {
    fontSize: fontSizeValue * 14,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
  },
  arrowIcon: {
    width: '25rem',
    height: '25rem',
  },
});
