import React, {Component, Fragment} from 'react';
import {
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  FlatList,
  ActivityIndicator,Platform,
  BackHandler
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import {connect} from 'react-redux';
import Toast, {DURATION} from 'react-native-easy-toast';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import Styles from '../styles/styles';
import Header from '../components/shared/header';
import Footer from '../components/shared/footer';
import NotificationCard from '../components/shared/notificationCard';
import * as HOMEAPI from '../services/api-services/home-api';
import CONFIG from '../config/config';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();
class lastOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: STRINGS.lastOrder.lastOrderTitle,
      backButton: true,
      bellIcon: false,
      showEmptyText: false,
      loader: false,
      orderdData: [],
    };
  }

  showNotification = ({item}) => {
    return (
      <NotificationCard
        notificationText={item.order_title}
        screen={'lastOrder'}></NotificationCard>
    );
  };

  showToast = (toastMsg) => {
    this.toast.show(toastMsg);
  };

  renderFooter = () => {
    return <View style={styles.listFooterStyle}></View>;
  };

  renderHeader = () => {
    return <View style={styles.listHeaderStyle}></View>;
  };

  componentDidMount() {
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('#f2f2f2');
    this.setState({loader: true});
    if (this.state.orderdData.length == 0) {
      this.setState({showEmptyText: true});
    }
    var profile = this.props.userReducer.profile;
    if (profile) {
      var token = profile.token;
    }
    HOMEAPI.getRequestData(token, CONFIG.BASE_URL + CONFIG.LAST_ORDER_URL)
      .then(
        function (res) {
          this.setState({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                let dataArray = res.data
                this.setState({orderdData: dataArray,showEmptyText: false});
                break;
              case CONFIG.statusUnauthorized:
                this.showToast('Your account has been deactivated. Please logout and login with valid user credentials');
                this.props.navigation.navigate('Home');
                break;
              case CONFIG.statusError:
                this.showToast('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast('Something went wrong. Please try again!');
          }
        }.bind(this),
      )
      .catch(
        function (err) {
          this.setState({loader: false});
          this.showToast('Something went wrong. Please try again!');
        }.bind(this),
      );
  }

  render() {
    return (
      <Fragment>
        {/* <SafeAreaView style={Styles.screen}> */}
        <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
          <StatusBar backgroundColor="#47170d" barStyle="light-content" />
          {this.state.loader && (
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>
          )}
          <Header
            title={this.state.title}
            backButton={this.state.backButton}
            navigation={this.props.navigation}
            hideBellIcon={this.state.bellIcon}
            notifCount={this.state.notifCount}></Header>
          <View style={styles.background}>
            {!this.state.showEmptyText && (
              <FlatList
                showsVerticalScrollIndicator={false}
                data={this.state.orderdData}
                renderItem={this.showNotification}
                keyExtractor={(item) => item.id.toString()}
                ListFooterComponent={this.renderFooter}
                ListHeaderComponent={this.renderHeader}
              />
            )}
            {this.state.showEmptyText && (
              <View style={styles.emptyBackground}>
                <Text style={styles.emptyCartStyle}>
                  {'You have no orders yet'}
                </Text>
              </View>
            )}
            <Toast
              position="bottom"
              style={Styles.toastStyle}
              textStyle={{color: 'white'}}
              ref={(ref) => {
                this.toast = ref;
              }}
            />
          </View>

          <View style={Styles.footerOverlay}>
            <Footer navigation={this.props.navigation}></Footer>
          </View>
          </View>
        {/* </SafeAreaView> */}
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const {userReducer} = state;
  return {userReducer};
};

export default connect(mapStateToProps)(lastOrder);

const styles = EStyleSheet.create({
  background: {
    backgroundColor: '#d2d2d6',
    flex: 1,
  },
  listFooterStyle: {
    marginBottom: '30rem',
  },
  listHeaderStyle: {
    marginTop: '20rem',
  },
  emptyCartStyle: {
    fontSize: fontSizeValue * 15,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  emptyBackground: {
    backgroundColor: '#d2d2d6',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
