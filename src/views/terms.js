import React, {Component, Fragment} from 'react';
import {
  SafeAreaView,
  StatusBar,
  View,
  ScrollView,
  ActivityIndicator,
  Dimensions,
  Text,
  BackHandler
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import NavigationBar from 'react-native-navbar-color';
import Toast, {DURATION} from 'react-native-easy-toast';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import Styles from '../styles/styles';
import HTML from 'react-native-render-html';
import CONFIG from '../config/config';
import * as HOMEAPI from '../services/api-services/home-api';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class terms extends Component {
  constructor(props) {
    super(props);
    this.state = {
        loader: false,
        termsandconditionContent: ''
    };
  }

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  handleBackButton = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    this.props.navigation.navigate('SignUp')
    return true;
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    NavigationBar.setStatusBarColor('#47170d',false)
    NavigationBar.setColor('white');
    this.setState({loader: true});
    HOMEAPI.getRequestPageData(CONFIG.BASE_URL + CONFIG.GET_PAGE_URL)
      .then(
        function (res) {
          this.setState({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                let termsandconditionContent = res.data[1].page_content;
                this.setState({
                  termsandconditionContent: termsandconditionContent,
                });
                break;
              case CONFIG.statusUnauthorized:
                this.showToast('Account does not exists');
                break;
              case CONFIG.statusError:
                this.showToast('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast('Something went wrong. Please try again!');
          }
        }.bind(this),
      )
      .catch(
        function (err) {
          this.setState({loader: false});
          this.showToast('Something went wrong. Please try again!');
        }.bind(this),
      );
  }

  render() {
    return (
        <Fragment>
          {/* <SafeAreaView style={Styles.screen}> */}
          <View style={[Styles.screen]}>
         <SafeAreaView style={[{backgroundColor:'#47170d'}]} />
            <StatusBar backgroundColor="#47170d" barStyle="light-content" />
            {this.state.loader && (
              <View style={Styles.loading}>
                <ActivityIndicator size="large" color="#47170d" />
              </View>
            )}
            <View style={styles.headerBody}>
                <Text style={styles.headerTitle}>
                {STRINGS.account.termsAndConditions}
                </Text>
            </View>
            <ScrollView style={styles.background}>
              <View style={styles.formBackground}>
                {!this.state.termsandconditionContent == '' && (
                  <HTML html={this.state.termsandconditionContent} />
                )}
              </View>
            </ScrollView>
            <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
          </View>
          {/* </SafeAreaView> */}
        </Fragment>
      );
  }
}

export default terms;

const styles = EStyleSheet.create({
    background: {
      backgroundColor: '#d2d2d6',
    },
    formBackground: {
      backgroundColor: 'white',
      marginTop: '30rem',
      marginLeft: '10rem',
      marginRight: '10rem',
      paddingRight: '10rem',
      paddingLeft: '10rem',
      marginBottom: '30rem',
    },
    termsTextStyle: {
      fontSize: fontSizeValue * 12,
      fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
      color: 'black',
    },
    headerBody: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: '55rem',
        backgroundColor: '#47170d',
      },
      headerTitle: {
        fontSize: fontSizeValue * 18,
        fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
        color: 'white',
      },
  });