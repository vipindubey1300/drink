import React, {Component, Fragment} from 'react';
import {
  ImageBackground,
  Dimensions,
  SafeAreaView,
  StatusBar,
  View,
  Text,
  TextInput,
  Image,
  ScrollView,
  ActivityIndicator,
  Platform,
  BackHandler
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import STRINGS from '../utils/strings';
import {fontSize} from '../components/global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Styles from '../styles/styles';
import Toast, {DURATION} from 'react-native-easy-toast';
import CONFIG from '../config/config';
import * as LOGINAPI from '../services/api-services/login-api';
import NavigationBar from 'react-native-navbar-color'

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class forgotPassword extends Component {
  constructor (props) {
    super (props);
    this.state = {
      emailOrNumber: '',
      emailMsg: false,
      loader: false,
      count: 0
    };
  }

  navigateToOTP = () => {
    this.props.navigation.navigate ('Otp',{value: this.state.emailOrNumber});
  };

  componentDidMount () {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    NavigationBar.setStatusBarColor('white',false)
    NavigationBar.setColor('white');
    this.setState ({emailMsg: false});
  }

  validate = () => {
    let isValidate = true;
    let emailMsg = 'Incorrect email Id or number';
    let emailOrNumber = this.state.emailOrNumber;
    if (
      this.state.emailOrNumber == '' &&
      !CONFIG.EMAIL_FORMAT.test (this.state.emailOrNumber)
    ) {
      this.setState ({emailMsg: true});
      isValidate = false;
    } else {
      this.setState ({
        emailOrNumber: emailOrNumber,
      });
    }
    if (isValidate) {
      this.sendOtp ();
    }
  };

  componentWillUnmount () {
    // Remove the event listener
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton = () => {
    let count = this.state.count;
    if (count == 1) {
      BackHandler.exitApp ();
      return true;
    } else {
      this.showToast ('Press back again to leave');
      this.setState ({count: 1});
      return true;
    }
  };

  showToast = toastMsg => {
    this.toast.show (toastMsg);
  };

  sendOtp = () => {
    this.setState ({loader: true});
    let emailOrNumber = this.state.emailOrNumber;
    let payload = {
      mobile_number: emailOrNumber,
    };
    LOGINAPI.postSignupData (payload, CONFIG.BASE_URL + CONFIG.FORGOT_PASS_URL)
      .then (
        function (res) {
          this.setState ({loader: false});
          if (res != undefined) {
            let code = res.code;
            console.warn('res in otp forgot : ',res);
            switch (code) {
              case CONFIG.statusOk:
                this.showToast ('OTP has been sent successfully');
                this.navigateToOTP();
                break;
              case CONFIG.statusUnauthorized:
                this.showToast ('User does not exists with these details');
                break;
              case CONFIG.statusError:
                this.showToast ('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast ('Something went wrong. Please try again!');
          }
        }.bind (this)
      )
      .catch (
        function (err) {
          console.warn('err : ',err);
          this.setState ({loader: false});
          this.showToast ('Something went wrong. Please try again!');
        }.bind (this)
      );
  };

  render () {
    return (
      <Fragment>
        <SafeAreaView style={Styles.screen}>
          <StatusBar backgroundColor="white" barStyle="light-content" />
          {this.state.loader &&
            <View style={Styles.loading}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>}
          <ScrollView>
            <View>
              <Image
                source={require ('../assets/drink_a_drink_logo.png')}
                style={styles.logoStyle}
              />
            </View>
            <ImageBackground
              source={require ('../assets/Mugs2.png')}
              style={styles.imgBackground}
            >
              <View style={styles.header}>
                <Text style={styles.headerTextStyle}>
                  {STRINGS.forgotPassword.forgotPassword}
                </Text>
              </View>
              <View style={styles.inputStyles}>
                <View style={styles.alignRowCenter}>
                  <View style={styles.inputDirectionStyles}>
                    <Image
                      source={require ('../assets/mailId.png')}
                      style={styles.iconStyle}
                    />
                    <TextInput
                      style={styles.textInput}
                      onChangeText={emailOrNumber => {
                        this.setState ({
                          emailOrNumber: emailOrNumber,
                          emailMsg: false,
                        });
                      }}
                      placeholderTextColor="black"
                      value={this.state.emailOrNumber}
                      placeholder={STRINGS.forgotPassword.emailOrNumber}
                      // onSubmitEditing = {this.validate}
                    />
                  </View>
                </View>
                {this.state.emailMsg &&
                  <View style={styles.alignRowCenter}>
                    <View style={styles.errorRowWidth}>
                      <Text style={Styles.error}>
                        {STRINGS.forgotPassword.emailMsg}
                      </Text>
                    </View>
                  </View>}
              </View>
              <View style={styles.submitSection}>
                <TouchableOpacity
                  style={styles.submitBtn}
                  onPress={this.validate}
                >
                  <Text style={styles.submitText}>
                    {STRINGS.forgotPassword.submit + ' '}
                    <Image
                      source={require ('../assets/arrow.png')}
                      style={styles.arrowIcon}
                    />
                  </Text>
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </ScrollView>
          <Toast
            position="bottom"
            style={Styles.toastStyle}
            textStyle={{color: 'white'}}
            ref={ref => {
              this.toast = ref;
            }}
          />
        </SafeAreaView>
      </Fragment>
    );
  }
}

export default forgotPassword;

const styles = EStyleSheet.create ({
  inputDirectionStyles: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderBottomWidth: 1,
    borderBottomColor: '#000',
    height: '45rem',
    width: '70%',
    marginBottom: '10rem',
  },
  iconStyle: {
    padding: '10rem',
    margin: '5rem',
    height: '30rem',
    width: '30rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  inputStyles: {
    marginTop: '90rem',
  },
  imgBackground: {
    flex: 1,
    width: null,
    height: null,
  },
  logoStyle: {
    height: '125rem',
    width: '327rem',
    margin: '30rem',
  },
  header: {
    position: 'relative',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '30rem',
  },
  headerTextStyle: {
    fontSize: fontSizeValue * 23,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
  },
  textInput: {
    flex: 1,
    width: '100%',
    color: '#000000',
    paddingLeft: '10rem',
    fontSize: fontSizeValue * 10.7,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
  },
  submitSection: {
    height: '46rem',
    marginTop: '80rem',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitBtn: {
    width: '142rem',
    height: '46rem',
    backgroundColor: '#47170d',
    borderRadius: '6rem',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  submitText: {
    fontSize: fontSizeValue * 14,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
    paddingBottom: '5rem',
  },
  arrowIcon: {
    width: '25rem',
    height: '25rem',
  },
  alignRowCenter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  errorRowWidth: {width: '70%'},
});
