import axios from 'axios';

export const postSignupData = (payload,url) => {
    return axios(url, {
      method: 'post',
      headers: {
        'content-type': 'application/json',
      },
      data: payload,
    })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error.response;
      });
  };

  