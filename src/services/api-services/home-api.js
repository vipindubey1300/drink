import axios from 'axios';

export const getRequestData = (token, url) => {
  return axios(url, {
    method: 'get',
    headers: {
      'Accept': 'application/json',
      Authorization: 'Bearer ' + token,
    },
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return error.response;
    });
};

export const getRequestPageData = (url) => {
  return axios(url, {
    method: 'get',
    headers: {
      'Accept': 'application/json',
    },
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return error.response;
    });
};

export const postRequestData = (payload, token, url) => {
  return axios(url, {
    method: 'post',
    headers: {
      'Accept': 'application/json',
      Authorization: 'Bearer ' + token,
    },
    data: payload,
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return error.response;
    });
};

export const getSearchData = (token, url,params) => {
  return axios(url, {
    method: 'get',
    headers: {
      'Accept': 'application/json',
      Authorization: 'Bearer ' + token,
    },
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return error.response;
    });
};


