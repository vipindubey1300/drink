import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import 'react-native-gesture-handler';
import {createStackNavigator} from 'react-navigation-stack';
import SplashScreen from '../views/splash/splashScreen';
import SignIn from '../views/signIn';
import SignUp from '../views/signUp';
import ForgotPassword from '../views/forgotPassword';
import ResetPassword from '../views/resetPassword';
import Otp from '../views/otp';
import Home from '../views/Home/home';
import Account from '../views/account';
import Notifications from '../views/notifications';
import Cart from '../views/cart';
import CheckOutStep1 from '../views/checkoutStep1';
import CheckOutStep2 from '../views/checkoutStep2';
import AddAddress from '../views/AddAddress';
import ChangePassword from '../views/changePassword';
import Profile from '../views/profile';
import LastOrder from '../views/lastOrder';
import ProductListing from '../views/productListing';
import ProductDetails from '../views/productDetails';
import AgeVerification from '../views/ageVerificationPopup';
import EditAddress from '../views/editAddress';
import VerifyOtp from '../views/verifyOtp';
import TermsAndConditions from '../views/termsAndConditions';
import PrivacyPolicy from '../views/privacyPolicy';
import Terms from  '../views/terms';

const HomeStack = createStackNavigator(
  {
    Home: {screen: Home},
    Notifications: {screen: Notifications},
    Cart: {screen: Cart},
    CheckOutStep1: {screen: CheckOutStep1},
    CheckOutStep2: {screen: CheckOutStep2},
    Account: {screen: Account},
    LastOrder: {screen: LastOrder},
    ProductListing: {screen: ProductListing},
    ProductDetails: {screen: ProductDetails},
    AddAddress: {screen: AddAddress},
    ChangePassword: {screen: ChangePassword},
    Profile: {screen: Profile},
    EditAddress: {screen: EditAddress},
    TermsAndConditions: {screen: TermsAndConditions},
    PrivacyPolicy: {screen: PrivacyPolicy},
    SignIn: {screen: SignIn},
    VerifyOtp: {screen: VerifyOtp},
    Otp: {screen: Otp},
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
    navigationOptions: {
      unmountInactiveRoutes: true
    },
  },
);

const LoginStack = createStackNavigator(
  {
     AgeVerification: {screen: AgeVerification},
    SignIn: {screen: SignIn},
    SignUp: {screen: SignUp,navigationOptions:{gestureEnabled:false}},
    ForgotPassword: {screen: ForgotPassword},
    ResetPassword: {screen: ResetPassword},
    VerifyOtp: {screen: VerifyOtp},
    Otp: {screen: Otp},
    Terms: {screen: Terms}
  },
  {
    headerMode: 'none',
    initialRouteName: 'SignIn',
  },
);

const MainNavigator = createSwitchNavigator(
  {
    Splash: {screen: SplashScreen},
    Login: {screen: LoginStack},
    Home: {screen: HomeStack},
  },
  {
    headerMode: 'none',
    initialRouteName: 'Splash',
  },
);
const Navigator = createAppContainer(MainNavigator);

export default Navigator;
