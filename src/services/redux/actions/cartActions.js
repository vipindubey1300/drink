import {CART_COUNT} from '../constants/index';

export function updateCartCount(cartCount) {
  return {
    type: CART_COUNT,
    payload: cartCount,
  };
}
