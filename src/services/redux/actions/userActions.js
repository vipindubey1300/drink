import {USER_UPDATE} from '../constants/index';

export function updateUserDetails(profile) {
  return {
    type: USER_UPDATE,
    payload: profile,
  };
}
