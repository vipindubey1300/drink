import {combineReducers} from 'redux';
import userReducer from './userReducer';
import productsReducer from './productsReducer';
import cartReducer from './cartReducer';
import notificationReducer from './notificationReducer';

export default combineReducers({
  userReducer,
  productsReducer,
  cartReducer,
  notificationReducer,
})