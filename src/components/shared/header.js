import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  Platform,
  BackHandler,
} from 'react-native';
import {connect} from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Badge} from 'react-native-elements';
import {fontSize} from '../global/Fontsize';
import {updateCartCount} from '../../services/redux/actions/cartActions';
import {
  updateNotificationCount,
} from '../../services/redux/actions/notificationActions';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();
class header extends Component {
  constructor (props) {
    super (props);
    this.state = {
      title: this.props.title,
      titleSet: true,
      cartCount: 0,
      notifCount: 0,
    };
  }

  navigationOn = () => {
    this.props.navigation.goBack ();
  };

  navigateToNotifications = () => {
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.props.handleBackButton
    );
    this.props.navigation.navigate ('Notifications');
  };

  navigateToCart = () => {
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.props.handleBackButton
    );
    this.props.navigation.navigate ('Cart');
  };

  componentDidMount () {
    var cartCount = this.props.cartReducer.cartCount;
    var notifCount = this.props.notificationReducer.notificationCount;
    this.setState ({
      cartCount: cartCount,
      notifCount: notifCount,
    });
  }

  componentDidUpdate (prevProps, prevState) {
    var cartCount = this.props.cartReducer.cartCount;
    var notifCount = this.props.notificationReducer.notificationCount;
    if (prevState.cartCount != cartCount) {
      this.setState ({cartCount: cartCount});
    }
    if (prevState.notifCount != notifCount) {
      this.setState ({notifCount: notifCount});
    }
  }

  render () {
    return (
      <View style={styles.headerBody}>
        <View
          style={
            this.props.backButton == true
              ? styles.headerSection
              : styles.headerSectionWithoutBackButton
          }
        >
          {this.props.backButton &&
            <TouchableOpacity onPress={this.navigationOn}>
              <Image
                source={require ('../../assets/backButton.png')}
                style={
                  this.props.hideBellIcon == true
                    ? styles.headerIconStyle
                    : styles.headerIconNotifyStyle
                }
              />
            </TouchableOpacity>}
          <View
            style={
              this.props.backButton == true
                ? styles.textSectionStyle
                : styles.headerTitleWithoutBackButtton
            }
          >
            <Text style={styles.headerTitle}>{this.props.title}</Text>
          </View>
        </View>
        <View
          style={
            this.props.hideBellIcon == true ? styles.icons : styles.iconNotify
          }
        >

                
          <TouchableOpacity
            style={styles.iconSection}
            onPress={this.navigateToCart}
          >
            <View>
              <Image
                source={require ('../../assets/cart.png')}
                style={styles.iconStyle}
              />
            </View>
            {this.state.cartCount !== 0 &&
              <View>
                <Badge
                  status="success"
                  value={this.state.cartCount}
                  containerStyle={styles.badgeStyle}
                />
              </View>}
          </TouchableOpacity>


          {this.props.hideBellIcon &&
            <TouchableOpacity
              style={styles.iconSection}
              onPress={this.navigateToNotifications}
            >
              <View>
                <Image
                  source={require ('../../assets/bell.png')}
                  style={styles.iconStyle}
                />
              </View>
              {this.state.notifCount !== 0 &&
                <View>
                  <Badge
                    status="success"
                    value={this.state.notifCount}
                    containerStyle={styles.badgeStyle}
                  />
                </View>}
            </TouchableOpacity>}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const {notificationReducer, cartReducer} = state;
  return {notificationReducer, cartReducer};
};

const mapDispatchToProps = dispatch => {
  return {
    updateCartCount: cartCount => {
      dispatch (updateCartCount (cartCount));
    },
    updateNotificationCount: notificationCount => {
      dispatch (updateNotificationCount (notificationCount));
    },
  };
};
export default connect (mapStateToProps, mapDispatchToProps) (header);

const styles = EStyleSheet.create ({
  headerBody: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '55rem',
    backgroundColor: '#47170d',
  },
  headerSection: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  headerSectionWithoutBackButton: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    flexDirection: 'row',
    width: '60%',
  },
  headerTitle: {
    fontSize: fontSizeValue * 18,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'white',
  },
  icons: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '20%',
    paddingRight: '25rem',
  },
  iconNotify: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '20%',
    paddingRight: '25rem',
  },
  iconStyle: {
    padding: '10rem',
    margin: '5rem',
    height: '30rem',
    width: '30rem',
    resizeMode: 'contain',
    alignItems: 'center',
  },
  headerIconStyle: {
    height: '20rem',
    width: '20rem',
    resizeMode: 'contain',
    alignItems: 'center',
    marginLeft: '15rem',
  },
  headerIconNotifyStyle: {
    height: '20rem',
    width: '20rem',
    resizeMode: 'contain',
    alignItems: 'center',
    marginLeft: '15rem',
  },
  iconSection: {
    flexDirection: 'row',
  },
  textSectionStyle: {
    width: '270rem',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  headerTitleWithoutBackButtton: {
    flexDirection: 'row',
  },
  badgeStyle: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
});
