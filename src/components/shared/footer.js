import React, {Component} from 'react';
import {View, Text, Dimensions, Image, BackHandler} from 'react-native';
import Styles from '../../styles/styles';
import EStyleSheet from 'react-native-extended-stylesheet';
import {fontSize} from '../global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';

const screenWidth = Dimensions.get ('window').width;
EStyleSheet.build ({$rem: screenWidth / 380});
const fontSizeValue = fontSize ();

class footer extends Component {
  constructor (props) {
    super (props);
    this.state = {};
  }

  navigateToHome = () => {
    this.props.navigation.navigate ('Home');
  };

  navigateToAccount = () => {
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.props.handleBackButton
    );
    this.props.navigation.navigate ('Account');
  };

  navigateToHistory = () => {
    BackHandler.removeEventListener (
      'hardwareBackPress',
      this.props.handleBackButton
    );
    this.props.navigation.navigate ('LastOrder');
  };

  render () {
    return (
      <View style={styles.footerBody}>
        <TouchableOpacity onPress={this.navigateToAccount}>
          <Image
            source={require ('../../assets/account.png')}
            style={styles.userIconStyle}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.navigateToHome}>
          <Image
            source={require ('../../assets/home.png')}
            style={styles.homeIconStyle}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.navigateToHistory}>
          <Image
            source={require ('../../assets/history.png')}
            style={styles.lastOrderIconStyle}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default footer;

const styles = EStyleSheet.create ({
  footerBody: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#F2F2F2',
    height: '50rem',
    elevation: 10,
    borderColor: 'black',
  },
  lastOrderIconStyle: {
    padding: '10rem',
    margin: '20rem',
    height: '45rem',
    width: '45rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  userIconStyle: {
    padding: '10rem',
    margin: '20rem',
    height: '42rem',
    width: '42rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  homeIconStyle: {
    padding: '10rem',
    marginBottom: '50rem',
    height: '70rem',
    width: '70rem',
    resizeMode: 'stretch',
    alignItems: 'center',
  },
});
