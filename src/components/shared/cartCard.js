import React, {Component} from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  ActivityIndicator,
  Platform,
  StyleSheet,
  Alert,
  BackHandler,
} from 'react-native';
import {connect} from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';
import Toast, {DURATION} from 'react-native-easy-toast';
import {fontSize} from '../global/Fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';
import STRINGS from '../../utils/strings';
import * as HOMEAPI from '../../services/api-services/home-api';
import CONFIG from '../../config/config';
import Styles from '../../styles/styles';
import {updateCartCount} from '../../services/redux/actions/cartActions';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class cartCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 0,
      title: '',
      item: {},
      loader: false,
      token: '',
      isQuantityZero: false,
      previousQuantity: 0,
      cartCount: 0,
    };
  }

  showToast = (toastMsg) => {
    this.toast.show(toastMsg);
  };

  addItem = () => {
    let quantity = this.state.quantity + 1;
    let previousQuantity = this.state.quantity;
    this.setState({
      quantity: quantity,
      previousQuantity: previousQuantity,
    });
  };

  removeItem = () => {
    let quantity = this.state.quantity - 1;
    let previousQuantity = this.state.quantity;
    if (this.state.quantity != 0) {
      this.setState({
        quantity: quantity,
        previousQuantity: previousQuantity,
      });
    }
  };

  addCartItem = () => {
    let quantity = this.state.quantity + 1;
    let previousQuantity = this.state.quantity;
    this.setState({
      quantity: quantity,
      previousQuantity: previousQuantity,
    });
    this.updateProductQuantity(quantity);
  };

  removeCartItem = () => {
    let quantity = this.state.quantity - 1;
   
    let previousQuantity = this.state.quantity;
    if (quantity != 0) {
      this.setState({
        quantity: quantity,
        previousQuantity: previousQuantity,
      });
     
      this.updateProductQuantity(quantity);
    }
     else if (quantity < 1){
    
      this.removeFromCart()
        
     }


  };

  updateProductQuantity = (quantity) => {
    // this.setState({loader: true});

     if(this.props.lode){this.props.lode(true)}
         


    let payload = {
      product_id: this.props.productId,
      quantity: quantity, 
    };
    HOMEAPI.postRequestData(
      payload,
      this.state.token,
      CONFIG.BASE_URL + CONFIG.UPDATE_PRODUCT_QUANTITY_URL,
    )
      .then(
        function (res) {
          this.setState({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                this.showToast('Item quantity updated successfully');
                if (this.props.cart == 'cart') {
                  this.props.getCartItem(this.state.token);
                }
                break;
              case CONFIG.statusUnauthorized:
                this.showToast(
                  'Your account has been deactivated. Please logout and login with valid user credentials',
                );
                this.setState({quantity: this.state.previousQuantity});
                this.props.navigation.navigate('Home');
                break;
              case CONFIG.statusError:
                this.setState({quantity: 0});
                this.showToast('Something went wrong. Please try again!');
                break;
            }
          } else {
            this.showToast('Something went wrong. Please try again!');
          }
        }.bind(this),
      )
      .catch(
        function (err) {
          this.setState({loader: false});
          this.showToast('Something went wrong. Please try again!');
        }.bind(this),
      );
  };

  addToCart = () => {
    if (this.state.quantity == 0) {
      this.showToast('Please select quantity');
    } else {
      this.setState({loader: true});
      let previousQuantity = this.state.quantity;
      let payload = {
        product_id: this.props.productId,
        quantity: this.state.quantity,
      };
      let cartCount = this.state.cartCount;
      HOMEAPI.postRequestData(
        payload,
        this.state.token,
        CONFIG.BASE_URL + CONFIG.ADD_CART_ITEM_URL,
      )
        .then(
          async function (res) {
            this.setState({loader: false});
            if (res != undefined) {
              let code = res.code;
              switch (code) {
                case CONFIG.statusOk:
                  // cartCount = (await cartCount) + 1;
                  // await this.props.updateCartCount(cartCount);
                  // console.log('asdfghjsdfghjke-------++++++------>>>>>>>>>>>>>',cartCount)
                  // this.showToast('Item added to cart successfully');
                  // break;

                  var cartCounts = this.props.cartReducer.cartCount;
                  if (cartCounts) {
                    this.setState({cartCount: cartCounts});
                  }
                 
                    this.setState({cartCount: this.state.cartCount + 1},()=>{
                      console.log('asdfghjsdfghjke-------++++++------>>>>>>>>>>>>>',this.state.cartCount)
                    });
                    cartCounts = cartCounts + 1;
                    console.log('asdfghjsdfghjke-------++++++------>>>>>>>>>>>>>',this.state.cartCount)
                    this.props.updateCartCount(cartCounts);
                    this.showToast('Item added to cart successfully');
                    break;
                case CONFIG.statusAlreadyAdded:
                  this.showToast('Item already exist in the cart');
                  this.updateProductQuantity(this.state.quantity);
                  break;
                case CONFIG.statusUnauthorized:
                  this.showToast(
                    'Your account has been deactivated. Please logout and login with valid user credentials',
                  );
                  this.props.navigation.navigate('Home');
                  break;
                case CONFIG.statusError:
                  console.log('----------------->>>>>>>>>>>>>>>>>>>>>>')
                  this.showToast('Something went wrong. Please try again!');
                  break;
              }
            } else {
              console.log('>>>>>>>>>>>>>>>>>>>>>')
              this.showToast('Something went wrong. Please try again!');
            }
          }.bind(this),
        )
        .catch(
          function (err) {
            this.setState({loader: false});
            console.log('++++++++++++++++++++++++++',err.message)
            this.showToast('Something went wrong. Please try again!');
          }.bind(this),
        );
    }
  };

  deleteItem = () => {
    Alert.alert(
      '',
      'Are you sure you want to remove ' + this.props.drinkName + ' from cart',
      [
        {
          text: 'CANCEL',
          style: 'destructive',
        },
        {text: 'YES', onPress: () => this.removeFromCart()},
      ],
      {cancelable: false},
    );
  }; 

  removeFromCart = () => {
    // this.setState({loader: true});
    // this.props.lode(true)
    if(this.props.lode){this.props.lode(true)}

    let payload = {
      product_id: this.props.productId,
    };
    let cartCount = this.state.cartCount;
    HOMEAPI.postRequestData(
      payload,
      this.state.token,
      CONFIG.BASE_URL + CONFIG.REMOVE_CART_ITEM_URL,
    )
      .then(
        function (res) {
          this.setState({loader: false});
          if (res != undefined) {
            let code = res.code;
            switch (code) {
              case CONFIG.statusOk:
                this.setState({quantity: this.state.quantity - 1});
                cartCount = cartCount - 1;
                this.props.updateCartCount(cartCount);
                this.props.getCartItem(this.state.token);
                this.showToast('Item removed from cart successfully');
                break;
              case CONFIG.statusUnauthorized:
                this.showToast(
                  'Your account has been deactivated. Please logout and login with valid user credentials',
                );
                this.props.navigation.navigate('Home');
                break;
              case CONFIG.statusError:
                this.showToast('Something went wrong. Please try again!');
                let errors = res.errors;
                break;
            }
          } else {
            this.showToast('Something went wrong. Please try again!');
          }
        }.bind(this),
      )
      .catch(
        function (err) {
          this.setState({loader: false});
          this.showToast('Something went wrong. Please try again!');
        }.bind(this),
      );
  };

  componentDidMount() {
    var profile = this.props.userReducer.profile;
    var cartCount = this.props.cartReducer.cartCount;
    if (cartCount) {
      this.setState({cartCount: cartCount});
    }
    if (profile) {
      var token = profile.token;
      this.setState({token: token});
    }
    this.setState({item: this.props.item, title: this.props.drinkName});
    if (this.props.quantity) {
      this.setState({quantity: this.props.quantity});
    }
  }

  navigateToProductDetails = () => {
    this.props.navigation.navigate('ProductDetails', {
      title: this.state.title,
      quantity: this.state.quantity,
      item: this.state.item,
      productId: this.props.productId,
    });
  };

  render() {
    
    return (
      <View style={styles.cartList}>
        <View style={styles.cartCardStyle}>

        {/* {this.state.loader &&
          <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'transparent', justifyContent: 'center' }
          ]}>
            <ActivityIndicator size="large" color="#47170d" />
          </View>
        } */}

          {this.state.loader && (
            <View style={[Styles.loading]}>
              <ActivityIndicator size="large" color="#47170d" />
            </View>
          )}
          <View style={styles.drinkPicture}>
            <Image
              source={{uri: this.props.imageUrl}}
              style={styles.drinkPictureStyle}
            />
          </View>
          <View style={styles.cartDetails}>
            <View>
              <TouchableOpacity
                activeOpacity={0.5}
                onPress={this.navigateToProductDetails}>
                {this.props.product_quantity === undefined ? (
                  <Text style={styles.drinkName}>{this.props.drinkName}</Text>
                ) : (
                  <Text style={styles.drinkName}>
                    {this.props.drinkName + this.props.product_quantity}
                  </Text>
                )}
              </TouchableOpacity>
              {this.props.offer !== '0% off' ? (
                <Text style={styles.offerText}>
                  {'$' + this.props.item.product_price}
                  <Text style={styles.offerTextGreen}>
                    {'  (' + this.props.offer + ')'}
                  </Text>
                </Text>
              ) : (
                <Text style={styles.offerText}>
                  {'$' + this.props.item.product_price}
                  <Text style={styles.noOfferText}>
                    {' (No offer applied)'}
                  </Text>
                </Text>
              )}
              {this.props.offer !== '0% off' && (
                <Text style={styles.discountText}>
                  {'Discounted price ' +
                    '($' +
                    this.props.item.discounted_price +
                    ')'}
                </Text>
              )}
              <Text style={styles.deliveryText}>{this.props.deliveryTime}</Text>
            </View>
            <View style={styles.iconConatiner}>
              <View style={styles.addRemoveStyle}>
                {this.props.showAddButton && (
                  <TouchableOpacity
                    onPress={this.removeItem}
                    activeOpacity={0.5}>
                    <Image
                      source={require('../../assets/remove.png')}
                      style={styles.iconStyle}
                    />
                  </TouchableOpacity>
                )}
                {this.props.showDeleteButton && (
                  <TouchableOpacity
                    onPress={this.removeCartItem}
                    activeOpacity={0.5}>
                    <Image
                      source={require('../../assets/remove.png')}
                      style={styles.iconStyle}
                    />
                  </TouchableOpacity>
                )}

              {Platform.OS=='ios' ? 
               <View style={{position:'absolute',top:0}}>
                <Text style={{color:'#47170d',fontSize:23,top:-3,position:'absolute',left:25}}>|</Text>
                <Text style={[styles.quantityText,{position:'absolute',left:32,top:6}]}>{this.state.quantity}</Text>
                <Text style={{color:'#47170d',fontSize:23,top:-3,position:'absolute',left:65}}>|</Text>
               </View>
               : <Text style={styles.quantityText}>{this.state.quantity}</Text>
                }
              

                {this.props.showAddButton && (
                  <TouchableOpacity onPress={this.addItem} activeOpacity={0.5}>
                    <Image
                      source={require('../../assets/add.png')}
                      style={styles.iconStyle}
                    />
                  </TouchableOpacity>
                )}
                {this.props.showDeleteButton && (
                  <TouchableOpacity
                    onPress={this.addCartItem}
                    activeOpacity={0.5}>
                    <Image
                      source={require('../../assets/add.png')}
                      style={styles.iconStyle}
                    />
                  </TouchableOpacity>
                )}
              </View>
              <View>
                {this.props.showDeleteButton && (
                  <TouchableOpacity onPress={this.deleteItem}>
                    <Image
                      source={require('../../assets/delete.png')}
                      style={styles.deleteIconStyle}
                    />
                  </TouchableOpacity>
                )}
                {this.props.showAddButton && (
                  <TouchableOpacity onPress={this.addToCart}>
                    <Text style={styles.addToCartText}>
                      {STRINGS.productListing.addToCart}
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
        </View>
        <Toast
          position="top"
          style={styles.toastStyle}
          textStyle={{color: 'white'}}
          ref={(ref) => {
            this.toast = ref;
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const {userReducer, cartReducer} = state;
  return {userReducer, cartReducer};
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCartCount: (cartCount) => {
      dispatch(updateCartCount(cartCount));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(cartCard);


cartCard.defaulProps={
  lode : ()=>{}
}


const styles = EStyleSheet.create({
  cartList: {
    paddingLeft: '20rem',
    paddingRight: '20rem',
    paddingBottom: '20rem',
  },
  cartCardStyle: {
    flexDirection: 'row',
    width: '100%',
    borderColor: 'black',
    borderWidth: 0.5,
    elevation: 1,
    backgroundColor: 'white',
    borderRadius: '5rem',
    paddingBottom: '5rem',
    paddingTop: '5rem',
  },
  drinkPicture: {
    flexDirection: 'column',
    // justifyContent: 'center',
    width: '35%',
    paddingTop: '5rem',
    paddingBottom: '5rem',
    paddingLeft: '8rem',
  },
  cartDetails: {
    flexDirection: 'column',
    width: '65%',
    justifyContent: 'space-between',
    paddingTop: '2rem',
    paddingBottom: '5rem',
    paddingLeft: '10rem',
    paddingRight: '10rem',
  },
  drinkName: {
    fontSize: fontSizeValue * 11.5,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Bold' : 'Calisto MT',
    color: 'black',
    fontWeight:'bold',
  },
  offerText: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
    marginTop: '5rem',
  },
  discountText: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
    marginTop: '5rem',
  },
  deliveryText: {
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
    marginTop: '5rem',
    marginBottom: '3rem',
  },
  iconStyle: {
    padding: '8rem',
    margin: '5rem',
    height: '8rem',
    width: '8rem',
    resizeMode: 'contain',
    alignItems: 'center',
  },
  deleteIconStyle: {
    padding: '10rem',
    margin: '5rem',
    height: '8rem',
    width: '8rem',
    resizeMode: 'contain',
    alignItems: 'center',
  },
  addRemoveStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#ccc',
    width: '100rem',
    height: '25rem',
  },
  quantityText: {
    fontSize: fontSizeValue * 11,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Bold' : 'Calisto MT',
    fontWeight:'bold',
    color: 'black',
    paddingLeft: '12rem',
    paddingRight: '12rem',
    borderLeftWidth: 1,
    borderLeftColor: '#47170d',
    borderRightWidth: 1,
    borderRightColor: '#47170d',
    // overflow: 'hidden', 
  },
  iconConatiner: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: '25rem',
  },
  drinkPictureStyle: {
    height: '125rem',
    width: '115rem',
  },
  addToCartText: {
    backgroundColor: '#ccc',
    padding: '4rem',
    fontSize: fontSizeValue * 10,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: 'black',
  },
  toastStyle: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    borderRadius: '30rem',
    paddingTop: '5rem',
    paddingBottom: '5rem',
    paddingLeft: '20rem',
    paddingRight: '20rem',
    bottom: '30rem',
  },
  offerTextGreen: {
    fontSize: fontSizeValue * 8,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#12b007',
    marginTop: '5rem',
  },
  noOfferText: {
    fontSize: fontSizeValue * 8,
    fontFamily: Platform.OS == 'android' ? 'Calisto-Regular' : 'Calisto MT',
    color: '#47170d',
    marginTop: '5rem',
  },
});







