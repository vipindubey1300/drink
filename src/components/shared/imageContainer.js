import React, {Component} from 'react';
import {View, Text, Image, Dimensions} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {fontSize} from '../global/Fontsize';

const screenWidth = Dimensions.get('window').width;
EStyleSheet.build({$rem: screenWidth / 380});
const fontSizeValue = fontSize();

class imageContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        <Image source={{ uri:this.props.imageUrl }} style={styles.imageStyle} />
      </View>
    );
  }
}

export default imageContainer;

const styles = EStyleSheet.create({
  imageStyle: {
    height: '117rem',
    width: '150rem',
  },
});
