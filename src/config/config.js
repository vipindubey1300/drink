/**********************************************************************************************
 * File: Config file to store configuration used in application
********************************************************************************************* */

const MOBILE_NUM_FORMAT = /^[\s()+-]*([0-9][\s()+-]*){6,15}$/

const NAME_FORMAT = new RegExp('^[a-zA-Z][a-zA-Z0-9 ]*$');

const CITY_FORMAT = new RegExp('^[a-zA-Z][a-zA-Z ]*$');

const EMAIL_FORMAT = /[a-zA-Z0-9!#$%&'*+\=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;

const PASSWORD_POLICY = new RegExp('^(?=.{8,})(?=.*[0-9])(?=.*[a-z])');

const CHANGE_PASS = new RegExp('^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$')

const SIGNIN_FORMAT = /^([_a-z0-9+_-]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))|(\d+$)$/;

/*************Base URL***********/
const BASE_URL = 'https://www.webmobril.org/dev/beverage/api/'

/***********API URLs******************************************/
const SIGNUP_URL = 'signup'
const SIGNIN_URL = 'signin'
const FORGOT_PASS_URL = 'forgot-password'
const VERIFY_OTP_URL = 'verify-forgot-otp'
const CHANGE_PASSWORD_URL = 'change-password'
const VIEW_PROFILE_URL = 'view-profile'
const EDIT_PROFILE_URL = 'edit-profile'
const ADD_ADDRESS_URL = 'add-address'
const VIEW_ADDRESS_URL = 'view-address'
const VIEW_NOTIFICATIONS_URL = 'view-notifications'
const GET_CART_URL = 'get-cart'
const ADD_CART_ITEM_URL = 'add-cart-item'
const REMOVE_CART_ITEM_URL = 'remove-cart-item'
const DASHBOARD_URL = 'dashboard'
const RECOVER_PASSWORD_URL = 'recover-password'
const PRODUCT_BY_CATEGORY_URL = 'add-cart-item'
const SEARCH_PRODUCT_URL = 'search-product?'
const PRODUCT_DETAIL_URL = 'dashboard'
const LAST_ORDER_URL = 'last-order'
const ORDER_URL = 'order'
const RESEND_URL = 'resend-otp'
const EDIT_ADDRESS_URL ='edit-address'
const VERIFY_ACCOUNT_OTP_URL = 'verify-account'
const UPDATE_SESSION = 'update-session'
const UPDATE_PRODUCT_QUANTITY_URL = 'update-product-quantity'
const GET_PAGE_URL = 'get-page'
const DELETE_ADDRESS_URL = 'remove-address'
const PAYMENT_URL = 'payment'
const MARK_READ_NOTIFICATION_URL = 'mark-read-notification'
const GET_COUNT_URL = 'get-count'

/************************************************************/

/*******API Status Codes*******/
const statusOk = 200
const statusError = 400
const statusUnauthorized = 401
const statusAlreadyAdded = 422

/******************************/

export default {
    MOBILE_NUM_FORMAT,
    CITY_FORMAT,
    NAME_FORMAT,
    EMAIL_FORMAT,
    SIGNIN_FORMAT,
    CHANGE_PASS,
    PASSWORD_POLICY,
    BASE_URL,
    FORGOT_PASS_URL,
    GET_COUNT_URL,
    VERIFY_OTP_URL,
    VERIFY_ACCOUNT_OTP_URL,
    SIGNUP_URL,
    SIGNIN_URL,
    CHANGE_PASSWORD_URL,
    DASHBOARD_URL,
    REMOVE_CART_ITEM_URL,
    ADD_CART_ITEM_URL,
    GET_CART_URL,
    VIEW_NOTIFICATIONS_URL,
    VIEW_ADDRESS_URL,
    ADD_ADDRESS_URL,
    EDIT_PROFILE_URL,
    VIEW_PROFILE_URL,
    PRODUCT_BY_CATEGORY_URL,
    SEARCH_PRODUCT_URL,
    PRODUCT_DETAIL_URL,
    RECOVER_PASSWORD_URL,
    LAST_ORDER_URL,
    ORDER_URL,
    RESEND_URL,
    EDIT_ADDRESS_URL,
    UPDATE_SESSION,
    UPDATE_PRODUCT_QUANTITY_URL,
    GET_PAGE_URL,
    DELETE_ADDRESS_URL,
    PAYMENT_URL,
    MARK_READ_NOTIFICATION_URL,
    statusOk,
    statusError,
    statusUnauthorized,
    statusAlreadyAdded
}