const en = {
  signIn: {
    signIn: 'Sign In',
    userId: 'User ID',
    password: 'Password',
    forgotPassword: 'Forgot Password?',
    dontHaveAnAcc: "Don't have an account?",
    signUp: 'Sign Up',
    selectDate: 'Select Birth Date',
    passMsg: 'Field is required.',
    emailMsg: 'Field is required'
  },
  forgotPassword: {
    emailOrNumber: 'Email ID/Mobile Number',
    submit: 'SUBMIT',
    forgotPassword: 'Forgot Password',
    emailMsg: 'Invalid Email Id/Mobile Number',
    resendOtp: 'Resend OTP'
  },
  signUp: {
    name: 'Name',
    phnNumber: 'Mobile Number',
    emailId: 'Email ID',
    password: 'Password',
    signUp: 'Sign Up',
    signIn: 'Sign In',
    confirmText: 'Confirm that you are above the legal age of 18:',
    yes: 'Yes',
    no: 'No',
    alreadyHaveAnAcc: 'Already have an account?',
    termsAndConditionText1:'"By signing up you agree to the',
    termsAndConditionText2:'Terms ',
    termsAndConditionText3: 'and Conditions ',
    termsAndConditionText4:'of Dial A Drink Zimbabwe"',
    passMsg: 'Password length should be between 8-16 characters with uppercase, lowercase, special character and numeric value',
    nameMsg: 'Name field is required',
    mobileMsg: 'Mobile Number field is required',
    emailMsg: 'Email field is required'
  },
  resetPassword: {
    resetPassword: 'Reset Password',
    newPassword: 'New Password',
    confirmPassword: 'Confirm Password',
    reset: 'RESET',
    confirmPassMsg1: 'Confirm password field is required',
    confirmPassMsg2: 'Password entered do not match',
    newPassMsg: 'New password field is required',
    newPassErrMsg: 'Password length should be between 8-16 characters with uppercase, lowercase, special character and numeric value',
  },
  otp: {
    otp: 'OTP',
    verifyOtp: 'Verify OTP',
    otpText:
      'OTP has been sent to your registered Email ID/Mobile Number, please enter Verification code to reset your password',
    verifyOtpText:
      'OTP has been sent to your registered Email ID/Mobile Number, please enter Verification code to verify mobile number and email id',
    enterOTPHere: 'Enter your OTP here',
    enterMobileOTPHere: 'Enter mobile OTP here',
    enterEmailOTPHere: 'Enter email OTP here',
  },
  home: {
    hometitle: 'Home',
    search: 'Search',
    all: 'All',
    beersAndCiders: 'Beers & Ciders',
    whiskeyAndBrandy: 'Whiskey & Brandy',
    vodkaAndGin: 'Vodka & Gin',
    topDrinks: 'Top Drinks',
    specialDrinks: 'On Special',
    viewAll: 'View All >>',
  },
  account: {
    changePassword: 'Change Password',
    accountTitle: 'Account',
    changePwd: 'Change Password',
    address: 'Address',
    lastOrder: 'Last Order',
    termsAndConditions: 'Terms and Conditions',
    logout: 'Log Out',
    save: 'Save',
    password: 'Password',
    newPassword: 'New Password',
    confirmPassword: 'Confirm Password',
    emailId: 'Email ID',
    phnNumber: 'PhoneNumber',
    profile: 'Profile',
    newPasswordMsg: 'Field is required',
    privacyPolicy: 'Privacy Policy'
  },
  notifications: {
    notificationsTitle: 'Notifications',
    notificationText:
      'Your order has been shipped. ETA 30min. Thank you for choosing Dial A Drink ZW',
  },
  lastOrder: {
    lastOrderTitle: 'Last Order ',
    lastOrderText:
      'Your Orange Drink has been delivered. Thank you for choosing Dial A Drink ZW',
  },
  cart: {
    cartTitle: 'Cart',
    cartTotal: 'Cart Total',
    tax: 'Tax',
    other: 'Other',
    subtotal: 'Subtotal',
    checkout: 'Check Out',
    requiredText: 'Minimum order requirement is $40',
  },
  checkout: {
    checkout: 'Check Out',
    step1: 'Step 1',
    address: 'Address',
    submit: 'SUBMIT',
    addAddress: 'Add Address',
    editAddress: 'Edit Address',
    step2: 'Step 2',
    pay: 'Pay',
    cashOnDelivery: 'Cash on Delivery',
    cardSwipe: 'Card Swipe',
    ecocash: 'Ecocash / Zipit',
    add: 'Add',
    name: 'Name',
    houseName: 'House Name, Number',
    lane: 'Lane and Suburb',
    city: 'City',
    phnNumber: 'Phone Number',
  },
  productListing: {
    hometitle: 'Home',
    search: 'Search',
    addToCart: 'Add to Cart',
    details: 'Details'
  },
  productDetails: {
    addToCart: 'Add to Cart',
  }
};

export default en;
