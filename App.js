import React, {Component} from 'react';
import PushNotification from 'react-native-push-notification';
import messaging from '@react-native-firebase/messaging';
import Navigator from './src/services/Navigator';
import NavigationService from './src/services/navigationService';
import { LogBox } from 'react-native';
LogBox.ignoreAllLogs()
// Register background handler
messaging ().setBackgroundMessageHandler (async remoteMessage => {
  console.log ('Message handled in the background!', remoteMessage);
  NavigationService.navigate ('Notifications');
});

messaging ().getInitialNotification ().then (remoteMessage => {
  if (remoteMessage) {
    NavigationService.navigate ('Notifications');
  }
});

messaging ().onNotificationOpenedApp (remoteMessage => {
  console.log (
    'Notification caused app to open from background state:',
    remoteMessage.notification
  );
  NavigationService.navigate ('Notifications');
});

// When a user receives a push notification and the app is in foreground
messaging ().onMessage (async remoteMessage => {
  console.log (
    'Message handled in the forground!',
    JSON.stringify (remoteMessage)
  );
// NavigationService.navigate('Notifications');
});

class App extends Component {
  constructor (props) {
    super (props);
    this.state = {};
  }

  render () {
    return (
      <Navigator
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator (navigatorRef);
        }}
      />
    );
  }
}

export default App;
